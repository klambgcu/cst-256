<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    //Test Controller
    public function test() {
        return "Hello World from Test Controller";
    }
    
    // Test Controller 2
    public function test2() {
        return view('helloworld');
    }
    
}
