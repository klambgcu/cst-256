<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Web Routing 
 * 2. Handles routing - very conveniently
 * 3. 
 * ---------------------------------------------------------------
 */

// Home Page
Route::get('/', function () {
    return view('welcome');
});


// ---------------------------------------------------------------
// Login Section
// ---------------------------------------------------------------

// Login Form
Route::get('/login', function () {
    return view('login');
});

// Login Form Action
Route::post('/doLogin', 'LoginController@login');

// Login Status Form
Route::get('/loginstatus', function () {
    return view('loginstatus');
});
    
    
// ---------------------------------------------------------------
// Registration Section
// ---------------------------------------------------------------

// Registration Form
Route::get('/register', function () {
    return view('register');
});

// Registration Form Action
Route::post('/doRegister', 'RegisterController@register');

// Register Status Form
Route::get('/registerstatus', function () {
    return view('registerstatus');
});
        
    