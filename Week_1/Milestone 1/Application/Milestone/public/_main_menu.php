<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Main Menu (_main_menu.php)
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
 
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">GetJobs</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

<?php
$user_info ="TEST";
if ($user_info == "USER_AUTH")
{
    //
    // User is logged into the application
    //
    echo "  <ul class='navbar-nav me-auto mb-2 mb-lg-0'>\r\n";
    echo "      <li class='nav-item'><a class='nav-link active' aria-current='page' href='/'>Home</a></li>\r\n";
    echo "      <li class='nav-item dropdown'>\r\n";
    echo "          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Account</a>\r\n";
    echo "          <ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\r\n";
    echo "              <li><a class='dropdown-item' href='/register'>Registration...</a></li>\r\n";
    echo "              <li><hr class='dropdown-divider'></li>\r\n";
    echo "              <li><a class='dropdown-item' href='/logout'>Log Out</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";
}
else
{
    //
    // User is NOT logged into the application
    //
    echo "  <ul class='navbar-nav me-auto mb-2 mb-lg-0'>\r\n";
    echo "      <li class='nav-item'><a class='nav-link active' aria-current='page' href='/'>Home</a></li>\r\n";
    echo "      <li class='nav-item dropdown'>\r\n";
    echo "          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Account</a>\r\n";
    echo "          <ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\r\n";
    echo "              <li><a class='dropdown-item' href='/register'>Registration...</a></li>\r\n";
    echo "              <li><hr class='dropdown-divider'></li>\r\n";
    echo "              <li><a class='dropdown-item' href='/login'>Login...</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";
    echo "  </ul>\r\n";
}
?>
    </div>
  </div>
</nav>

<!-- Put the Store Name / Title Here -->
<div align="center">
	<br /><h1>Get Jobs</h1>
	<hr>
</div>

<style>
.form-floating > .form-control::placeholder {
    color: revert;
}

.form-floating > .form-control:not(:focus)::placeholder {
    color: transparent;
}
</style>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
