<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handle Login Functionality
 * 3. Added Main Menu requirement
 * ---------------------------------------------------------------
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    //Login Method
    public function login()
    {
        // store registration parameters
        $email    = filter_input(INPUT_POST,'Email');
        $password = filter_input(INPUT_POST,'Password');

        $users = DB::select('select * from users where email = ? and password = ?', [$email, $password]);
        
        $num_rows = count($users);
        
        // Determine if login successful
        if ($num_rows === 1)
        {
            // Valid user
            $_SESSION["message"] = "Welcome " . $users[0]->FIRST_NAME . " " . $users[0]->LAST_NAME;
        }
        elseif ($num_rows === 0 || $num_rows >= 2)
        {
            // Invalid user
            $_SESSION["message"] = "Login was unsuccessful - please try again.";
        }

       return view('loginstatus');
        
    }
}
