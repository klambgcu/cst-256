<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Member Profile Form
 * 2. 
 * 3. 
 * ---------------------------------------------------------------
 */

if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}

$user = $_SESSION['principal'];

$um = $userAggregateModel->getUserModel();
$up = $userAggregateModel->getUserProfile();
$ue = $userAggregateModel->getUserEducation();
$us = $userAggregateModel->getUserSkillset();
$uw = $userAggregateModel->getUserWorkHistory();

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Profile Page')
@section('content')

<div class="container">

	<form action="doMemberProfile" method="POST">
	@csrf
		<div align="center">
		    <h1>Member Profile Form</h1>
		    <p>Please examine your profile and update accordingly.</p>
		    <hr>
		</div>

    	<h3>Photo:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<div class="form-floating mb-3 mt-3">
					<img src="{{ asset('images/'.$up->getPhoto()) }}" width="300px" height="300px" />
        		    <input type="file" class="form-control"  placeholder="Change Photo" name="Photo" id="Photo" value = "<?php echo $up->getPhoto(); ?>">
				</div>
			</div>
			<div class="col-1"></div>
		</div>

	<hr>
    	<h3>Description:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control"  placeholder="Tell us about you..." name="Description" id="Description" value = "<?php echo $up->getDescription(); ?>" required>
        		    <label for="Description" class="form-label">Description:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

	<hr>
    	<h3>Name:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control"  placeholder="Enter first name" name="FirstName" id="FirstName" value = "<?php echo $um->getFirst_name(); ?>" required>
        		    <label for="FirstName" class="form-label">First Name:</label>
				</div>
			</div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control"  placeholder="Enter last name" name="LastName" id="LastName" value = "<?php echo $um->getLast_name(); ?>" required>
        		    <label for="LastName" class="form-label">Last Name:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

	<hr>
    <h3>Contact Information:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="email" class="form-control" placeholder="Enter email address" name="Email" id="Email" value = "<?php echo $um->getEmail(); ?>" required>
        		    <label for="Email" class="form-label">Email:</label>
				</div>
			</div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="tel" class="form-control" placeholder="(999) 999-9999" name="Mobile" id="Mobile" pattern="\([0-9]{3}\) [0-9]{3}-[0-9]{4}" value = "<?php echo $um->getMobile(); ?>" required>
        		    <label for="Mobile" class="form-label">Mobile:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
	<hr>

    <h3>Identification:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="password" class="form-control" placeholder="Password Length 8 minimum" name="Password" id="Password" pattern=".{8,}" value = "<?php echo $um->getPassword(); ?>" required>
        		    <label for="Password" class="form-label">Password:</label>
				</div>
			</div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="date" class="form-control" placeholder="Enter birthdate" name="Birthdate" id="Birthdate" value = "<?php echo $um->getBirthdate(); ?>" required>
        		    <label for="Birthdate" class="form-label">Birth Date:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
					<select class="form-select" aria-label="Default select example" name="Gender" id="Gender" required>
<?php
echo "                  <option value='0' ";
echo ($um->getGender() == 0) ? "selected" : "";
echo ">Male</option>\n";
echo "                  <option value='1' ";
echo ($um->getGender() == 1) ? "selected" : "";
echo ">Female</option>\n";
?>

        			</select>
        		    <label for="Gender" class="form-label">Sex:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
	<hr>

    <h3>Address:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Street Address" name="Street" id="Street" value = "<?php echo $up->getStreet(); ?>" required>
        		    <label for="Street" class="form-label">Street:</label>
				</div>
			</div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="City" name="City" id="City" value = "<?php echo $up->getCity(); ?>" required>
        		    <label for="City" class="form-label">City:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
					<select class="form-select" aria-label="Default select example" name="State" id="State" required>
                        <option value="AL"<?php echo ($up->getState() == "AL") ? " selected" : ""; ?>>Alabama</option>
                        <option value="AK"<?php echo ($up->getState() == "AK") ? " selected" : ""; ?>>Alaska</option>
                        <option value="AZ"<?php echo ($up->getState() == "AZ") ? " selected" : ""; ?>>Arizona</option>
                        <option value="AR"<?php echo ($up->getState() == "AR") ? " selected" : ""; ?>>Arkansas</option>
                        <option value="CA"<?php echo ($up->getState() == "CA") ? " selected" : ""; ?>>California</option>
                        <option value="CO"<?php echo ($up->getState() == "CO") ? " selected" : ""; ?>>Colorado</option>
                        <option value="CT"<?php echo ($up->getState() == "CT") ? " selected" : ""; ?>>Connecticut</option>
                        <option value="DE"<?php echo ($up->getState() == "DE") ? " selected" : ""; ?>>Delaware</option>
                        <option value="DC"<?php echo ($up->getState() == "DC") ? " selected" : ""; ?>>District Of Columbia</option>
                        <option value="FL"<?php echo ($up->getState() == "FL") ? " selected" : ""; ?>>Florida</option>
                        <option value="GA"<?php echo ($up->getState() == "GA") ? " selected" : ""; ?>>Georgia</option>
                        <option value="HI"<?php echo ($up->getState() == "HI") ? " selected" : ""; ?>>Hawaii</option>
                        <option value="ID"<?php echo ($up->getState() == "ID") ? " selected" : ""; ?>>Idaho</option>
                        <option value="IL"<?php echo ($up->getState() == "IL") ? " selected" : ""; ?>>Illinois</option>
                        <option value="IN"<?php echo ($up->getState() == "IN") ? " selected" : ""; ?>>Indiana</option>
                        <option value="IA"<?php echo ($up->getState() == "IA") ? " selected" : ""; ?>>Iowa</option>
                        <option value="KS"<?php echo ($up->getState() == "KS") ? " selected" : ""; ?>>Kansas</option>
                        <option value="KY"<?php echo ($up->getState() == "KY") ? " selected" : ""; ?>>Kentucky</option>
                        <option value="LA"<?php echo ($up->getState() == "LA") ? " selected" : ""; ?>>Louisiana</option>
                        <option value="ME"<?php echo ($up->getState() == "ME") ? " selected" : ""; ?>>Maine</option>
                        <option value="MD"<?php echo ($up->getState() == "MD") ? " selected" : ""; ?>>Maryland</option>
                        <option value="MA"<?php echo ($up->getState() == "MA") ? " selected" : ""; ?>>Massachusetts</option>
                        <option value="MI"<?php echo ($up->getState() == "MI") ? " selected" : ""; ?>>Michigan</option>
                        <option value="MN"<?php echo ($up->getState() == "MN") ? " selected" : ""; ?>>Minnesota</option>
                        <option value="MS"<?php echo ($up->getState() == "MS") ? " selected" : ""; ?>>Mississippi</option>
                        <option value="MO"<?php echo ($up->getState() == "MO") ? " selected" : ""; ?>>Missouri</option>
                        <option value="MT"<?php echo ($up->getState() == "MT") ? " selected" : ""; ?>>Montana</option>
                        <option value="NE"<?php echo ($up->getState() == "NE") ? " selected" : ""; ?>>Nebraska</option>
                        <option value="NV"<?php echo ($up->getState() == "NV") ? " selected" : ""; ?>>Nevada</option>
                        <option value="NH"<?php echo ($up->getState() == "NH") ? " selected" : ""; ?>>New Hampshire</option>
                        <option value="NJ"<?php echo ($up->getState() == "NJ") ? " selected" : ""; ?>>New Jersey</option>
                        <option value="NM"<?php echo ($up->getState() == "NM") ? " selected" : ""; ?>>New Mexico</option>
                        <option value="NY"<?php echo ($up->getState() == "NY") ? " selected" : ""; ?>>New York</option>
                        <option value="NC"<?php echo ($up->getState() == "NC") ? " selected" : ""; ?>>North Carolina</option>
                        <option value="ND"<?php echo ($up->getState() == "ND") ? " selected" : ""; ?>>North Dakota</option>
                        <option value="OH"<?php echo ($up->getState() == "OH") ? " selected" : ""; ?>>Ohio</option>
                        <option value="OK"<?php echo ($up->getState() == "OK") ? " selected" : ""; ?>>Oklahoma</option>
                        <option value="OR"<?php echo ($up->getState() == "OR") ? " selected" : ""; ?>>Oregon</option>
                        <option value="PA"<?php echo ($up->getState() == "PA") ? " selected" : ""; ?>>Pennsylvania</option>
                        <option value="RI"<?php echo ($up->getState() == "RI") ? " selected" : ""; ?>>Rhode Island</option>
                        <option value="SC"<?php echo ($up->getState() == "SC") ? " selected" : ""; ?>>South Carolina</option>
                        <option value="SD"<?php echo ($up->getState() == "SD") ? " selected" : ""; ?>>South Dakota</option>
                        <option value="TN"<?php echo ($up->getState() == "TN") ? " selected" : ""; ?>>Tennessee</option>
                        <option value="TX"<?php echo ($up->getState() == "TX") ? " selected" : ""; ?>>Texas</option>
                        <option value="UT"<?php echo ($up->getState() == "UT") ? " selected" : ""; ?>>Utah</option>
                        <option value="VT"<?php echo ($up->getState() == "VT") ? " selected" : ""; ?>>Vermont</option>
                        <option value="VA"<?php echo ($up->getState() == "VA") ? " selected" : ""; ?>>Virginia</option>
                        <option value="WA"<?php echo ($up->getState() == "WA") ? " selected" : ""; ?>>Washington</option>
                        <option value="WV"<?php echo ($up->getState() == "WV") ? " selected" : ""; ?>>West Virginia</option>
                        <option value="WI"<?php echo ($up->getState() == "WI") ? " selected" : ""; ?>>Wisconsin</option>
                        <option value="WY"<?php echo ($up->getState() == "WY") ? " selected" : ""; ?>>Wyoming</option>
        			</select>
        		    <label for="State" class="form-label">State:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Zip Code" name="ZipCode" id="ZipCode" value = "<?php echo $up->getPostal_code(); ?>" required>
        		    <label for="ZipCode" class="form-label">Zip Code:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
	<hr>

	<div id="SkillSet">
    <h3>Skill Set:</h3>
    <button onclick="addSkills()">Add</button>
<script>
function addSkills() {
    document.getElementById("SkillSet").innerHTML += 
      "<div class='row'><div class='col-1'></div><div class='col-5'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Skill Name' name='SkillName[]' id='SkillName' required><label for='SkillName' class='form-label'>Skill Name:</label></div></div><div class='col-5'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Description' name='SkillDescription[]' id='SkillDescription' required><label for='SkillDescription' class='form-label'>Description:</label></div></div><div class='col-1'></div></div>";
}
</script>
    
<?php
    // Loop through all existing and then add as needed 
    foreach($us as $item)
    {
        echo "	<div class='row'>\n";
        echo "		<div class='col-1'></div>\n";
        echo "		<div class='col-5'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Skill Name' name='SkillName[]' id='SkillName' value = '" . $item->getSkill_name() . "' >\n";
        echo "    		    <label for='SkillName' class='form-label'>Skill Name:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-5'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Description' name='SkillDescription[]' id='SkillDescription' value = '" . $item->getDescription() . "' >\n";
        echo "    		    <label for='SkillDescription' class='form-label'>Description:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-1'></div>\n";
        echo "	</div>\n";
    }
?>
	</div>
	<hr>

	<div id="Education">
    <h3>Education:</h3>
    <button onclick="addEducation()">Add</button>
<script>
function addEducation() {
    document.getElementById("Education").innerHTML += 
      "<div class='row'><div class='col-1'></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Organization Name' name='OrgName[]' id='OrgName' required><label for='OrgName' class='form-label'>Organization Name:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Degree' name='Degree[]' id='Degree' required><label for='Degree' class='form-label'>Degree:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='date' class='form-control' name='EStartDate[]' id='EStartDate' required><label for='EStartDate' class='form-label'>Start Date:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='date' class='form-control' name='EEndDate[]' id='EEndDate' required><label for='EEndDate' class='form-label'>End Date:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><select class='form-select' aria-label='Default select example' name='Graduated[]' id='Graduated' required><option value='0'>No</option><option value='1'>Yes</option></select><label for='Graduated' class='form-label'>Graduated:</label></div></div><div class='col-1'></div></div>";
}
</script>
<?php 
    // Loop through all existing and then add as needed
    foreach($ue as $item)
    {
        echo "	<div class='row'>\n";
        echo "		<div class='col-1'></div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Organization Name' name='OrgName[]' id='OrgName' value = '" . $item->getOrg_name() . "' >\n";
        echo "    		    <label for='OrgName' class='form-label'>Organization Name:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Degree' name='Degree[]' id='Degree' value = '" . $item->getDegree_description() . "' >\n";
        echo "    		    <label for='Degree' class='form-label'>Degree:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='date' class='form-control' name='EStartDate[]' id='EStartDate' value = '" . $item->getStart_date() . "' >\n";
        echo "    		    <label for='EStartDate' class='form-label'>Start Date:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='date' class='form-control' name='EEndDate[]' id='EEndDate' value = '" . $item->getEnd_date() . "' >\n";
        echo "    		    <label for='EEndDate' class='form-label'>End Date:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "              <select class='form-select' aria-label='Default select example' name='Graduated[]' id='Graduated'>\n";
        echo "                  <option value='0' ";
        echo ($item->getGraduated() == 0) ? "selected" : "";
        echo ">No</option>\n";
        echo "                  <option value='1' ";
        echo ($item->getGraduated() == 1) ? "selected" : "";
        echo ">Yes</option>\n";
        echo "              </select>\n";
        echo "              <label for='Graduated' class='form-label'>Graduated:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-1'></div>\n";
        echo "	</div>\n";
    }
?>
	</div>
	<hr>

	<div id="WorkHistory">
    <h3>Work Experience History :</h3>
    <button onclick="addWorkHistory()">Add</button>
<script>
function addWorkHistory() {
    document.getElementById("WorkHistory").innerHTML += 
      "<div class='row'><div class='col-1'></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Business Name' name='BusinessName[]' id='BusinessName' required><label for='BusinessName' class='form-label'>Business Name:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Title: Duties' name='WorkDescription[]' id='WorkDescription' required><label for='WorkDescription' class='form-label'>Work Description:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='date' class='form-control' name='WStartDate[]' id='WStartDate' required><label for='WStartDate' class='form-label'>Start Date:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='date' class='form-control' name='WEndDate[]' id='WEndDate' required><label for='WEndDate' class='form-label'>End Date:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><select class='form-select' aria-label='Default select example' name='Current[]' id='Current' required><option value='0'>No</option><option value='1'>Yes</option></select><label for='Current' class='form-label'>Current:</label></div></div><div class='col-1'></div></div>";
}
</script>
<?php 
    // Loop through all existing and then add as needed
    foreach($uw as $item)
    {
        echo "	<div class='row'>\n";
        echo "		<div class='col-1'></div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Business Name' name='BusinessName[]' id='BusinessName' value = '" . $item->getOrg_name() . "' >\n";
        echo "    		    <label for='BusinessName' class='form-label'>Business Name:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='text' class='form-control' placeholder='Title: Duties' name='WorkDescription[]' id='WorkDescription' value = '" . $item->getWork_description() . "' >\n";
        echo "    		    <label for='WorkDescription' class='form-label'>Work Description:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='date' class='form-control' name='WStartDate[]' id='WStartDate' value = '" . $item->getStart_date() . "' >\n";
        echo "    		    <label for='WStartDate' class='form-label'>Start Date:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "    		    <input type='date' class='form-control' name='WEndDate[]' id='WEndDate' value = '" . $item->getEnd_date() . "' >\n";
        echo "    		    <label for='WEndDate' class='form-label'>End Date:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-2'>\n";
        echo "			<div class='form-floating mb-3 mt-3'>\n";
        echo "              <select class='form-select' aria-label='Default select example' name='Current[]' id='Current'>\n";
        echo "                  <option value='0' ";
        echo ($item->getCurrently_employed() == 0) ? "selected" : "";
        echo ">No</option>\n";
        echo "                  <option value='1' ";
        echo ($item->getCurrently_employed() == 1) ? "selected" : "";
        echo ">Yes</option>\n";
        echo "              </select>\n";
        echo "              <label for='Current' class='form-label'>Current:</label>\n";
        echo "			</div>\n";
        echo "		</div>\n";
        echo "		<div class='col-1'></div>\n";
        echo "	</div>\n";
    }
?>
	</div>
	<br>

		<div align='center'>
<?php
if ($user->getRole_id() == 2 || $user->getRole_id() == 4)
{
    echo "<hr>\n";
    echo "<h3>Administrator Options:</h3>\n";
    echo "<div class='row'>\n";
    echo "	<div class='col-1'></div>\n";
    echo "	<div class='col-5'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<select class='form-select' aria-label='Default select example' name='RoleID' id='RoleID'>\n";
    
    $r = $rolesList;
    for ($x = 0; $x < count($r); ++$x)
    {
        echo "                  <option value='" . ($x + 1) . "' ";
        echo ($r[$x]->getId() == $um->getRole_id()) ? "selected" : "";
        echo ">" . $r[$x]->getRoleName() . "</option>\n";
    }
    
    echo "			</select>\n";
    echo "			<label for='RoleID' class='form-label'>Role:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    
    echo "	<div class='col-5'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<select class='form-select' aria-label='Default select example' name='Suspended' id='Suspended'>\n";
    echo "                  <option value='0' ";
    echo ($um->getSuspended() == 0) ? "selected" : "";
    echo ">No</option>\n";
    echo "                  <option value='1' ";
    echo ($um->getSuspended() == 1) ? "selected" : "";
    echo ">Yes</option>\n";
    echo "			</select>\n";
    echo "			<label for='Suspended' class='form-label'>Suspended:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-1'></div>\n";
    echo "</div>\n";
    echo "<hr>\n";
    
}
else
{
    echo "			<input type='hidden' name='RoleID' id='RoleID' value='" . $um->getRole_id() . "'>\n";
    echo "			<input type='hidden' name='Suspended' id='Suspended' value='" . $um->getSuspended() . "'>\n";
}
?>
			<input type="hidden" name="UserID" id="UserID" value="<?php echo $um->getId(); ?>">
			<button type="submit" class="btn btn-primary">Save Changes</button>
		</div>
	</form>
</div>

@endsection

