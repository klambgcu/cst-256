<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-14
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Security
 * 2. Authenicate Login
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Data;

use Illuminate\Support\Facades\DB;

class SecurityDAO
{

    public function __construct()
    {}
    
    public function AuthenicateLogin($email, $password)
    {
        $exists = DB::select('SELECT COUNT(1) AS AMOUNT FROM users WHERE EMAIL = ? AND PASSWORD = ? AND SUSPENDED = 0', [$email, $password]);
        
        return ($exists[0]->AMOUNT > 0);
    }
}

