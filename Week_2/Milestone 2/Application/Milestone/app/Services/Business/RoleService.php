<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-14
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service - User
 * 2. Various CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Business;

use App\Services\Data\RoleDAO;

class RoleService
{

    public function __construct()
    {}
    
    public function createRole($roleModel)
    {
        $dao = new RoleDAO();
        return $dao->createRole($roleModel);
    }
    
    public function getAllRoles()
    {
        $dao = new RoleDAO();
        return $dao->getAllRoles();        
    }

    public function getRoleByID($role_id)
    {
        $dao = new RoleDAO();
        return $dao->getRoleByID($role_id);
    }
    
    public function updateRole($roleModel)
    {
        $dao = new RoleDAO();
        return $dao->updateRole($roleModel);
    }

    public function deleteRole($role_id)
    {
        $dao = new RoleDAO();
        return $dao->deleteRole($role_id);
    }
}