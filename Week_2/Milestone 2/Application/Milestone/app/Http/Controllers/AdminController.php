<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Controller
 * 2. Handles Listing Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

namespace App\Http\Controllers;

use App\Services\Business\UserService;
use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserAggregateModel;
use App\Models\UserSkillsetModel;
use App\Models\UserEducationModel;
use App\Models\UserWorkHistoryModel;
use App\Services\Business\RoleService;

class AdminController extends Controller
{
    // Display the admin user listing
    public function showAdminUserListing()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        $service = new UserService();
        $usersList = $service->getAllUsers();

        return view('adminUserListing')->with(['usersList' => $usersList]);
    }

    
    // Admin possibly altering user
    public function showMemberProfile(Request $request)
    {        
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        $user_id = $request->input('id');
        $mode    = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen user
            $service = new UserService();
            $userAggregateModel = $service->getUserAggregate($user_id);
            
            $roleService = new RoleService();
            $rolesList = $roleService->getAllRoles();
            $data = ['userAggregateModel' => $userAggregateModel, 'rolesList' => $rolesList];
            return view('memberProfile')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete user
            $service = new UserService();
            $service->deleteUser($user_id);

           // $usersList = $service->getAllUsers();
            
            //return view('adminUserListing')->with(['usersList' => $usersList]);
            return redirect('adminUser');
        }
        else 
        {
            // Something wrong mode (0,1) only - redirect to welcome
            return view('welcome');
        }
        
    }
}
