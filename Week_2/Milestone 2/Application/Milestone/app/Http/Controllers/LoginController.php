<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handle Login Functionality
 * 3. Added Main Menu requirement
 * ---------------------------------------------------------------
 */

namespace App\Http\Controllers;

use App\Services\Business\SecurityService;
use App\Services\Business\UserService;

// use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    //Login Method
    public function login()
    {
        session_start();

        // store registration parameters
        $email    = filter_input(INPUT_POST,'Email');
        $password = filter_input(INPUT_POST,'Password');

        // Validate Login
        $service = new SecurityService();
        $valid = $service->AuthenicateLogin($email, $password);
        
        // Determine if login successful
        if ($valid)
        {
            // Valid user
            $user_service = new UserService();
            $user = $user_service->getUserByEmail($email);
            $_SESSION["message"] = "Welcome " . $user->getFirst_name() . " " . $user->getLast_name() . ".";
            $_SESSION['principal'] = $user;
        }
        else
        {
            // Invalid user
            $_SESSION["message"] = "Login was unsuccessful - please try again.";
            unset($_SESSION['principal']);
        }

        return view('loginstatus');
       
// $x = new UserDAO();
// $a5 = $x->getUserAggregate(5);
// $a4 = $x->getUserAggregate(4);
// $a4->setUserSkillset($a5->getUserSkillset());
// $x->updateUserByAggregate($a4);
// echo "<pre>\n";
// //print_r($x->getAllUsers());
// print_r($x->getUserAggregate(4));
// echo "<hr>";           
// echo "</pre>\n";
        
    }
    
    public function logout()
    {
        session_start();
        
        // remove all session variables
        session_unset();
        
        // destroy the session
        session_destroy();
        
        return view('welcome');
    }
}
