-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2021 at 06:13 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-256`
--
CREATE DATABASE IF NOT EXISTS `cst-256` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-256`;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLENAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Normal User', 'A Customer'),
(2, 'User Maintenance', 'User Maintenance'),
(3, 'Group Maintenance', 'Group Maintenance'),
(4, 'Admin', 'Administrator Users and Groups');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `BIRTHDATE` date NOT NULL,
  `GENDER` tinyint(1) NOT NULL,
  `ROLE_ID` int(11) NOT NULL DEFAULT '1',
  `SUSPENDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  KEY `ROLE_ID_idx` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `BIRTHDATE`, `GENDER`, `ROLE_ID`, `SUSPENDED`) VALUES
(1, 'User', 'Maint', 'umaint@getjobs.com', '(111) 111-1111', '11111111', '2000-01-01', 0, 2, 0),
(2, 'Group', 'Maint', 'gmaint@getjobs.com', '(222) 222-2222', '22222222', '2000-01-01', 0, 3, 0),
(3, 'Ad', 'min', 'admin@getjobs.com', '(333) 333-3333', '33333333', '2000-01-01', 0, 4, 0),
(4, 'Kelly', 'Lamb', 'kl@kl.com', '(562) 555-1234', '12345678', '1965-05-01', 0, 1, 0),
(5, 'Test1', 'Test2', 'test@test.com', '(123) 123-1234', '12345678', '2021-12-14', 0, 1, 0),
(6, 'Deena', 'Lamb', 'dl@dl.com', '(123) 456-7890', '11111111', '1970-07-20', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_education`
--

DROP TABLE IF EXISTS `users_education`;
CREATE TABLE IF NOT EXISTS `users_education` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ORG_NAME` varchar(100) NOT NULL,
  `DEGREE_DESCRIPTION` varchar(100) NOT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `GRADUATED` tinyint(1) NOT NULL,
  `LINK_TO_ORG` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_education`
--

TRUNCATE TABLE `users_education`;
--
-- Dumping data for table `users_education`
--

INSERT INTO `users_education` (`ID`, `USER_ID`, `ORG_NAME`, `DEGREE_DESCRIPTION`, `START_DATE`, `END_DATE`, `GRADUATED`, `LINK_TO_ORG`) VALUES
(2, 5, 'abc', 'abc123', '2021-12-01', '2021-12-02', 1, 'link abc'),
(8, 4, 'abc', 'abc123', '2021-12-01', '2021-12-02', 1, 'link abc');

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
CREATE TABLE IF NOT EXISTS `users_profile` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `PHOTO` varchar(100) NOT NULL DEFAULT 'default_member_photo.jpg',
  `DESCRIPTION` varchar(500) NOT NULL,
  `STREET` varchar(100) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `STATE` varchar(100) NOT NULL,
  `POSTAL_CODE` varchar(100) NOT NULL,
  `COUNTRY` varchar(100) NOT NULL DEFAULT 'USA',
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_profile`
--

TRUNCATE TABLE `users_profile`;
--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`ID`, `USER_ID`, `PHOTO`, `DESCRIPTION`, `STREET`, `CITY`, `STATE`, `POSTAL_CODE`, `COUNTRY`) VALUES
(1, 1, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(2, 2, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(3, 3, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(4, 4, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(5, 5, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(6, 6, 'default_member_photo.jpg', '', '', '', '', '', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `users_skillset`
--

DROP TABLE IF EXISTS `users_skillset`;
CREATE TABLE IF NOT EXISTS `users_skillset` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SKILL_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_skillset`
--

TRUNCATE TABLE `users_skillset`;
--
-- Dumping data for table `users_skillset`
--

INSERT INTO `users_skillset` (`ID`, `USER_ID`, `SKILL_NAME`, `DESCRIPTION`) VALUES
(1, 5, 'JAVA', 'J2EE'),
(2, 5, 'C#', 'C#'),
(3, 5, 'PHP', 'PHP'),
(16, 4, 'JAVA', 'J2EE'),
(17, 4, 'C#', 'C#'),
(18, 4, 'PHP', 'PHP');

-- --------------------------------------------------------

--
-- Table structure for table `users_work_history`
--

DROP TABLE IF EXISTS `users_work_history`;
CREATE TABLE IF NOT EXISTS `users_work_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ORG_NAME` varchar(100) NOT NULL,
  `WORK_DESCRIPTION` varchar(500) NOT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `CURRENTLY_EMPLOYED` tinyint(1) NOT NULL,
  `LINK_TO_ORG` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_work_history`
--

TRUNCATE TABLE `users_work_history`;
--
-- Dumping data for table `users_work_history`
--

INSERT INTO `users_work_history` (`ID`, `USER_ID`, `ORG_NAME`, `WORK_DESCRIPTION`, `START_DATE`, `END_DATE`, `CURRENTLY_EMPLOYED`, `LINK_TO_ORG`) VALUES
(1, 5, 'abc', 'abc123', '2021-12-01', '2021-12-01', 1, 'link abc'),
(4, 4, 'abc', 'abc123', '2021-12-01', '2021-12-01', 1, 'link abc');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_education`
--
ALTER TABLE `users_education`
  ADD CONSTRAINT `USERS_ID_FK3` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD CONSTRAINT `USERS_ID_FK1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_skillset`
--
ALTER TABLE `users_skillset`
  ADD CONSTRAINT `USERS_ID_FK` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_work_history`
--
ALTER TABLE `users_work_history`
  ADD CONSTRAINT `USERS_ID_FK2` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
