<?php
namespace App\Services\Data;

use App\Models\UserModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Security DAO (Data Access Object)
 * 2. Support Authentication Functionalities
 * 3.
 * ---------------------------------------------------------------
 */

class SecurityDAO
{

    public function __construct()
    {}

    public function findByUser($userModel)
    {
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            die("Connection failed: " . $connection->connect_error);
        }
        
        // prepare and bind
        $count = 0;
        if ($statement = $connection->prepare("SELECT COUNT(*) FROM USERS WHERE USERNAME = ? AND PASSWORD = ?") )
        {
            $u = $userModel->getUsername();
            $p = $userModel->getPassword();
            $statement->bind_param('ss', $u, $p);
            $statement->execute();
            $statement->bind_result($count);
            $statement->fetch();
            $statement->close();
        }
        else 
        {
            var_dump($connection);
        }
        $connection->close();
        
        return ($count > 0) ? true : false;
    }
}

