<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Services\Business\SecurityService;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handles Login Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $userModel = new UserModel($username, $password);
        
        $service = new SecurityService();
        $results = $service->login($userModel);
        
        if ($results)
        {
            $data = ['userModel' => $userModel];
            return view('loginPassed2')->with($data);
        }
        else 
        {
            return view('loginFailed');
        }
        
    }
}
