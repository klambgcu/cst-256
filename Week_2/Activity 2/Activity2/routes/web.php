<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Routing Configuration
 * 2. Handle routing
 * 3. 
 * ---------------------------------------------------------------
 */

// -----------------------------------------------------------------
// Activity 2.1 Section
// -----------------------------------------------------------------

Route::get('/', function () { return view('welcome'); });
Route::post('/whoami', 'WhatsMyNameController@index');
Route::get('/askme', function () { return view('whoami'); }); 


// -----------------------------------------------------------------
// Activity 2.2 Section
// -----------------------------------------------------------------
Route::get('/login', function () { return view('login'); });
Route::post('/dologin','LoginController@index');


// -----------------------------------------------------------------
// Activity 2.3 Section
// -----------------------------------------------------------------
Route::get('/login2', function () { return view('login2'); });






