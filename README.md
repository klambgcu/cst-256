# README #

Name: Kelly Lamb
Class: CST-256 Database Application Programming III
University: GCU (Grand Canyon University)

### What is this repository for? ###

Weekly Activity Assignments
Weekly Milestone Assignments
Documentation for both.

### How do I get set up? ###

Download / Install
Latest version of MAMP
Latest version of Eclipse PHP
Configure X-Debug, Composer, PDT, ANT (or something similar for deployment)

### Contribution guidelines ###

Personal/team member usage for school assignment


