<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 202-01-02
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Routing Configuration
 * 2. Handle routing
 * 3.
 * ---------------------------------------------------------------
 */


// -----------------------------------------------------------------
// Activity 2.1 Section
// -----------------------------------------------------------------

// Route::get('/', function () { return view('welcome'); });
// Route::post('/whoami', 'WhatsMyNameController@index');
// Route::get('/askme', function () { return view('whoami'); });


// -----------------------------------------------------------------
// Activity 2.2 Section
// -----------------------------------------------------------------
// Route::get('/login', function () { return view('login'); });
// Route::post('/dologin','LoginController@index');


// -----------------------------------------------------------------
// Activity 2.3 Section
// -----------------------------------------------------------------
// Route::get('/login2', function () { return view('login2'); });


// -----------------------------------------------------------------
// Activity 3.1 Section
// -----------------------------------------------------------------
Route::get('/login3', function () { return view('login3'); });
Route::post('/dologin3', 'Login3Controller@index');

// -----------------------------------------------------------------
// Activity 5.1 Section
// -----------------------------------------------------------------
Route::get('/', function () { return view('index'); });

// -----------------------------------------------------------------
// Activity 5.2 Section
// -----------------------------------------------------------------
Route::resource('/usersrest', 'UsersRestController');
Route::resource('/testuserapi', 'RestClientController');

// -----------------------------------------------------------------
// Activity 5.3 Section
// -----------------------------------------------------------------
Route::get('/login5', function () { return view('login5'); });
Route::post('/dologin5', 'Login5Controller@index');

// -----------------------------------------------------------------
// Activity 5.4 Section
// -----------------------------------------------------------------
Route::get('/loggingservice', 'TestLoggingController@index');




