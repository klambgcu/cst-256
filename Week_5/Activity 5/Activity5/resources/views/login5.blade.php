<?php 

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. login view
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */
?>

<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Login Page')
@section('content')

<!-- Display all the Data Validation Rule Errors -->
<?php
    if($errors->count() != 0)
    {
        echo "<h5>List of Errors</h5>";
        foreach($errors->all() as $message)
        {
            echo $message . "<br/>";
        }
    }
?>

    <form action = "dologin5" method = "POST">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
        <h2>Login</h2>
        <table>
        	<tr>
        		<td>User Name: </td>
        		<td><input type = "text" name = "username" /><?php echo $errors->first('username')?></td>
        	</tr>
        
        	<tr>
        		<td>Password:</td>
        		<td><input type = "password" name = "password" /><?php echo $errors->first('password')?></td>
        	</tr>
        	<tr>
        		<td colspan = "2" align = "center">
        			<input type = "submit" value = "Login" />
        		</td>
        </table>
	</form>

@endsection
