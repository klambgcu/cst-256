<?php
namespace App\Services\Utility;

use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-29
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Logging Mechanism Concrete - use MonoLogger
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

class MyLogger2 implements ILogger
{
    protected static $instance;
    
    private function __construct()
    {}
    
    public static function getLogger()
    {
        if (! self::$instance)
        {
            self::configureInstance();
        }
        
        return self::$instance;
    }
    
    protected static function configureInstance()
    {
        $dir = storage_path() . DIRECTORY_SEPARATOR . 'logs';
        
        if (!file_exists($dir)){
            mkdir($dir, 0777, true);
        }
        
        $formatter = new LineFormatter();
        $formatter->ignoreEmptyContextAndExtra();
        $streamHandler = new RotatingFileHandler($dir . DIRECTORY_SEPARATOR . 'myapp.log', 5);
        $streamHandler->setFormatter($formatter);
        $logger = new Logger('myapp.log');
        $logger->pushHandler($streamHandler);
        
        self::$instance = $logger;
    }
    
    public static function debug($message, array $context = array())
    {
        self::getLogger()->addDebug($message, $context);
    }

    public static function warning($message, array $context = array())
    {
        self::getLogger()->addWarning($message, $context);
    }

    public static function error($message, array $context = array())
    {
        self::getLogger()->addError($message, $context);
    }

    public static function info($message, array $context = array())
    {
        self::getLogger()->addInfo($message, $context);
    }
}

