<?php
namespace App\Services\Business;

use App\Services\Data\SecurityDAO;
use Illuminate\Support\Facades\Log;
use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;
use Exception;

class SecurityService
{

    public function __construct()
    {}
    
    public function login($userModel)
    {
        MyLogger2::info("Entering SecurityService::login()");
        
        try
        {
            $dao = new SecurityDAO();
            $results = $dao->findByUser($userModel);
            
            MyLogger2::info("Exiting SecurityService::login()");
            
            return $results;
        }
        catch (Exception $e)
        {
            MyLogger2::error("Exception SecurityService::login()" . $e->getMessage());
        }        
    }
    
    public function getAllUsers()
    {
        MyLogger2::info("Entering SecurityService::getAllUsers()");
        
        try
        {
            $dao = new SecurityDAO();
            $results = $dao->findAllUsers();
            
            MyLogger2::info("Exiting SecurityService::getAllUsers()");
            
            return $results;
        }
        catch (Exception $e)
        {
            MyLogger2::error("Exception SecurityService::getAllUsers()" . $e->getMessage());
        }
    }

    public function getUser($id)
    {
        MyLogger2::info("Entering SecurityService::getUser()");
        MyLogger2::info("Parameter is: ", array("user id" => $id) );
        
        try
        {
            $dao = new SecurityDAO();
            $results = $dao->findUserByID($id);
            
            MyLogger2::info("Exiting SecurityService::getUser()");
            
            return $results;
        }
        catch (Exception $e)
        {
            MyLogger2::error("Exception SecurityService::getUser()" . $e->getMessage());
        }
    }
}