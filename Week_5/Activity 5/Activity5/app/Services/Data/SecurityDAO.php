<?php
namespace App\Services\Data;

use App\Models\UserModel;
use Illuminate\Support\Facades\Log;
use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;
/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Security DAO (Data Access Object)
 * 2. Support Authentication Functionalities
 * 3.
 * ---------------------------------------------------------------
 */

class SecurityDAO
{
    public function __construct()
    {}

    /**
     * Determine if the user exists in the database for based
     * upon username and password for login security. 
     * @param \App\Models\UserModel $userModel
     * @return boolean
     */
    public function findByUser($userModel)
    {
        MyLogger2::info("Entering SecurityDAO::findByUser()");
        MyLogger2::info("Parameters are: ",array("userModel.username" => $userModel->getUsername(), "userModel.password" => $userModel->getPassword()));
        
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            MyLogger2::error("Exception SecurityDAO::findByUser()" . $connection->error);
            die("Connection failed: " . $connection->connect_error);
        }
        
        // prepare and bind
        $count = 0;
        if ($statement = $connection->prepare("SELECT COUNT(*) FROM USERS WHERE USERNAME = ? AND PASSWORD = ?") )
        {
            $u = $userModel->getUsername();
            $p = $userModel->getPassword();
            $statement->bind_param('ss', $u, $p);
            $statement->execute();
            $statement->bind_result($count);
            $statement->fetch();
            $statement->close();
        }
        else 
        {
            MyLogger2::error("Exception SecurityDAO::findByUser()" . $connection->error);
            var_dump($connection);
        }
        $connection->close();
        
        MyLogger2::info("Exiting SecurityDAO::findByUser()");
        
        return ($count > 0) ? true : false;
    }

    /**
     * Get all the users from the database and return a list of user models
     * @return \App\Models\UserModel[]
     */
    public function findAllUsers()
    {
        MyLogger2::info("Entering SecurityDAO::findAllUsers()");
        
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            MyLogger2::error("Exception SecurityDAO::findAllUsers()" . $connection->error);
            die("Connection failed: " . $connection->connect_error);
        }
        
        // prepare and bind
        $index = 0;
        $list = array();
        
        if ($statement = $connection->prepare("SELECT * FROM USERS ORDER BY ID ASC") )
        {
            $statement->execute();
            $result = $statement->get_result();

            while ($row = $result->fetch_assoc())
            {
                $user = new UserModel($row['ID'], $row['USERNAME'], $row['PASSWORD']);
                $list[$index] = $user;
                $index++;
            }

            $statement->close();
        }
        else
        {
            MyLogger2::error("Exception SecurityDAO::findAllUsers()" . $connection->error);
            var_dump($connection);
        }
        
        $connection->close();
        
        MyLogger2::info("Exiting SecurityDAO::findAllUsers()");
        
        return $list;
    }

    public function findUserByID($id)
    {
        MyLogger2::info("Entering SecurityDAO::findUserByID()");
        MyLogger2::info("Parameters are: ", array("user id" => $id) );
        
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            MyLogger2::error("Exception SecurityDAO::findUserByID()" . $connection->error);
            die("Connection failed: " . $connection->connect_error);
        }
        
        // Create an empty/default user 
        $user = new UserModel(0, "", "");

        // prepare and bind
        if ($statement = $connection->prepare("SELECT * FROM USERS WHERE ID = ?") )
        {
            $statement->bind_param('i', $id);
            $statement->execute();
            $result = $statement->get_result();
            
            if ($row = $result->fetch_assoc())
            {
                // If found, override empty user from above.
                $user = new UserModel($row['ID'], $row['USERNAME'], $row['PASSWORD']);
            }
            $statement->close();
        }
        else
        {
            MyLogger2::error("Exception SecurityDAO::findUserByID()" . $connection->error);
            var_dump($connection);
        }
        
        $connection->close();
        
        MyLogger2::info("Exiting SecurityDAO::findUserByID()");
        
        return $user;
    }
}

