<?php
namespace App\Models;

use JsonSerializable;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-17
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. User Model
 * 2. Support User and Login Functionalities
 * 3.
 * ---------------------------------------------------------------
 */

class UserModel implements JsonSerializable
{
    private $id;
    private $username;
    private $password;
    
    public function __construct($id, $username, $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this); 
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}