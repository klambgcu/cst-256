<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Utility\ILoggerService;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-02
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handles Login Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class TestLoggingController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    public function index(Request $request)
    {
        $this->logger->info("Entering TestLoggingController::index()");
    
        $this->logger->info("Exiting TestLoggingController::index()");
        
        return "TestLoggingController::index()";
    }
}
