<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;

class RestClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Call Rest API - Separate instance running - otherwise it freezes up
        $serviceURL = "http://localhost/Activity5/public/";
        $api = "usersrest";
        $param = "";
        $uri = $api . "/". $param;
                
        try
        {
            // Make REST call
            $client = new Client(['base_uri' => $serviceURL]);
            $response = $client->request('GET', $uri);
            
            // Return JSON or Error
            if ($response->getStatusCode() == 200)
                return (new Response($response->getBody(), 200))->header('Content-Type', 'application/json; charset=utf-8');
            else
                return "There was an error: " . $response->getStatusCode();
        }
        catch(ClientException $e)
        {
            // Return an error
            return "There was an exception: " . $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Call Rest API - Separate instance running - otherwise it freezes up
        $serviceURL = "http://localhost/Activity5/public/";
        $api = "usersrest";
        $param = $id;
        $uri = $api . "/". $param;
        
        try
        {
            // Make REST call
            $client = new Client(['base_uri' => $serviceURL]);
            $response = $client->request('GET', $uri);
            
            // Return JSON or Error
            if ($response->getStatusCode() == 200)
                return (new Response($response->getBody(), 200))->header('Content-Type', 'application/json; charset=utf-8');
            else
                return "There was an error: " . $response->getStatusCode();
        }
        catch(ClientException $e)
        {
            // Return an error
            return "There was an exception: " . $e->getMessage();
        }
    }
}
