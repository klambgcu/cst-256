<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Services\Business\SecurityService;
use App\Models\DTO;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-01
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Users Rest Controller
 * 2. Handles API Calls for User Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class UsersRestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = new SecurityService();
        $data = $service->getAllUsers();

        $results = new DTO(200, "List of users: count = " . count($data), $data);
        $content = json_encode($results);
        
        return (new Response($content, 200))
        ->header('Content-Type', 'application/json; charset=utf-8');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = new SecurityService();
        $data = $service->getUser($id);
        
        if ($data->getId() == 0)
        {
            $data->setId($id);
            $status = 404;
            $results = new DTO($status, "User not found: id = " . $id, $data);
        }
        else 
        {
            $status = 200;
            $results = new DTO($status, "User information for id = " . $id, $data);
        }
        
        $content = json_encode($results);
        
        return (new Response($content, $status))
        ->header('Content-Type', 'application/json; charset=utf-8');
    }
}
