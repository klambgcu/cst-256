<?php

namespace App\Http\Middleware;

use App\Services\Utility\MyLogger2;
use Illuminate\Support\Facades\Cache;
use Closure;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-02
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handles Login Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class MyTestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        MyLogger2::info("Entering MyTestMiddleware::handle()");

        $username = Cache::get( 'mydata', '**NO CACHE VALUE YET**' );
        MyLogger2::info("**** MyTestMiddleware::handle() - mydata cache: ", array("username" => $username));
        
        return $next($request);
    }
}
