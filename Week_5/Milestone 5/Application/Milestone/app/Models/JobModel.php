<?php
namespace App\Models;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold job posting information
 * 2. Job Postings
 * 3.
 * ---------------------------------------------------------------
 */

class JobModel
{
    private $id;
    private $organization_id;
    private $location_id;
    private $position_name;
    private $description;
    private $position_type;
    private $expire_date;
    private $skills_keywords;
    private $education_keywords;
    private $org_name;
    private $org_loc_name;
    
    public function __construct($id, $organization_id, $location_id, $position_name, $description, $position_type, $expire_date, $skills_keywords, $education_keywords, $org_name, $org_loc_name)
    {
        $this->id = $id;
        $this->organization_id = $organization_id;
        $this->location_id = $location_id;
        $this->position_name = $position_name;
        $this->description = $description;
        $this->position_type = $position_type;
        $this->expire_date = $expire_date;
        $this->skills_keywords = $skills_keywords;
        $this->education_keywords = $education_keywords;
        $this->org_name = $org_name;
        $this->org_loc_name = $org_loc_name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization_id()
    {
        return $this->organization_id;
    }

    /**
     * @return mixed
     */
    public function getLocation_id()
    {
        return $this->location_id;
    }

    /**
     * @return mixed
     */
    public function getPosition_name()
    {
        return $this->position_name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPosition_type()
    {
        return $this->position_type;
    }

    /**
     * @return mixed
     */
    public function getExpire_date()
    {
        return $this->expire_date;
    }

    /**
     * @return mixed
     */
    public function getSkills_keywords()
    {
        return $this->skills_keywords;
    }

    /**
     * @return mixed
     */
    public function getEducation_keywords()
    {
        return $this->education_keywords;
    }

    /**
     * @return mixed
     */
    public function getOrg_name()
    {
        return $this->org_name;
    }

    /**
     * @return mixed
     */
    public function getOrg_loc_name()
    {
        return $this->org_loc_name;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $organization_id
     */
    public function setOrganization_id($organization_id)
    {
        $this->organization_id = $organization_id;
    }

    /**
     * @param mixed $location_id
     */
    public function setLocation_id($location_id)
    {
        $this->location_id = $location_id;
    }

    /**
     * @param mixed $position_name
     */
    public function setPosition_name($position_name)
    {
        $this->position_name = $position_name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $position_type
     */
    public function setPosition_type($position_type)
    {
        $this->position_type = $position_type;
    }

    /**
     * @param mixed $expire_date
     */
    public function setExpire_date($expire_date)
    {
        $this->expire_date = $expire_date;
    }

    /**
     * @param mixed $skills_keywords
     */
    public function setSkills_keywords($skills_keywords)
    {
        $this->skills_keywords = $skills_keywords;
    }

    /**
     * @param mixed $education_keywords
     */
    public function setEducation_keywords($education_keywords)
    {
        $this->education_keywords = $education_keywords;
    }

    /**
     * @param mixed $org_name
     */
    public function setOrg_name($org_name)
    {
        $this->org_name = $org_name;
    }

    /**
     * @param mixed $org_loc_name
     */
    public function setOrg_loc_name($org_loc_name)
    {
        $this->org_loc_name = $org_loc_name;
    }
}

