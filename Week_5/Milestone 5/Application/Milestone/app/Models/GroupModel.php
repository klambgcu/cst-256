<?php
namespace App\Models;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold group information
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

class GroupModel
{
    private $id;
    private $creator_id;
    private $group_name;
    private $group_description;
    private $created_date;
    private $is_member;
    
    public function __construct($id, $creator_id, $group_name, $group_description, $created_date, $is_member)
    {
        $this->id = $id;
        $this->creator_id = $creator_id;
        $this->group_name = $group_name;
        $this->group_description = $group_description;
        $this->created_date = $created_date;
        $this->is_member = $is_member;
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreator_id()
    {
        return $this->creator_id;
    }

    /**
     * @return mixed
     */
    public function getGroup_name()
    {
        return $this->group_name;
    }

    /**
     * @return mixed
     */
    public function getGroup_description()
    {
        return $this->group_description;
    }

    /**
     * @return mixed
     */
    public function getCreated_date()
    {
        return $this->created_date;
    }

    /**
     * @return mixed
     */
    public function getIs_member()
    {
        return $this->is_member;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $creator_id
     */
    public function setCreator_id($creator_id)
    {
        $this->creator_id = $creator_id;
    }

    /**
     * @param mixed $group_name
     */
    public function setGroup_name($group_name)
    {
        $this->group_name = $group_name;
    }

    /**
     * @param mixed $group_description
     */
    public function setGroup_description($group_description)
    {
        $this->group_description = $group_description;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreated_date($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @param mixed $is_member
     */
    public function setIs_member($is_member)
    {
        $this->is_member = $is_member;
    }
}

