<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function assertLogin()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
}
