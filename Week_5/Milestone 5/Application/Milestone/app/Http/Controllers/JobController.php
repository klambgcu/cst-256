<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Business\JobListingService;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2023-01-16
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Job Controller
 * 2. Handles Job Search Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class JobController extends Controller
{
    // Display the initial job search form
    public function showJobSearch(Request $request)
    {
        if ($this->assertLogin()) return view('login');

        $jobsList = array();
        $data = ['jobsList' => $jobsList, 'position' => "", 'description' => "", 'skill' => ""];
        
        return view('jobListing')->with($data);
    }
    
    // Display the job search form using input
    public function doJobSearch(Request $request)
    {
        if ($this->assertLogin()) return view('login');

        $position    = $request->input('Position', "");
        $description = $request->input('Description', "");
        $skill       = $request->input('Skill', "");
        
        $service = new JobListingService();
        $jobsList = $service->searchJobs($position, $description, $skill);

        $data = ['jobsList' => $jobsList, 'position' => $position, 'description' => $description, 'skill' => $skill];
        
        return view('jobListing')->with($data);
    }
    
    public function doJobView(Request $request)
    {
        if ($this->assertLogin()) return view('login');
        
        $job_id = $request->input('id');
        
        $service = new JobListingService();
        $job = $service->getJobByID($job_id);
        $org = $service->getOrganizationByID($job->getOrganization_id());
        $loc = $service->getLocationByID($job->getLocation_id());
        
        $data = ['job' => $job, 'organization' => $org, 'location' => $loc];
        
        return view('jobView')->with($data);
    }
}
