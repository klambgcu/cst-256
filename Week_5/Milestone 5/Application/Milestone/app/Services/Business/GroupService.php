<?php
namespace App\Services\Business;

use App\Services\Data\GroupDAO;
use App\Services\Data\GroupMemberDAO;
use App\Services\Data\GroupPostDAO;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service - Groups
 * 2. Various CRUD - Groups, Posts, Members
 * 3.
 * ---------------------------------------------------------------
 */

class GroupService
{

    public function __construct()
    {}

    // -------------------------------------------------------------------
    // Business Logic Section
    // -------------------------------------------------------------------
    public function createUpdateGroupAndPosts($groupModel, $groupPostModelArray)
    {
        // Insert if group id = 0, otherwise update
        if ($groupModel->getId() > 0)
        {
            $this->updateGroup($groupModel);
            $this->deleteAllPostsForGroup($groupModel->getId());
            foreach ($groupPostModelArray as $p)
            {
                $this->createGroupPost($p);
            }
        }
        else 
        {
            $group_id = $this->createGroup($groupModel);
            foreach ($groupPostModelArray as $p)
            {
                $p->setGroup_id($group_id);
                $this->createGroupPost($p);
            }
        }
        return 1;        
    }
    
    
    // -------------------------------------------------------------------
    // Groups DAO Specific Methods
    // -------------------------------------------------------------------
    
    public function createGroup($groupModel)
    {
        $dao = new GroupDAO();
        return $dao->create($groupModel);
    }
    
    public function getAllGroups()
    {
        $dao = new GroupDAO();
        return $dao->getAll();
    }
    
    public function getGroupByID($group_id)
    {
        $dao = new GroupDAO();
        return $dao->getByID($group_id);
    }
    
    public function updateGroup($groupModel)
    {
        $dao = new GroupDAO();
        return $dao->update($groupModel);
    }
    
    public function deleteGroupByID($group_id)
    {
        $dao = new GroupDAO();
        return $dao->deleteByID($group_id);        
    }    

    public function getAllGroupsSetMemberTag($user_id)
    {
        $allGroups = $this->getAllGroups();
        $memberGroups = $this->getAllGroupsForMember($user_id);
        
        // loop through members to tag is_member on all groups
        foreach ($memberGroups as $m)
        {
            foreach ($allGroups as $a)
            {
                if ($a->getId() == $m->getGroup_id())
                {
                    $a->setIs_member(1);
                    break;
                }
            }
        }
        return $allGroups;
    }
    
    // -------------------------------------------------------------------
    // Groups Members DAO Specific Methods
    // -------------------------------------------------------------------
    
    public function createGroupMember($groupMemberModel)
    {
        $dao = new GroupMemberDAO();
        return $dao->create($groupMemberModel);
    }
    
    public function getAllGroupsMember()
    {
        $dao = new GroupMemberDAO();
        return $dao->getAll();
    }
    
    public function getAllGroupsForMember($user_id)
    {
        $dao = new GroupMemberDAO();
        return $dao->getAllGroupsForMember($user_id);
    }
    
    public function getAllMembersForGroup($group_id)
    {
        $dao = new GroupMemberDAO();
        return $dao->getAllMembersForGroup($group_id);
    }

    public function getAllMemberUserModelsForGroup($group_id)
    {
        $users = array();
        $index = 0;
        $members = $this->getAllMembersForGroup($group_id);
        $service = new UserService();

        foreach ($members as $m)
        {
            $user = $service->getUserByID($m->getUser_id());
            $users[$index] = $user;
            $index++;
        }
        return $users;
    }
    
    public function isGroupMember($user_id, $group_id)
    {
        $dao = new GroupMemberDAO();
        return $dao->isGroupMember($user_id, $group_id);
    }
    
    public function getByID($id)
    {
        $dao = new GroupMemberDAO();
        return $dao->getByID($id);
    }
    
    public function deleteGroupMemberByID($id)
    {
        $dao = new GroupMemberDAO();
        return $dao->deleteByID($id);
    }
    
    public function removeUserFromGroup($group_id, $user_id)
    {
        $dao = new GroupMemberDAO();
        return $dao->removeUserFromGroup($group_id, $user_id);
    }


    
    // -------------------------------------------------------------------
    // Groups Posts Specific Methods
    // -------------------------------------------------------------------
    
    public function createGroupPost($groupPostModel)
    {
        $dao = new GroupPostDAO();
        return $dao->create($groupPostModel);
    }
    
    public function getAllPosts()
    {
        $dao = new GroupPostDAO();
        return $dao->getAll();
    }
    
    public function getAllPostsForGroup($group_id)
    {
        $dao = new GroupPostDAO();
        return $dao->getAllPostsForGroup($group_id);
    }
    
    public function getPostByID($id)
    {
        $dao = new GroupPostDAO();
        return $dao->getByID($id);
    }
    
    public function updatePost($groupPostModel)
    {
        $dao = new GroupPostDAO();
        return $dao->update($groupPostModel);
    }
    
    public function deletePostByID($id)
    {
        $dao = new GroupPostDAO();
        return $dao->deleteByID($id);
    }
    
    public function deleteAllPostsForGroup($group_id)
    {
        $dao = new GroupPostDAO();
        return $dao->deleteAllPostsForGroup($group_id);
    }
}

