<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Locations (Job Postings)
 * 2. Various Location CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\LocationModel;

class LocationDAO
{    
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($locationModel)
    {
        $location_id = 0;
        $organization_id = $locationModel->getOrganization_id();
        $location_name = $locationModel->getLocation_name();
        $street = $locationModel->getStreet();
        $city = $locationModel->getCity();
        $state = $locationModel->getState();
        $postal_code = $locationModel->getPostal_code();
        $country = $locationModel->getCountry();
        
        $sql = 'INSERT INTO locations (ORGANIZATION_ID, LOCATION_NAME, STREET, CITY, STATE, POSTAL_CODE, COUNTRY) VALUES (?, ?, ?, ?, ?, ?, ?)';
        $data = [$organization_id, $location_name, $street, $city, $state, $postal_code, $country];
        DB::insert($sql, $data);
        $location_id = DB::getPdo()->lastInsertId();
        
        return $location_id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $locations = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM locations ORDER BY ID ASC');
        
        foreach ($rows as $row)
        {
            $location = new LocationModel($row->ID,
                                          $row->ORGANIZATION_ID,
                                          $row->LOCATION_NAME,
                                          $row->STREET,
                                          $row->CITY,
                                          $row->STATE,
                                          $row->POSTAL_CODE,
                                          $row->COUNTRY);
            $locations[$index] = $location;
            ++$index;
        }
        
        return $locations;
    }
    
    public function getByID($location_id)
    {
        $row = DB::select('SELECT * FROM locations WHERE ID = ?', [$location_id]);
        
        $location = new LocationModel($row[0]->ID,
                                      $row[0]->ORGANIZATION_ID,
                                      $row[0]->LOCATION_NAME,
                                      $row[0]->STREET,
                                      $row[0]->CITY,
                                      $row[0]->STATE,
                                      $row[0]->POSTAL_CODE,
                                      $row[0]->COUNTRY);
        
        return $location;
    }
    
    public function getLocationsByOrgID($organization_id)
    {
        $locations = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM locations WHERE ORGANIZATION_ID = ? ORDER BY ID ASC', [$organization_id]);
        
        foreach ($rows as $row)
        {
            $location = new LocationModel($row->ID,
                                          $row->ORGANIZATION_ID,
                                          $row->LOCATION_NAME,
                                          $row->STREET,
                                          $row->CITY,
                                          $row->STATE,
                                          $row->POSTAL_CODE,
                                          $row->COUNTRY);
            $locations[$index] = $location;
            ++$index;
        }
        
        return $locations;
    }
    
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function update($locationModel)
    {
        $id = $locationModel->getId();
        $organization_id = $locationModel->getOrganization_id();
        $location_name = $locationModel->getLocation_name();
        $street = $locationModel->getStreet();
        $city = $locationModel->getCity();
        $state = $locationModel->getState();
        $postal_code = $locationModel->getPostal_code();
        $country = $locationModel->getCountry();
        
        $sql = 'UPDATE locations SET ORGANIZATION_ID = ?, LOCATION_NAME = ?, STREET = ?, CITY = ?, STATE = ?, POSTAL_CODE = ?, COUNTRY = ? WHERE ID = ?';
        $data = [$organization_id, $location_name, $street, $city, $state, $postal_code, $country, $id];
        $count = DB::update($sql, $data);
        
        return $count;
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($location_id)
    {
        $count = DB::delete('DELETE FROM locations WHERE ID = ?', [$location_id]);
        
        return $count;
    }
    
}

