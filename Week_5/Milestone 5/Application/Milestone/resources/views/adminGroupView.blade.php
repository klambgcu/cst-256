<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-15
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Group View
 * 2. Create / Edit Group Entries
 * 3.
 * ---------------------------------------------------------------
 */

if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}

$user = $_SESSION['principal'];

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Group Page')
@section('content')

<div class="container">

	<form action="doAdminGroup" method="POST">
	@csrf
		<div align="center">
		    <h1>Group View Form</h1>
		    <p>Please enter group information.</p>
		    <hr>
		</div>
    	<div class="row">
    		<div class="col-1"></div>
    		<div class="col-4">
    			<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Group Name" name="GroupName" id="GroupName" value = "<?php echo $group->getGroup_name(); ?>" required>
        		    <label for="GroupName" class="form-label">Name:</label>
    			</div>
    		</div>
		</div>
    	<div class="row">
    		<div class="col-1"></div>
    		<div class="col-10">
    			<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Description" name="Description" id="Description" value = "<?php echo $group->getGroup_description(); ?>" required>
        		    <label for="Description" class="form-label">Description:</label>
    			</div>
    		</div>
    		<div class="col-1"></div>
    	</div>

    	<div id="Announcement">
    	<h3>Announcements:</h3>
        <button onclick="addAnnouncement()">Add</button>
        <script>
        function addAnnouncement() {
            document.getElementById("Announcement").innerHTML += 
              "<div class='row'><div class='col-1'></div><div class='col-10'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Announcement Post' name='Post[]' id='Post[]' value = '' required><input type='hidden' name='PostID[]' id='PostID[]' value = '0'><input type='hidden' name='CreatedTime[]' id='CreatedTime[]' value = ''><label for='Description' class='form-label'>Announcement Post:</label></div></div><div class='col-1'></div></div>";
        }
        </script>

		@foreach ($postsList as $p)
    		<div class='row'>
        		<div class='col-1'></div>
        		<div class='col-10'>
        			<div class='form-floating mb-3 mt-3'>
            		    <input type='text' class='form-control' placeholder='Announcement Post' name='Post[]' id='Post[]' value = '{{ $p->getPost() }}' required>
            		    <input type='hidden' name='PostID[]' id='PostID[]' value = '{{ $p->getId() }}'>
            		    <input type='hidden' name='CreatedTime[]' id='CreatedTime[]' value = '{{ $p->getCreated_time() }}'>
            		    <label for='Description' class='form-label'>Announcement Post:</label>
        			</div>
        		</div>
        		<div class='col-1'></div>
        	</div>
		@endforeach
		</div>
		<hr>
		<input type="hidden" name="GroupID" id="GroupID" value="{{ $group->getId() }}">
		<input type="hidden" name="CreatedDate" id="CreatedDate" value="{{ $group->getCreated_date() }}">
		<button type="submit" class="btn btn-primary">Save Changes</button>
	</form>
</div>

@endsection

	