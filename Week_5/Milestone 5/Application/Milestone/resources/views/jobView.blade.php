<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-16
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Job View
 * 2. View details for the job
 * 3.
 * ---------------------------------------------------------------
 */

if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}

$user = $_SESSION['principal'];
$type = ["Full-Time", "Part-Time", "Contract"];

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Job Details')
@section('content')

<div class="container">

	<form action="jobSearch" method="POST">
		@csrf
		<div align="center">
		    <h1>Job Details</h1>
			<button type="submit" class="btn btn-primary" onclick="return confirm('Confirmation Required. Send Personal Information Y/N?')">Apply</button>
		    <hr>
		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Title:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $job->getPosition_name() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Type:</p>				
			</div>
			<div class="col-7">
				<p class="text-start"><?php echo $type[ $job->getPosition_type() ]; ?></p>				
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Description:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $job->getDescription() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Required Skills:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $job->getSkills_keywords() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Required Education:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $job->getEducation_keywords() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Offer Expires:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $job->getExpire_date() }}</p>
			</div>
			<div class="col-1"></div>
		</div>

		<hr>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Organization:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getName() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Description:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getDescription() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Mission Statement:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getMission_statement() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Ethics Statment:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getEthics_statement() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Phone:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getPhone() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Website:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getWebsite() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Email:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $organization->getEmail() }}</p>
			</div>
			<div class="col-1"></div>
		</div>

		<hr>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Location:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getLocation_name() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Street:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getStreet() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">City:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getCity() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">State:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getState() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Postal Code:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getPostal_code() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<p class="text-end fw-bold">Country:</p>
			</div>
			<div class="col-7">
				<p class="text-start">{{ $location->getCountry() }}</p>
			</div>
			<div class="col-1"></div>
		</div>
		<hr>
		<input type="hidden" name="JobID" id="JobID" value="<?php echo $job->getId(); ?>">
	</form>
</div>

@endsection
