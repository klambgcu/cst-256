<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Web Routing 
 * 2. Handles routing - very conveniently
 * 3. 
 * ---------------------------------------------------------------
 */

Route::group(['middleware' => ['web']], function ()
{
    
// TESTING AREA
//
// Route::get('/test', 'GroupController@testGroupBackend');
    

// ---------------------------------------------------------------
// Home Page Section
// ---------------------------------------------------------------
Route::get('/', function () {return view('welcome');});
Route::get('/home', function () {return view('welcome');});


// ---------------------------------------------------------------
// Group Section
// ---------------------------------------------------------------
Route::get('/userGroup', 'GroupController@showGroupListing');
Route::get('/groupAction', 'GroupController@doGroupAction');


// ---------------------------------------------------------------
// Login Section
// ---------------------------------------------------------------
// Login Form
Route::get('/login', function () {return view('login');});

// Login Form Action
Route::post('/doLogin', 'LoginController@login');

// Login Status Form
Route::get('/loginstatus', function () {return view('loginstatus');});
    
// Login Status Form
Route::get('/logout', 'LoginController@logout');


// ---------------------------------------------------------------
// Member Profile Section
// ---------------------------------------------------------------
// Member Profile Form
Route::get('/memberprofile', 'ProfileController@showMemberPage');

// Member Profile Form Action
Route::post('/doMemberProfile', 'ProfileController@updateMemberProfile');


// ---------------------------------------------------------------
// Registration Section
// ---------------------------------------------------------------
// Registration Form
Route::get('/register', function () {return view('register');});

// Registration Form Action
Route::post('/doRegister', 'RegisterController@register');

// Register Status Form
Route::get('/registerstatus', function () {return view('registerstatus');});


// ---------------------------------------------------------------
// Administrator Section
// Group
// ---------------------------------------------------------------
// Admin Group Listing Form
Route::get('/adminGroup', 'AdminController@showAdminGroupListing');

// Admin Group Listing Form - Action Delete or show Edit)
Route::get('/adminGroupAction', 'AdminController@doAdminGroupAction');

// Admin Group Listing Form - Alter Group (Edit)
Route::get('/adminChangeGroup', 'AdminController@showAdminGroup');

// Admin Group - Create New Group
Route::get('/adminCreateGroup', 'AdminController@showAdminCreateGroup');

// Admin Group View Form Action
Route::post('/doAdminGroup', 'AdminController@doAdminGroup');












// ---------------------------------------------------------------
// Administrator Section
// User 
// ---------------------------------------------------------------
// Admin User Listing Form
Route::get('/adminUser', 'AdminController@showAdminUserListing');

// Admin User Listing Form - Alter user
Route::get('adminChangeUser', 'AdminController@showMemberProfile');


// ---------------------------------------------------------------
// Administrator Section
// Job
// ---------------------------------------------------------------
// Admin Job Listing Form
Route::get('/adminJob', 'AdminController@showAdminJobListing');

// Admin Job Listing Form - Alter job (Edit/Delete)
Route::get('/adminChangeJob', 'AdminController@showAdminJob');

// Admin Job - Create New Job
Route::get('/adminCreateJob', 'AdminController@showAdminCreateJob');

// Admin Job View Form
Route::get('/adminJobView', function () {return view('adminJobView');});

// Admin Job View Form Action
Route::post('/doAdminJob', 'AdminController@doAdminJob');
    

// ---------------------------------------------------------------
// Administrator Section
// Organization
// ---------------------------------------------------------------
// Admin Organization Listing Form
Route::get('/adminOrganization', 'AdminController@showAdminOrganizationListing');

// Admin Organization Listing Form - Alter org (Edit/Delete)
Route::get('/adminChangeOrganization', 'AdminController@showAdminOrganization');

// Admin Organization - Create New Organization
Route::get('/adminCreateOrganization', 'AdminController@showAdminCreateOrganization');

// Admin Organization View Form
Route::get('/adminOrganizationView', function () {return view('adminOrgView');});

// Admin Organization View Form Action
Route::post('/doAdminOrganization', 'AdminController@doAdminOrganization');





}); // Try wrapping all routes inside group to retain session on redirects/etc.
    