<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Profile Controller
 * 2. Handles Profile Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

namespace App\Http\Controllers;

use App\Services\Business\UserService;
use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserAggregateModel;
use App\Models\UserSkillsetModel;
use App\Models\UserEducationModel;
use App\Models\UserWorkHistoryModel;
use App\Services\Business\RoleService;

class ProfileController extends Controller
{
    // Display the member profile Page
    public function showMemberPage()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (isset($_SESSION['principal']))
        {
            $user = $_SESSION['principal'];
        }
        else 
        {
            return view('login');
        }
        $service = new UserService();
        $userAggregateModel = $service->getUserAggregate($user->getId());

        $roleService = new RoleService();
        $rolesList = $roleService->getAllRoles();
        $data = ['userAggregateModel' => $userAggregateModel, 'rolesList' => $rolesList];
        return view('memberProfile')->with($data);
    }

    // User made changes - gather input and send to database
    public function updateMemberProfile(Request $request)
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        $user = $_SESSION['principal'];
        
        $curDate = date("Y-m-d");
        
        // UserModel
        $user_id   = $request->input('UserID');
        $firstname = $request->input('FirstName');
        $lastname  = $request->input('LastName');
        $email     = $request->input('Email');
        $password  = $request->input('Password');
        $mobile    = $request->input('Mobile');
        $birthdate = $request->input('Birthdate');
        $gender    = $request->input('Gender');
        $role_id   = $request->input('RoleID');
        $suspended = $request->input('Suspended');
        $userModel = new UserModel($user_id, $firstname, $lastname, $email, $mobile, $password, $birthdate, $gender, $role_id, $suspended);        
        
        // UserProfileModel
        $prevphoto   = $request->input('PrevPhoto');
        $desc        = $request->input('Description', "");
        $street      = $request->input('Street', "");
        $city        = $request->input('City', "");
        $state       = $request->input('State', "");
        $postal_code = $request->input('ZipCode', "");
        $country     = "USA";
        $photoName = basename($_FILES["Photo"]["name"]);
        
        if (trim($photoName) == "")
        {
            $photo = $prevphoto;
        }
        else 
        {
            $ext = strtolower(pathinfo($photoName,PATHINFO_EXTENSION));
            $tmp_path = $_FILES["Photo"]["tmp_name"];
            $target_dir = "images" . DIRECTORY_SEPARATOR . "users" . DIRECTORY_SEPARATOR;
            $photo = $user_id . '.' . $ext;
            $filepath = $target_dir . $photo;
            // Move the image to public/images/users directory
            move_uploaded_file($tmp_path, $filepath);
        }
        
        $userProfile = new UserProfileModel(0, $user_id, $photo, $desc, $street, $city, $state, $postal_code, $country);
        
        // UserSkillsetModel Array
        $emptyArray = array();
        $skillName = $request->input('SkillName', $emptyArray);
        $skillDesc = $request->input('SkillDescription', $emptyArray);
        $userSkillset = array();
        $index = 0;
        for ($x = 0; $x < count($skillName); ++$x)
        {
            if (trim($skillName[$x]) !== "")
            {
                $skill = new UserSkillsetModel(0, $user_id, $skillName[$x], $skillDesc[$x]);
                $userSkillset[$index] = $skill;
                ++$index;
            }
        }
        
        // UserEducationModel Array
        $orgName = $request->input('OrgName', $emptyArray);
        $degree  = $request->input('Degree', $emptyArray);
        $eStart  = $request->input('EStartDate', $emptyArray);
        $eEnd    = $request->input('EEndDate', $emptyArray);
        $grad    = $request->input('Graduated', $emptyArray);
        $link_to_org = "";
        $userEducation = array();
        $index = 0;
        for ($x = 0; $x < count($orgName); ++$x)
        {
            if (trim($orgName[$x]) !== "")
            {
                $education = new UserEducationModel(0, $user_id, $orgName[$x], $degree[$x], $eStart[$x], $eEnd[$x], $grad[$x], $link_to_org);
                $userEducation[$index] = $education;
                ++$index;
            }
        }
        
        // UserWorkHistoryModel Array
        $busName = $request->input('BusinessName', $emptyArray);
        $wDesc   = $request->input('WorkDescription', $emptyArray);
        $wStart  = $request->input('WStartDate', $emptyArray);
        $WEnd    = $request->input('WEndDate', $emptyArray);
        $current = $request->input('Current', $emptyArray);
        $link_to_org = "";
        $userWorkHistory = array();
        for ($x = 0; $x < count($busName); ++$x)
        {
            if (trim($busName[$x]) !== "")
            {
                $history = new UserWorkHistoryModel(0, $user_id, $busName[$x], $wDesc[$x], $wStart[$x], $WEnd[$x], $current[$x], $link_to_org);
                $userWorkHistory[$index] = $history;
                ++$index;
            }
        }
        
        // Create user aggregate so we can stick it in the database :)
        $userAggregateModel = new UserAggregateModel($userModel, $userProfile, $userWorkHistory, $userEducation, $userSkillset);
        $service = new UserService();
        $service->updateUserByAggregate($userAggregateModel);
                
//         echo "<pre>\n";
//         print_r($userAggregateModel);
//         echo "</pre>\n";
//         exit;
        
        if ($user->getRole_id() == 2 || $user->getRole_id() == 4)
        {
            return redirect('adminUser');
        }
        
        return view('welcome');
    }
}
