<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-012
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold user education
 * 2. Member Profile
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Models;

class UserEducationModel
{
    private $id;
    private $user_id;
    private $org_name;
    private $degree_description;
    private $start_date;
    private $end_date;
    private $graduated;
    private $link_to_org;
    
    public function __construct($id, $user_id, $org_name, $degree_description, $start_date, $end_date, $graduated, $link_to_org)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->org_name = $org_name;
        $this->degree_description = $degree_description;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->graduated = $graduated;
        $this->link_to_org = $link_to_org;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getOrg_name()
    {
        return $this->org_name;
    }

    /**
     * @return mixed
     */
    public function getDegree_description()
    {
        return $this->degree_description;
    }

    /**
     * @return mixed
     */
    public function getStart_date()
    {
        return $this->start_date;
    }

    /**
     * @return mixed
     */
    public function getEnd_date()
    {
        return $this->end_date;
    }

    /**
     * @return mixed
     */
    public function getGraduated()
    {
        return $this->graduated;
    }

    /**
     * @return mixed
     */
    public function getLink_to_org()
    {
        return $this->link_to_org;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $org_name
     */
    public function setOrg_name($org_name)
    {
        $this->org_name = $org_name;
    }

    /**
     * @param mixed $degree_description
     */
    public function setDegree_description($degree_description)
    {
        $this->degree_description = $degree_description;
    }

    /**
     * @param mixed $start_date
     */
    public function setStart_date($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEnd_date($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @param mixed $graduated
     */
    public function setGraduated($graduated)
    {
        $this->graduated = $graduated;
    }

    /**
     * @param mixed $link_to_org
     */
    public function setLink_to_org($link_to_org)
    {
        $this->link_to_org = $link_to_org;
    }
}

