<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold organization information
 * 2. Job Postings
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Models;

class OrganizationModel
{
    private $id;
    private $name;
    private $description;
    private $mission_statement;
    private $ethics_statement;
    private $phone;
    private $website;
    private $email;
    
    public function __construct($id, $name, $description, $mission_statement, $ethics_statement, $phone, $website, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->mission_statement = $mission_statement;
        $this->ethics_statement = $ethics_statement;
        $this->phone = $phone;
        $this->website = $website;
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getMission_statement()
    {
        return $this->mission_statement;
    }

    /**
     * @return mixed
     */
    public function getEthics_statement()
    {
        return $this->ethics_statement;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $mission_statement
     */
    public function setMission_statement($mission_statement)
    {
        $this->mission_statement = $mission_statement;
    }

    /**
     * @param mixed $ethics_statement
     */
    public function setEthics_statement($ethics_statement)
    {
        $this->ethics_statement = $ethics_statement;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param mixed $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}

