<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-14
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Users (Members)
 * 2. Various Users CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\UserProfileModel;
use App\Models\UserEducationModel;
use App\Models\UserSkillsetModel;
use App\Models\UserWorkHistoryModel;
use App\Models\UserModel;
use App\Models\UserAggregateModel;

class UserDAO
{
    public function __construct()
    {}

    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------

    public function createUser($userModel)
    {
        // Parameter - user model
 
        $firstname = $userModel->getFirst_name();
        $lastname  = $userModel->getLast_name();
        $email     = $userModel->getEmail();
        $password  = $userModel->getPassword();
        $mobile    = $userModel->getMobile();
        $birthdate = $userModel->getBirthdate();
        $gender    = $userModel->getGender();
        $role_id   = $userModel->getRole_id();
        $suspended = $userModel->getSuspended();
        
        $sql = 'INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, MOBILE, PASSWORD, BIRTHDATE, GENDER, ROLE_ID, SUSPENDED) ' .
               'VALUES (?,?,?,?,?,?,?,?,?)';
        DB::insert($sql, [$firstname, $lastname, $email, $mobile, $password, $birthdate, $gender, $role_id, $suspended]);

        // Create a user profile - with default values, An extension of users with 1-to-1 relationship
        $user_id = DB::getPdo()->lastInsertId();
        $description = "";
        $street = "";
        $city = "";
        $state = "";
        $postal_code = "";
        
        $sql = 'INSERT INTO users_profile (USER_ID, DESCRIPTION, STREET, CITY, STATE, POSTAL_CODE) ' .
               'VALUES (?,?,?,?,?,?)';
        DB::insert($sql, [$user_id, $description, $street, $city, $state, $postal_code]);

        $userModel->setID($user_id);
        
        return $userModel;
    }

    public function createUserEducation($user_id, $education)
    {
        // education is an array of UserEducationModel
        foreach ($education as $item)
        {
            $org_name = $item->getOrg_name();
            $degree_description = $item->getDegree_description();
            $start_date = $item->getStart_date();
            $end_date = $item->getEnd_date();
            $graduated = $item->getGraduated();
            $link_to_org = $item->getLink_to_org();
            
            $sql = 'INSERT INTO users_education (USER_ID, ORG_NAME, DEGREE_DESCRIPTION, START_DATE, END_DATE, GRADUATED, LINK_TO_ORG) VALUES (?, ?, ?, ?, ?, ?, ?)';
            $data = [$user_id, $org_name, $degree_description, $start_date, $end_date, $graduated, $link_to_org];
            DB::insert($sql, $data);
        }
    }

    public function createUserSkillset($user_id, $skillset)
    {
        // skillset is an array of UserSkillsetModel
        foreach ($skillset as $item)
        {
            $skill_name = $item->getSkill_name();
            $description = $item->getDescription();
            
            $sql = 'INSERT INTO users_skillset (USER_ID, SKILL_NAME, DESCRIPTION) VALUES (?, ?, ?)';
            $data = [$user_id, $skill_name, $description];
            DB::insert($sql, $data);
        }
    }

    public function createUserWorkHistory($user_id, $history)
    {
        // history is an array of UserWorkHistoryModel
        foreach ($history as $item)
        {
            $org_name = $item->getOrg_name();
            $work_description = $item->getWork_description();
            $start_date = $item->getStart_date();
            $end_date = $item->getEnd_date();
            $currently_employed = $item->getCurrently_employed();
            $link_to_org = $item->getLink_to_org();

            $sql = 'INSERT INTO users_work_history (USER_ID, ORG_NAME, WORK_DESCRIPTION, START_DATE, END_DATE, CURRENTLY_EMPLOYED, LINK_TO_ORG) VALUES (?, ?, ?, ?, ?, ?, ?)';
            $data = [$user_id, $org_name, $work_description, $start_date, $end_date, $currently_employed, $link_to_org];
            DB::insert($sql, $data);
        }
    }
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getUserByEmail($email)
    {
        $rows = DB::select('SELECT * FROM users WHERE EMAIL = ?', [$email]);
        
        $user = new UserModel($rows[0]->ID,
                              $rows[0]->FIRST_NAME,
                              $rows[0]->LAST_NAME,
                              $rows[0]->EMAIL,
                              $rows[0]->MOBILE,
                              $rows[0]->PASSWORD,
                              $rows[0]->BIRTHDATE,
                              $rows[0]->GENDER,
                              $rows[0]->ROLE_ID,
                              $rows[0]->SUSPENDED);
        
        return $user;
    }
    
    public function getUser($user_id)
    {
        $rows = DB::select('SELECT * FROM users WHERE ID = ?', [$user_id]);

        $user = new UserModel($rows[0]->ID, 
                              $rows[0]->FIRST_NAME, 
                              $rows[0]->LAST_NAME, 
                              $rows[0]->EMAIL, 
                              $rows[0]->MOBILE, 
                              $rows[0]->PASSWORD, 
                              $rows[0]->BIRTHDATE, 
                              $rows[0]->GENDER, 
                              $rows[0]->ROLE_ID, 
                              $rows[0]->SUSPENDED);        
        
        return $user;
    }
    
    public function getUserAggregate($user_id)
    {
        // Gather constituents
        $userModel = $this->getUser($user_id);
        $userProfile = $this->getUserProfile($user_id);
        $userWorkHistory = $this->getUserWorkHistory($user_id);
        $userEducation = $this->getUserEducation($user_id);
        $userSkillset = $this->getUserSkillset($user_id);
        
        $user = new UserAggregateModel($userModel, $userProfile, $userWorkHistory, $userEducation, $userSkillset);
        
        return $user;
    }

    public function getUserEducation($user_id)
    {
        $education = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM users_education WHERE USER_ID = ? ORDER BY ID ASC', [$user_id]);
        
        foreach ($rows as $row)
        {
            $user_education = new UserEducationModel($row->ID, 
                                                     $row->USER_ID, 
                                                     $row->ORG_NAME, 
                                                     $row->DEGREE_DESCRIPTION, 
                                                     $row->START_DATE, 
                                                     $row->END_DATE, 
                                                     $row->GRADUATED, 
                                                     $row->LINK_TO_ORG);
            $education[$index] = $user_education;
            ++$index;
        }

        return $education;
    }
        
    public function getUserProfile($user_id)
    {
        $rows = DB::select('SELECT * FROM users_profile WHERE USER_ID = ?', [$user_id]);
        
        $user_profile = new UserProfileModel($rows[0]->ID,
                                             $rows[0]->USER_ID,
                                             $rows[0]->PHOTO,
                                             $rows[0]->DESCRIPTION,
                                             $rows[0]->STREET,
                                             $rows[0]->CITY,
                                             $rows[0]->STATE,
                                             $rows[0]->POSTAL_CODE,
                                             $rows[0]->COUNTRY);
        
        return $user_profile;
    }

    public function getUserSkillset($user_id)
    {
        $skills = array();
        $index = 0;

        $rows = DB::select('SELECT * FROM users_skillset WHERE USER_ID = ? ORDER BY ID ASC', [$user_id]);
        
        foreach ($rows as $row)
        {
            $user_skillset = new UserSkillsetModel($row->ID, 
                                                   $row->USER_ID, 
                                                   $row->SKILL_NAME, 
                                                   $row->DESCRIPTION);
            $skills[$index] = $user_skillset;
            ++$index; 
        }

        return $skills;
    }
    
    public function getUserWorkHistory($user_id)
    {
        $history = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM users_work_history WHERE USER_ID = ? ORDER BY ID ASC', [$user_id]);
        
        foreach ($rows as $row)
        {
            $user_workhistory = new UserWorkHistoryModel($row->ID, 
                                                         $row->USER_ID, 
                                                         $row->ORG_NAME, 
                                                         $row->WORK_DESCRIPTION, 
                                                         $row->START_DATE, 
                                                         $row->END_DATE, 
                                                         $row->CURRENTLY_EMPLOYED, 
                                                         $row->LINK_TO_ORG);
            $history[$index] = $user_workhistory;
            ++$index;
        }

        return $history;
    }
    
    public function getAllUsers()
    {
        $users = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM users ORDER BY FIRST_NAME ASC');
        
        foreach ($rows as $row)
        {
            $user = new UserModel($row->ID,
                                  $row->FIRST_NAME,
                                  $row->LAST_NAME,
                                  $row->EMAIL,
                                  $row->MOBILE,
                                  $row->PASSWORD,
                                  $row->BIRTHDATE,
                                  $row->GENDER,
                                  $row->ROLE_ID,
                                  $row->SUSPENDED);
            $users[$index] = $user;
            ++$index;
        }
        
        return $users;
    }
    
    
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function suspendUser($user_id)
    {
        $result = DB::update('UPDATE users SET SUSPENDED = 1 WHERE USER_ID = ?', [$user_id]);
        return $result;
    }
    
    public function unsuspendUser($user_id)
    {
        $result = DB::update('UPDATE users SET SUSPENDED = 0 WHERE USER_ID = ?', [$user_id]);
        return $result;
    }
    
    public function updateUser($userModel)
    {
//        $userModel= new UserModel($id, $first_name, $last_name, $email, $mobile, $password, $birthdate, $gender, $role_id, $suspended);

        $user_id    = $userModel->getId();
        $first_name = $userModel->getFirst_name();
        $last_name  = $userModel->getLast_name();
        $email      = $userModel->getEmail();
        $mobile     = $userModel->getMobile();
        $password   = $userModel->getPassword();
        $birthdate  = $userModel->getBirthdate();
        $gender     = $userModel->getGender();
        $role_id    = $userModel->getRole_id();
        $suspended  = $userModel->getSuspended();
        
        $sql = 'UPDATE users SET FIRST_NAME = ?, LAST_NAME = ?, EMAIL = ?, MOBILE = ?, PASSWORD = ?, BIRTHDATE = ?, GENDER = ?, ROLE_ID = ?, SUSPENDED = ? WHERE ID = ?';
        $data = [$first_name, $last_name, $email, $mobile, $password, $birthdate, $gender, $role_id, $suspended, $user_id];
        $count = DB::update($sql, $data);
        
        return ($count > 0);
    }
    
    public function updateUserProfile($userProfile)
    {
//        $userProfile = new UserProfileModel($id, $user_id, $photo, $description, $street, $city, $state, $postal_code, $country);

        $user_id     = $userProfile->getUser_id();
        $photo       = $userProfile->getPhoto();
        $description = $userProfile->getDescription();
        $street      = $userProfile->getStreet();
        $city        = $userProfile->getCity();
        $state       = $userProfile->getState();
        $postal_code = $userProfile->getPostal_code();
        $country     = $userProfile->getCountry();
        
        $sql = 'UPDATE users_profile SET PHOTO = ?, DESCRIPTION=?, STREET = ?, CITY = ?, STATE = ?, POSTAL_CODE = ?, COUNTRY = ? WHERE USER_ID = ?';
        $data = [$photo, $description, $street, $city, $state, $postal_code, $country, $user_id];
        
        $count = DB::update($sql, $data);
        
        return ($count > 0);
    }

    public function updateUserByAggregate($userAggregateModel)
    {
        // Get constituent breakdown to send to individual functions
        $userModel       = $userAggregateModel->getUserModel();
        $userProfile     = $userAggregateModel->getUserProfile();
        $userEducation   = $userAggregateModel->getUserEducation();
        $userSkillset    = $userAggregateModel->getUserSkillset();
        $userWorkHistory = $userAggregateModel->getUserWorkHistory();
        
        $user_id = $userModel->getId();        

        // Update users$userWorkHistory
        $this->updateUser($userModel);
        
        // Update users_profile
        $this->updateUserProfile($userProfile);
        
        // Delete users_skillset
        $this->deleteUserSkillSet($user_id);

        // Delete users_education
        $this->deleteUserEducation($user_id);

        // Delete users_work_history
        $this->deleteUserWorkHistory($user_id);

        // Insert users_skillset
        $this->createUserSkillset($user_id, $userSkillset);
        
        // Insert users_education
        $this->createUserEducation($user_id, $userEducation);

        // Insert users_work_history
        $this->createUserWorkHistory($user_id, $userWorkHistory);
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteUser($user_id)
    {
        // DUE TO FOREIGN KEY CONSTRAINTS - THIS SHOULD DELETE ALL USER RELATED TABLE DATA
        $result = DB::delete('DELETE FROM users WHERE ID = ?', [$user_id]);
        return $result;
    }
    
    public function deleteUserEducation($user_id)
    {
        // Remove Education from this user
        $result = DB::delete('DELETE FROM users_education WHERE USER_ID = ?', [$user_id]);
        return $result;
    }
    
    public function deleteUserProfile($user_id)
    {
        // Remove profile from this user
        $result = DB::delete('DELETE FROM users_profile WHERE USER_ID = ?', [$user_id]);
        return $result;
    }

    public function deleteUserSkillSet($user_id)
    {
        // Remove skill sets from this user
        $result = DB::delete('DELETE FROM users_skillset WHERE USER_ID = ?', [$user_id]);
        return $result;
    }
    
    public function deleteUserWorkHistory($user_id)
    {
        // Remove work history from this user
        $result = DB::delete('DELETE FROM users_work_history WHERE USER_ID = ?', [$user_id]);
        return $result;
    }
    
}

