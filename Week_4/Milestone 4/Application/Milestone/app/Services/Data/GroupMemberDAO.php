<?php
namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\GroupMemberModel;


/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Groups_Members (Group Member Associations)
 * 2. Various Group CRUD
 * 3.
 * ---------------------------------------------------------------
 */

class GroupMemberDAO
{
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($groupMemberModel)
    {
        // $groupMemberModel = new GroupMemberModel($id, $group_id, $user_id, $start_date);
        
        $group_id = $groupMemberModel->getGroup_id();
        $user_id = $groupMemberModel->getUser_id();
        $start_date = $groupMemberModel->getStart_date();
        
        $sql = 'INSERT INTO groups_members (GROUP_ID, USER_ID, START_DATE) VALUES (?, ?, ?)';
        $data = [$group_id, $user_id, $start_date];
        DB::insert($sql, $data);
        $id = DB::getPdo()->lastInsertId();
        
        return $id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $list = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups_members g ORDER BY g.GROUP_ID ASC, g.USER_ID ASC');
        
        foreach ($rows as $row)
        {
            $item = new GroupMemberModel($row->ID,
                                         $row->GROUP_ID,
                                         $row->USER_ID,
                                         $row->START_DATE);
            $list[$index] = $item;
            ++$index;
        }
        
        return $list;
    }
    
    public function getAllGroupsForMember($user_id)
    {
        $list = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups_members g WHERE g.USER_ID = ? ORDER BY g.GROUP_ID ASC ', [$user_id]);
        
        foreach ($rows as $row)
        {
            $item = new GroupMemberModel($row->ID,
                                         $row->GROUP_ID,
                                         $row->USER_ID,
                                         $row->START_DATE);
            $list[$index] = $item;
            ++$index;
        }
        
        return $list;
    }
    
    public function getAllMembersForGroup($group_id)
    {
        $list = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups_members g WHERE g.GROUP_ID = ? ORDER BY g.USER_ID ASC ', [$group_id]);
        
        foreach ($rows as $row)
        {
            $item = new GroupMemberModel($row->ID,
                                         $row->GROUP_ID,
                                         $row->USER_ID,
                                         $row->START_DATE);
            $list[$index] = $item;
            ++$index;
        }
        
        return $list;
    }
        
    public function getByID($id)
    {
        $row = DB::select('SELECT g.* FROM groups_members g WHERE g.ID = ? ', [$id]);
        
        $group = new GroupMemberModel($row[0]->ID,
                                      $row[0]->GROUP_ID,
                                      $row[0]->USER_ID,
                                      $row[0]->START_DATE);
        
        return $group;
    }
    
    public function isGroupMember($user_id, $group_id)
    {
        $exists = DB::select('SELECT COUNT(1) AS AMOUNT FROM groups_members WHERE USER_ID = ? AND GROUP_ID = ?', [$user_id, $group_id]);
        
        return ($exists[0]->AMOUNT > 0);
    }
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    // -- DO NOT SEE A NEED TO UPDATE THE GROUPS MEMBERS TABLE

    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($id)
    {
        $count = DB::delete('DELETE FROM groups_members WHERE ID = ?', [$id]);
        
        return $count;
    }
    
    public function removeUserFromGroup($group_id, $user_id)
    {
        $count = DB::delete('DELETE FROM groups_members WHERE GROUP_ID = ? AND USER_ID = ?', [$group_id, $user_id]);
        
        return $count;
    }
    

}

