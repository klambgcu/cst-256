<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-12
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Group Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Group Listing')
@section('content')

<style>
  .button {
    background-color: lightblue;
    border: none;
    color: white;
    padding: 4px 16px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 2px 2px;
    cursor: pointer;
  }
</style>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	@csrf
	<div align="center">
	    <h1>Admin Group Listing</h1>
	    <p><a class='button' href='adminCreateGroup'>Create Group</a></p>
	    <hr>
	</div>

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php
    foreach ($groupsList as $g)
    {
        echo "  <tr>\n";
        echo "      <td>" . $g->getId() . "</td>\n";
        echo "      <td>" . $g->getGroup_name() . "</td>\n";
        echo "      <td>" . $g->getGroup_description() . "</td>\n";
        echo "      <td>" . "<a href='adminGroupAction?id=" . $g->getId() . "&mode=0'>Edit</a>" . 
            "&nbsp;|&nbsp;<a href='adminGroupAction?id=" . $g->getId() ."&mode=1' onclick=\"return confirm('Confirmation Required. Delete this group (" . $g->getId() . ") Y/N?')\">Delete</a></td>\n";
        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>

</div>

@endsection

