<?php

use App\Models\GroupModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-10
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Group Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Group Listing')
@section('content')

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	@csrf
	<div align="center">
	    <h1>Group Listing</h1>
	    <p></p>
	    <hr>
	</div>

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Member</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php
    $membership = ["No", "Yes"];

    foreach ($groupsList as $g)
    {
        //$g = new GroupModel($id, $creator_id, $group_name, $group_description, $created_date, $is_member);
        echo "  <tr>\n";
        echo "      <td>" . $g->getId() . "</td>\n";
        echo "      <td>" . $g->getGroup_name() . "</td>\n";
        echo "      <td>" . $g->getGroup_description() . "</td>\n";
        echo "      <td>" . $membership[$g->getIs_member()] . "</td>\n";
        echo "      <td>" . "<a href='groupAction?id=" . $g->getId() . "&mode=0'>View</a>" . 
            "&nbsp;|&nbsp;<a href='groupAction?id=" . $g->getId() ."&mode=";
        
        if ($g->getIs_member() == 0)
            { echo "1'>Join</a>"; }
        else
            { echo "2' onclick=\"return confirm('Confirmation Required. Exit (Leave) this group Y/N?')\">Leave</a>"; }
        echo "</td>\n";
                
        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>

</div>

@endsection

