<?php

use App\Models\UserModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Organization Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Organization Listing')
@section('content')

<style>
  .button {
    background-color: lightblue;
    border: none;
    color: white;
    padding: 4px 16px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 2px 2px;
    cursor: pointer;
  }
</style>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	@csrf
	<div align="center">
	    <h1>Admin Organization Maintenance Listing</h1>
	    <p><a class='button' href='adminCreateOrganization'>New Organization</a>
	    <hr>
	</div>

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>Organization</th>
        <th>Description</th>
        <th>Mission</th>
        <th>Ethics</th>
        <th>Phone</th>
        <th>Website</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php

// private $id;
// private $name;
// private $description;
// private $mission_statement;
// private $ethics_statement;
// private $phone;
// private $website;
// private $email;

    foreach ($orgsList as $u)
    {
        echo "  <tr>\n";
        echo "      <td>" . $u->getId() . "</td>\n";
        echo "      <td>" . $u->getName() . "</td>\n";
        echo "      <td>" . substr($u->getDescription(),0,10) . "..." . "</td>\n";
        echo "      <td>" . substr($u->getMission_statement(),0,10) . "..." . "</td>\n";
        echo "      <td>" . substr($u->getEthics_statement(),0,10) . "..." . "</td>\n";
        echo "      <td>" . $u->getPhone() . "</td>\n";
        echo "      <td>" . $u->getWebsite() . "</td>\n";
        echo "      <td>" . $u->getEmail() . "</td>\n";
        echo "      <td>" . "<a href='adminChangeOrganization?id=" . $u->getId() . "&mode=0'>Edit<a>" . 
            "&nbsp;|&nbsp;<a href='adminChangeOrganization?id=" . $u->getId() . "&mode=1' onclick=\"return confirm('Confirmation Required. Delete " . $u->getName() . " Y/N ?')\">Delete<a></td>\n";
        
        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>

</div>

@endsection

