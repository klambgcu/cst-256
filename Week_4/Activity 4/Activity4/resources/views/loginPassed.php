<?php 

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Successful view
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login Successful</title>
	</head>
<body>
	<div align="center">
    <h1>Login Successful!</h1>
	<hr>
	<h2>Welcome!</h2>
	<br>
    <?php
        echo "User Name: " . $userModel->getUsername() . "<br>\n";
        echo "Password: " . $userModel->getPassword() . "<br>\n";
    ?>
    <br>
    </div>
</body>
</html>