<?php 

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. login view
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>
	</head>
<body>

    <form action = "/dologin" method = "POST">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
        <h2>Login</h2>
        <table>
        	<tr>
        		<td>User Name: </td>
        		<td><input type = "text" name = "username" /></td>
        	</tr>
        
        	<tr>
        		<td>Password:</td>
        		<td><input type = "password" name = "password" /></td>
        	</tr>
        	<tr>
        		<td colspan = "2" align = "center">
        			<input type = "submit" value = "Login" />
        		</td>
        </table>
	</form>
</body>
</html>