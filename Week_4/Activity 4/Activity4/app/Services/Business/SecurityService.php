<?php
namespace App\Services\Business;

use App\Services\Data\SecurityDAO;
use Illuminate\Support\Facades\Log;
use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;
use Exception;

class SecurityService
{

    public function __construct()
    {}
    
    public function login($userModel)
    {
        MyLogger2::info("Entering SecurityService::login()");
        
        try
        {
            $dao = new SecurityDAO();
            $results = $dao->findByUser($userModel);
            
            MyLogger2::info("Exiting SecurityService::login()");
            
            return $results;
        }
        catch (Exception $e)
        {
            MyLogger2::error("Exception SecurityService::login()" . $e->getMessage());
        }        
    }
}
