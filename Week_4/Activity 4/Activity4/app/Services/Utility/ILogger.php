<?php
namespace App\Services\Utility;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-29
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Logging Mechanism Abstract
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

interface ILogger
{
    public static function getLogger();
    
    public static function debug($message, array $context = array());
    
    public static function info($message, array $context = array());
    
    public static function warning($message, array $context = array());
    
    public static function error($message, array $context = array()); 
}

