<?php
namespace App\Services\Utility;

use Illuminate\Support\Facades\Log;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-29
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 4
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Logging Mechanism Concrete
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

class MyLogger1 implements ILogger
{
    public static function getLogger()
    {}
    
    public static function debug($message, array $context = array())
    {
        Log::debug($message, $context);
    }

    public static function warning($message, array $context = array())
    {
        Log::warning($message, $context);
    }

    public static function error($message, array $context = array())
    {
        Log::error($message, $context);
    }

    public static function info($message, array $context = array())
    {
        Log::info($message, $context);
    }
}

