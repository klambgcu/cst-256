<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Illuminate\Support\Facades\Log;
use App\Services\Utility\MyLogger1;
use App\Services\Utility\MyLogger2;
use Exception;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-17
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handles Login Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class Login3Controller extends Controller
{
    public function index(Request $request)
    {
        MyLogger2::info("Entering LoginController::index()");
        
        // Validate the Form Data (note will automatically redirect back to Login View if errors)
        $this->validateForm($request);
        
        $username = $request->input('username');
        $password = $request->input('password');

        MyLogger2::info("Parameters are: ",array("username" => $username, "password" => $password));
        
        $userModel = new UserModel($username, $password);
                
        try
        {
            $service = new SecurityService();
            $results = $service->login($userModel);
            
        }
        catch (Exception $e)
        {
            MyLogger2::error("Exception LoginController::index()" . $e->getMessage());
        }
        
        if ($results)
        {
            MyLogger2::info("Exit LoginController::index() with login passing");
            
            $data = ['userModel' => $userModel];
            return view('loginPassed2')->with($data);
        }
        else 
        {
            MyLogger2::info("Exit LoginController::index() with login failing");
            
            return view('loginFailed');
        }
    }
    
    private function validateForm(Request $request)
    {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,10 | Alpha', 'password' => 'Required | Between:4,10'];
        
        // Run Data Validation Rules
        $this->validate($request, $rules);
    }
}
