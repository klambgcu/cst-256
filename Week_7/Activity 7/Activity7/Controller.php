<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Specifically ensure login acquired at each method - check role to assert privileged users
    protected function assertLogin($check_role = false)
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }

        if (! isset($_SESSION['principal']))
        {
            return true;
        }
        else
        {
            if ($check_role)
            {
                // Role 1 = Normal User, 2 = User Maint, 3 = Group Maint, 4 = Admin
                $user = $_SESSION['principal'];
                if ($user->getRole_id() > 1)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}
