<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Jobs (Job Postings)
 * 2. Various Job CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\JobModel;
use App\Models\JobAggregateModel;

class JobDAO
{
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($jobModel)
    {
        $job_id = 0;
        $organization_id = $jobModel->getOrganization_id();
        $location_id = $jobModel->getLocation_id();
        $position_name = $jobModel->getPosition_name();
        $description = $jobModel->getDescription();
        $position_type = $jobModel->getPosition_type();
        $expire_date = $jobModel->getExpire_date();
        $skills_keywords = $jobModel->getSkills_keywords();
        $education_keywords = $jobModel->getEducation_keywords();
        
        $sql = 'INSERT INTO jobs (ORGANIZATION_ID, LOCATION_ID, POSITION_NAME, DESCRIPTION, POSITION_TYPE, EXPIRE_DATE, SKILLS_KEYWORDS, EDUCATION_KEYWORDS) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
        $data = [$organization_id, $location_id, $position_name, $description, $position_type, $expire_date, $skills_keywords, $education_keywords];
        DB::insert($sql, $data);
        $job_id = DB::getPdo()->lastInsertId();
        
        return $job_id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $jobs = array();
        $index = 0;
        
        $rows = DB::select('SELECT j.*, o.NAME, l.LOCATION_NAME FROM jobs j, organizations o, locations l WHERE j.ORGANIZATION_ID = o.ID AND j.LOCATION_ID = l.ID ORDER BY j.ID ASC');
        
        foreach ($rows as $row)
        {
            $job = new JobModel($row->ID,
                                $row->ORGANIZATION_ID,
                                $row->LOCATION_ID,
                                $row->POSITION_NAME,
                                $row->DESCRIPTION,
                                $row->POSITION_TYPE,
                                $row->EXPIRE_DATE,
                                $row->SKILLS_KEYWORDS,
                                $row->EDUCATION_KEYWORDS,
                                $row->NAME,
                                $row->LOCATION_NAME);
            $jobs[$index] = $job;
            ++$index;
        }
        
        return $jobs;
    }
    
    public function search($title, $description, $skill)
    {
        $jobs = array();
        $index = 0;
        
        $title1 = "%" . trim($title) . "%";
        $description1 = "%" . trim($description) . "%";
        $skill1 = "%" . trim($skill) . "%";
        
        $rows = DB::select('SELECT j.*, o.NAME, l.LOCATION_NAME ' .
                           '  FROM jobs j, organizations o, locations l ' . 
                           ' WHERE j.ORGANIZATION_ID = o.ID ' .
                           '   AND j.LOCATION_ID = l.ID ' . 
                           '   AND j.POSITION_NAME LIKE ? ' .
                           '   AND j.DESCRIPTION LIKE ? ' .
                           '   AND j.SKILLS_KEYWORDS LIKE ? ' .
                           ' ORDER BY j.ID ASC', [$title1, $description1, $skill1]);
        
        foreach ($rows as $row)
        {
            $job = new JobModel($row->ID,
                                $row->ORGANIZATION_ID,
                                $row->LOCATION_ID,
                                $row->POSITION_NAME,
                                $row->DESCRIPTION,
                                $row->POSITION_TYPE,
                                $row->EXPIRE_DATE,
                                $row->SKILLS_KEYWORDS,
                                $row->EDUCATION_KEYWORDS,
                                $row->NAME,
                                $row->LOCATION_NAME);
            $jobs[$index] = $job;
            ++$index;
        }
        
        return $jobs;
    }
    
    public function getByID($job_id)
    {
        $row = DB::select('SELECT j.*, o.NAME, l.LOCATION_NAME FROM jobs j, organizations o, locations l WHERE j.ID = ? AND j.ORGANIZATION_ID = o.ID AND j.LOCATION_ID = l.ID', [$job_id]);
        
        $job = new JobModel($row[0]->ID,
                            $row[0]->ORGANIZATION_ID,
                            $row[0]->LOCATION_ID,
                            $row[0]->POSITION_NAME,
                            $row[0]->DESCRIPTION,
                            $row[0]->POSITION_TYPE,
                            $row[0]->EXPIRE_DATE,
                            $row[0]->SKILLS_KEYWORDS,
                            $row[0]->EDUCATION_KEYWORDS,
                            $row[0]->NAME,
                            $row[0]->LOCATION_NAME);

        return $job;
    }
    
    // Determine validity of job id - does a job exist for the id
    public function JobIDExists($job_id)
    {
        $exists = DB::select('SELECT COUNT(1) AS AMOUNT FROM jobs WHERE ID = ? ', [$job_id]);
        
        return ($exists[0]->AMOUNT > 0);
    }
    
    public function getJobAggregate($job_id)
    {
        $jobModel = $this->getByID($job_id);
        $daoOrg = new OrganizationDAO();
        $daoLoc = new LocationDAO();
        $organizationModel = $daoOrg->getByID($jobModel->getOrganization_id());
        $locationModel = $daoLoc->getByID($jobModel->getLocation_id());
        
        $job = new JobAggregateModel($jobModel, $organizationModel, $locationModel);
        
        return $job;
    }
    
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function update($jobModel)
    {
        $id = $jobModel->getId();
        $organization_id = $jobModel->getOrganization_id();
        $location_id = $jobModel->getLocation_id();
        $position_name = $jobModel->getPosition_name();
        $description = $jobModel->getDescription();
        $position_type = $jobModel->getPosition_type();
        $expire_date = $jobModel->getExpire_date();
        $skills_keywords = $jobModel->getSkills_keywords();
        $education_keywords = $jobModel->getEducation_keywords();

        $sql = 'UPDATE jobs SET ORGANIZATION_ID = ?, LOCATION_ID = ?, POSITION_NAME = ?, DESCRIPTION = ?, POSITION_TYPE = ?, EXPIRE_DATE = ?, SKILLS_KEYWORDS = ?, EDUCATION_KEYWORDS = ? WHERE ID = ?';
        $data = [$organization_id, $location_id, $position_name, $description, $position_type, $expire_date, $skills_keywords, $education_keywords, $id];
        $count = DB::update($sql, $data);
        
        return $count;
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($job_id)
    {
        $count = DB::delete('DELETE FROM jobs WHERE ID = ?', [$job_id]);
        
        return $count;
    }
    
}

