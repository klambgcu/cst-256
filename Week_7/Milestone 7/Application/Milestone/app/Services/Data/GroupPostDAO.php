<?php
namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\GroupPostModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Groups (Group Information)
 * 2. Various Group Posts CRUD
 * 3.
 * ---------------------------------------------------------------
 */

class GroupPostDAO
{
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($groupPostModel)
    {
        // $groupPostModel = new GroupPostModel($id, $group_id, $user_id, $post, $created_time);
        
        $group_id = $groupPostModel->getGroup_id();
        $user_id = $groupPostModel->getUser_id();
        $post = $groupPostModel->getPost();
        $created_time = $groupPostModel->getCreated_time();
        
        $sql = 'INSERT INTO groups_posts (GROUP_ID, USER_ID, POST, CREATED_TIME) VALUES (?, ?, ?, ?)';
        $data = [$group_id, $user_id, $post, $created_time];
        DB::insert($sql, $data);
        $id = DB::getPdo()->lastInsertId();
        
        return $id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $list = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups_posts g ORDER BY g.GROUP_ID ASC, g.ID ASC');
        
        foreach ($rows as $row)
        {
            $item = new GroupPostModel($row->ID,
                                       $row->GROUP_ID,
                                       $row->USER_ID,
                                       $row->POST,
                                       $row->CREATED_TIME);
            $list[$index] = $item;
            ++$index;
        }
        
        return $list;
    }
    
    public function getAllPostsForGroup($group_id)
    {
        $list = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups_posts g WHERE g.GROUP_ID = ? ORDER BY g.ID ASC', [$group_id]);
        
        foreach ($rows as $row)
        {
            $item = new GroupPostModel($row->ID,
                                       $row->GROUP_ID,
                                       $row->USER_ID,
                                       $row->POST,
                                       $row->CREATED_TIME);
            $list[$index] = $item;
            ++$index;
        }
        
        return $list;
    }

    public function getByID($id)
    {
        $row = DB::select('SELECT g.* FROM groups_posts g WHERE g.ID = ? ', [$id]);
        
        $item = new GroupPostModel($row[0]->ID,
                                   $row[0]->GROUP_ID,
                                   $row[0]->USER_ID,
                                   $row[0]->POST,
                                   $row[0]->CREATED_TIME);

        return $item;
    }
    
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function update($groupPostModel)
    {
        // $groupPostModel = new GroupPostModel($id, $group_id, $user_id, $post, $created_time);
        $id = $groupPostModel->getId();
        $post = $groupPostModel->getPost();
        $created_time = $groupPostModel->getCreated_time();
        
        $sql = 'UPDATE groups_posts SET POST = ?, CREATED_TIME = ? WHERE ID = ?';
        $data = [$post, $created_time, $id];
        $count = DB::update($sql, $data);
        
        return $count;
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($id)
    {
        $count = DB::delete('DELETE FROM groups_posts WHERE ID = ?', [$id]);
        
        return $count;
    }
    
    public function deleteAllPostsForGroup($group_id)
    {
        $count = DB::delete('DELETE FROM groups_posts WHERE GROUP_ID = ?', [$group_id]);
        
        return $count;
    }
}
