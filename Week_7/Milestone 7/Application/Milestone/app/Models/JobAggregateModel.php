<?php
namespace App\Models;

use JsonSerializable;

/*
* ---------------------------------------------------------------
* Name      : Kelly E. Lamb
* Date      : 2022-01-19
* Class     : CST-256 Database Application Programming III
* Professor : Dr. Todd Wolfe
* Assignment: Milestone
* Disclaimer: This is my own work
* ---------------------------------------------------------------
* Description:
* 1. Model to hold aggregate of all job information
* 2. (Job, Organization, Location)
* 3.
* ---------------------------------------------------------------
*/

class JobAggregateModel implements JsonSerializable
{
    private $jobModel;
    private $organizationModel;
    private $locationModel;
    
    public function __construct($jobModel, $organizationModel, $locationModel)
    {
        $this->jobModel = $jobModel;
        $this->organizationModel = $organizationModel;
        $this->locationModel = $locationModel;
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    /**
     * @return mixed
     */
    public function getJobModel()
    {
        return $this->jobModel;
    }

    /**
     * @return mixed
     */
    public function getOrganizationModel()
    {
        return $this->organizationModel;
    }

    /**
     * @return mixed
     */
    public function getLocationModel()
    {
        return $this->locationModel;
    }

    /**
     * @param mixed $jobModel
     */
    public function setJobModel($jobModel)
    {
        $this->jobModel = $jobModel;
    }

    /**
     * @param mixed $organizationModel
     */
    public function setOrganizationModel($organizationModel)
    {
        $this->organizationModel = $organizationModel;
    }

    /**
     * @param mixed $locationModel
     */
    public function setLocationModel($locationModel)
    {
        $this->locationModel = $locationModel;
    }
}

