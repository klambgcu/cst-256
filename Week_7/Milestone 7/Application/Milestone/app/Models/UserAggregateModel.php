<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-12
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold aggregate of all user information
 * 2. Member Profile
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Models;
use JsonSerializable;

class UserAggregateModel implements JsonSerializable
{
    private $userModel;
    private $userProfile;
    private $userWorkHistory;       // array
    private $userEducation;         // array
    private $userSkillset;          // array
    
    public function __construct($userModel, $userProfile, $userWorkHistory, $userEducation, $userSkillset)
    {
        $this->userModel = $userModel;
        $this->userProfile = $userProfile;
        $this->userWorkHistory = $userWorkHistory;
        $this->userEducation = $userEducation;
        $this->userSkillset = $userSkillset;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    /**
     * @return mixed
     */
    public function getUserModel()
    {
        return $this->userModel;
    }

    /**
     * @return mixed
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * @return mixed
     */
    public function getUserWorkHistory()
    {
        return $this->userWorkHistory;
    }

    /**
     * @return mixed
     */
    public function getUserEducation()
    {
        return $this->userEducation;
    }

    /**
     * @return mixed
     */
    public function getUserSkillset()
    {
        return $this->userSkillset;
    }

    /**
     * @param mixed $userModel
     */
    public function setUserModel($userModel)
    {
        $this->userModel = $userModel;
    }

    /**
     * @param mixed $userProfile
     */
    public function setUserProfile($userProfile)
    {
        $this->userProfile = $userProfile;
    }

    /**
     * @param mixed $userWorkHistory
     */
    public function setUserWorkHistory($userWorkHistory)
    {
        $this->userWorkHistory = $userWorkHistory;
    }

    /**
     * @param mixed $userEducation
     */
    public function setUserEducation($userEducation)
    {
        $this->userEducation = $userEducation;
    }

    /**
     * @param mixed $userSkillset
     */
    public function setUserSkillset($userSkillset)
    {
        $this->userSkillset = $userSkillset;
    }
}

