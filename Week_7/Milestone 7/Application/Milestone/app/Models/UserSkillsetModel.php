<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-012
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold user skillset
 * 2. Member Profile
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Models;
use JsonSerializable;

class UserSkillsetModel implements JsonSerializable
{
    private $id;
    private $user_id;
    private $skill_name;
    private $description;
    
    public function __construct($id, $user_id, $skill_name, $description)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->skill_name = $skill_name;
        $this->description = $description;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getSkill_name()
    {
        return $this->skill_name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $skill_name
     */
    public function setSkill_name($skill_name)
    {
        $this->skill_name = $skill_name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}

