<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-012
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold user work history
 * 2. Member Profile
 * 3. 
 * ---------------------------------------------------------------
 */

namespace App\Models;
use JsonSerializable;

class UserWorkHistoryModel implements JsonSerializable
{
    private $id;
    private $user_id;
    private $org_name;
    private $work_description;
    private $start_date;
    private $end_date;
    private $currently_employed;
    private $link_to_org;
    
    public function __construct($id, $user_id, $org_name, $work_description, $start_date, $end_date, $currently_employed, $link_to_org)
    {
        $this->id = $id;
        $this->user_id = $user_id;
        $this->org_name = $org_name;
        $this->work_description = $work_description;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->currently_employed = $currently_employed;
        $this->link_to_org = $link_to_org;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getOrg_name()
    {
        return $this->org_name;
    }

    /**
     * @return mixed
     */
    public function getWork_description()
    {
        return $this->work_description;
    }

    /**
     * @return mixed
     */
    public function getStart_date()
    {
        return $this->start_date;
    }

    /**
     * @return mixed
     */
    public function getEnd_date()
    {
        return $this->end_date;
    }

    /**
     * @return mixed
     */
    public function getCurrently_employed()
    {
        return $this->currently_employed;
    }

    /**
     * @return mixed
     */
    public function getLink_to_org()
    {
        return $this->link_to_org;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $org_name
     */
    public function setOrg_name($org_name)
    {
        $this->org_name = $org_name;
    }

    /**
     * @param mixed $work_description
     */
    public function setWork_description($work_description)
    {
        $this->work_description = $work_description;
    }

    /**
     * @param mixed $start_date
     */
    public function setStart_date($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEnd_date($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @param mixed $currently_employed
     */
    public function setCurrently_employed($currently_employed)
    {
        $this->currently_employed = $currently_employed;
    }

    /**
     * @param mixed $link_to_org
     */
    public function setLink_to_org($link_to_org)
    {
        $this->link_to_org = $link_to_org;
    }
}

