<?php
namespace App\Models;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold member group association information
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

class GroupMemberModel
{
    private $id;
    private $group_id;
    private $user_id;
    private $start_date;
    
    public function __construct($id, $group_id, $user_id, $start_date)
    {
        $this->id = $id;
        $this->group_id = $group_id;
        $this->user_id = $user_id;
        $this->start_date = $start_date;
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroup_id()
    {
        return $this->group_id;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getStart_date()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $group_id
     */
    public function setGroup_id($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $start_date
     */
    public function setStart_date($start_date)
    {
        $this->start_date = $start_date;
    }
}

