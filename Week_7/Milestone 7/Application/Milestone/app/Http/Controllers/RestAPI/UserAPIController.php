<?php
namespace App\Http\Controllers\RestAPI;

use App\Models\DTO;
use App\Services\Business\UserService;
use App\Services\Utility\ILoggerService;
use Exception;
use Illuminate\Http\Response;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2023-01-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. User API Controller
 * 2. Handles Rest API Functionality
 * 3. JSON User Information By Id
 * ---------------------------------------------------------------
 */

class UserAPIController
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->logger->info("Entering UserAPIController::show()");
        $service = new UserService();
        $data = [];
        
        if (! is_numeric($id))
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be numeric. id = " . $id, $data);
        }
        else if ($id <> floor($id))
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be an integer. id = " . $id, $data);
        }
        else if ($id <= 0)
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be a positive integer. id = " . $id, $data);
        }
        else 
        {
            try
            {
                if (! $service->UserIDExists($id))
                {
                    $status = 404;
                    $results = new DTO($status, "Not Found: User not found: id = " . $id, $data);
                }
                else 
                {
                    $data = $service->getUserAggregate($id);
                    $status = 200;
                    $results = new DTO($status, "OK: User information for id = " . $id, $data);
                }
                
            } catch (Exception $e)
            {
                $status = 500;
                $results = new DTO($status, "Internal Server Error: Please try again later or contact administrator. id = " . $id, $data);
                $this->logger->error("Internal Server Error: UserAPIController::show() " . $e->getMessage());
            }
        }
        
        $content = json_encode($results);
        
        $this->logger->info("Exiting UserAPIController::show()");
        return (new Response($content, $status))->header('Content-Type', 'application/json; charset=utf-8');
    }
    
}

