<?php
namespace App\Http\Controllers;

use App\Models\GroupMemberModel;
use App\Services\Business\GroupService;
use App\Services\Utility\ILoggerService;
use Illuminate\Http\Request;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2023-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Group Controller
 * 2. Handles Groups, Membership, Posts Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

class GroupController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    // Display the group listing
    public function showGroupListing()
    {
        $this->logger->info("Entering GroupController::showGroupListing()");
        if ($this->assertLogin()) return view('login');
        $user = $_SESSION['principal'];
        
        $service = new GroupService();
        $groupsList = $service->getAllGroupsSetMemberTag($user->getId());
        
        $this->logger->info("Exiting GroupController::showGroupListing()");
        return view('groupListing')->with(['groupsList' => $groupsList]);
    }

    // Handle the action requested by user
    public function doGroupAction(Request $request)
    {
        $this->logger->info("Entering GroupController::doGroupAction()");
        if ($this->assertLogin()) return view('login');

        $group_id = $request->input('id');
        $mode     = $request->input('mode');
        $user     = $_SESSION['principal'];
        $user_id  = $user->getId();
        $date = new \DateTime();
        $start_date = $date->format("Y-m-d");
        
        if ($mode == 0)
        {
            // Gather data required to view group
            $service = new GroupService();
            $membersUserModelList = $service->getAllMemberUserModelsForGroup($group_id);
            $postsList = $service->getAllPostsForGroup($group_id);
            $group = $service->getGroupByID($group_id);
            $isMember = $service->isGroupMember($user_id, $group_id);            

            $data = ['membersUserModelList' => $membersUserModelList,
                     'postsList' => $postsList,
                     'group' => $group,
                     'isMember' => $isMember];
            $this->logger->info("Exiting GroupController::doGroupAction() - groupView");
            return view('groupView')->with($data);
        }
        else if ($mode == 1)
        {
            // Join user to the group
            $groupMemberModel = New GroupMemberModel(0, $group_id, $user_id, $start_date);
            
            $service = new GroupService();
            $service->createGroupMember($groupMemberModel);
            $this->logger->info("Exiting GroupController::doGroupAction() - userGroup 1");
            return redirect('userGroup');
        }
        else if ($mode == 2)
        {
            // Remove the user from this group
            $service = new GroupService();
            $service->removeUserFromGroup($group_id, $user_id);
            $this->logger->info("Exiting GroupController::doGroupAction() - userGroup 2");
            return redirect('userGroup');
        }
        
        $this->logger->info("Exiting GroupController::doGroupAction() - userGroup");
        return redirect('userGroup');
    }
}
