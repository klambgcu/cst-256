<?php
namespace App\Http\Controllers;

use App\Services\Business\JobListingService;
use App\Services\Utility\ILoggerService;
use Illuminate\Http\Request;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2023-01-16
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Job Controller
 * 2. Handles Job Search Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class JobController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }

    // Display the initial job search form
    public function showJobSearch(Request $request)
    {
        $this->logger->info("Entering JobController::showJobSearch()");
        if ($this->assertLogin()) return view('login');

        $jobsList = array();
        $data = ['jobsList' => $jobsList, 'position' => "", 'description' => "", 'skill' => ""];
        
        $this->logger->info("Exiting JobController::showJobSearch()");
        return view('jobListing')->with($data);
    }
    
    // Display the job search form using input
    public function doJobSearch(Request $request)
    {
        $this->logger->info("Entering JobController::doJobSearch()");
        if ($this->assertLogin()) return view('login');

        $position    = $request->input('Position', "");
        $description = $request->input('Description', "");
        $skill       = $request->input('Skill', "");
        
        $service = new JobListingService();
        $jobsList = $service->searchJobs($position, $description, $skill);

        $data = ['jobsList' => $jobsList, 'position' => $position, 'description' => $description, 'skill' => $skill];
        
        $this->logger->info("Exiting JobController::doJobSearch()");
        return view('jobListing')->with($data);
    }

    // Display all information pertaining to the job 
    public function doJobView(Request $request)
    {
        $this->logger->info("Entering JobController::doJobView()");
        if ($this->assertLogin()) return view('login');
        
        $job_id = $request->input('id');
        
        $service = new JobListingService();
        $job = $service->getJobByID($job_id);
        $org = $service->getOrganizationByID($job->getOrganization_id());
        $loc = $service->getLocationByID($job->getLocation_id());
        
        $data = ['job' => $job, 'organization' => $org, 'location' => $loc];
        
        $this->logger->info("Exiting JobController::doJobView()");
        return view('jobView')->with($data);
    }
}
