<?php 

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Application Master Layout
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */
?>

<footer class="footer">
  <div class="container">
    <h5><span class="text-muted">Copyright @2021 GetJobs</span></h5>
  </div>
</footer>
