<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Layout Header - Main Menu
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
 
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);">
  <div class="container-fluid">
    <a class="navbar-brand" href="home">GetJobs</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

<?php
if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}
if (isset($_SESSION['principal']))
{
    //
    // User is logged into the application
    //
    $user = $_SESSION['principal'];
    $username = $user->getFirst_name() . " " . $user->getLast_name();
    $role_id = $user->getRole_id();
    
    echo "  <ul class='navbar-nav me-auto mb-2 mb-lg-0'>\r\n";
    echo "      <li class='nav-item'><a class='nav-link active' aria-current='page' href='home'>Home</a></li>\r\n";
    echo "      <li class='nav-item'><a class='nav-link active' aria-current='page' href='jobSearch'>Job Search</a></li>\r\n";
    echo "      <li class='nav-item dropdown'>\r\n";
    echo "          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Account</a>\r\n";
    echo "          <ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\r\n";
    echo "              <li><a class='dropdown-item' href='register'>Registration...</a></li>\r\n";
    echo "              <li><a class='dropdown-item' href='memberprofile'>Profile...</a></li>\r\n";
    echo "              <li><a class='dropdown-item' href='userGroup'>Groups...</a></li>\r\n";
    echo "              <li><hr class='dropdown-divider'></li>\r\n";
    echo "              <li><a class='dropdown-item' href='logout'>Log Out</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";

    // ADMINISTRATOR MENU Role ID = (Basic = 1,  User Maint = 2, Group Maint = 3, Admin (Both) = 4)
    if ($role_id > 1)
    {
        echo "      <li class='nav-item dropdown'>\r\n";
        echo "          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Administrator</a>\r\n";
        echo "          <ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\r\n";
        
        if ($role_id == 2 || $role_id == 4)
        {
            echo "              <li><a class='dropdown-item' href='adminUser'>User Admin...</a></li>\r\n";
        }
        
        if ($role_id == 3 || $role_id == 4)
        {
            echo "              <li><a class='dropdown-item' href='adminGroup'>Group Admin...</a></li>\r\n";
        }
        
        if ($role_id == 4)
        {
            echo "              <li><hr class='dropdown-divider'></li>\r\n";
            echo "              <li><a class='dropdown-item' href='adminJob'>Job Posting Admin...</a></li>\r\n";
        }
        
        echo "          </ul>\r\n";
        echo "      </li>\r\n";
    }
    
    
    // Add user name to top right menu
    echo "  </ul>\r\n";
    echo "  <ul class='navbar-nav d-flex'>\r\n";
    echo "    <li class='nav-item'>\r\n";
    echo "      <p class='nav-link btn btn-outline-success'>" . $username . "</p>\r\n";
    echo "    </li>\r\n";
    echo "  </ul>\r\n";
    
}
else
{
    //
    // User is NOT logged into the application
    //
    echo "  <ul class='navbar-nav me-auto mb-2 mb-lg-0'>\r\n";
    echo "      <li class='nav-item'><a class='nav-link active' aria-current='page' href='home'>Home</a></li>\r\n";
    echo "      <li class='nav-item dropdown'>\r\n";
    echo "          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'>Account</a>\r\n";
    echo "          <ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\r\n";
    echo "              <li><a class='dropdown-item' href='register'>Registration...</a></li>\r\n";
    echo "              <li><hr class='dropdown-divider'></li>\r\n";
    echo "              <li><a class='dropdown-item' href='login'>Login...</a></li>\r\n";
    echo "          </ul>\r\n";
    echo "      </li>\r\n";
    echo "  </ul>\r\n";
}
?>
    </div>
  </div>
</nav>

<!-- Put the Store Name / Title Here -->
<div align="center">
	<br /><img src="{{asset('images/logo.jpg')}}" alt="GetJobs Logo"/>
	<hr>
</div>

<style>
.form-floating > .form-control::placeholder {
    color: revert;
}

.form-floating > .form-control:not(:focus)::placeholder {
    color: transparent;
}
</style>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
