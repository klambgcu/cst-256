<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Job View
 * 2. Create / Edit Job Entries
 * 3.
 * ---------------------------------------------------------------
 */

if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}

$user = $_SESSION['principal'];

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Job Page')
@section('content')

<div class="container">

	<form action="doAdminJob" method="POST">
	@csrf
		<div align="center">
		    <h1>Job View Form</h1>
		    <p>Please enter job information.</p>
		    <hr>
		</div>

    <h3>Job Information:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Name of Position" name="PositionName" id="PositionName" value = "<?php echo $job->getPosition_name(); ?>" required>
        		    <label for="PositionName" class="form-label">Position Name:</label>
				</div>
			</div>
			<div class="col-5	">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Description" name="Description" id="Description" value = "<?php echo $job->getDescription(); ?>" required>
        		    <label for="Description" class="form-label">Description:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
        		    <input type="date" class="form-control" placeholder="Expire Date" name="ExpireDate" id="ExpireDate" value = "<?php echo $job->getExpire_date(); ?>" required>
        		    <label for="ExpireDate" class="form-label">Expire Date:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>


		<div class="row">
			<div class="col-1"></div>
			<div class="col-5">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Education Required (Comma Delimited)" name="Education" id="Education" value = "<?php echo $job->getEducation_keywords(); ?>" required>
        		    <label for="Education" class="form-label">Education:</label>
				</div>
			</div>
			<div class="col-5	">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Skills Required (Comma Delimited)" name="Skills" id="Skills" value = "<?php echo $job->getSkills_keywords(); ?>" required>
        		    <label for="Skills" class="form-label">Skills:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
 					<select class="form-select" aria-label="Default select example" name="OrgID" id="OrgID" required>
<?php
foreach($orgsList as $o)
{
    echo "                  <option value='" . $o->getId() . "'";
    echo ($job->getOrganization_id() == $o->getId()) ? " selected>" : ">";
    echo $o->getName() . "</option>\n";
}

?>
        			</select>
					<label for="OrgID" class="form-label">Organization:</label>
				</div>
			</div>
			<div class="col-5	">
				<div class="form-floating mb-3 mt-3">
 					<select class="form-select" aria-label="Default select example" name="LocID" id="LocID" required>
<?php
foreach($locList as $o)
{
    echo "                  <option value='" . $o->LOC_ID . "'"; 
    echo ($job->getLocation_id() == $o->LOC_ID) ? " selected>" : ">";
    echo $o->NAME_COMBO . "</option>\n";
}
?>
        			</select>
        		    <label for="LocID" class="form-label">Location:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
 					<select class="form-select" aria-label="Default select example" name="PosType" id="PosType" required>
<?php
echo "                  <option value='0' ";
echo ($job->getPosition_type() == 0) ? "selected " : "";
echo ">Full-Time</option>\n";
echo "                  <option value='1' ";
echo ($job->getPosition_type() == 1) ? "selected" : "";
echo ">Part-Time</option>\n";
echo "                  <option value='2' ";
echo ($job->getPosition_type() == 2) ? "selected" : "";
echo ">Contract</option>\n";
?>

        			</select>
        		    <label for="PosType" class="form-label">Type:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
		<hr>
		<input type="hidden" name="JobID" id="JobID" value="<?php echo $job->getId(); ?>">
		<button type="submit" class="btn btn-primary">Save Changes</button>
	</form>
</div>

@endsection
