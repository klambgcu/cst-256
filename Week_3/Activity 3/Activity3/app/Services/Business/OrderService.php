<?php
namespace App\Services\Business;


use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;

class OrderService
{

    public function __construct()
    {}
    
    public function createOrder($firstName, $lastName, $product)
    {
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            die("Connection failed: " . $connection->connect_error);
        }

        $commit = false;
        $connection->autocommit(FALSE);
        $connection->begin_transaction();
        
        $customerDAO = new CustomerDAO($connection);
        $id = $customerDAO->addCustomer($firstName, $lastName);
        
        if ($id > 0)
        {
            $orderDAO = new OrderDAO($connection);
            $id = $orderDAO->addOrder($id, $product);

            if ($id > 0)
            {
                $commit = true;
            }
        }

        if ($commit)
            $connection->commit();
        else 
            $connection->rollback();
        
        $connection->close();
        
        return $commit;
    }
}
