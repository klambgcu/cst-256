<?php
namespace App\Services\Data;

use App\Models\UserModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-17
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Customer DAO (Data Access Object)
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

class CustomerDAO
{
    private $connection;
    
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function addCustomer($firstName, $lastName)
    {
        $id = 0;
        $query = "INSERT INTO CUSTOMER (FIRST_NAME, LAST_NAME) VALUES (?, ?)";
        if ($statement = $this->connection->prepare($query))
        {
            $statement->bind_param('ss', $firstName, $lastName);
            $statement->execute();
            $id = $statement->insert_id;
            $statement->close();
        }
                
        return $id;        
    }
    
    
    /*--------------------------------------------------
     * Method with Auto Commit Enabled
     
     
    public function addCustomer($firstName, $lastName)
    {
        $connection = new \mysqli("localhost","root","root","activity2");
        
        // Check connection
        if ($connection->connect_error)
        {
            die("Connection failed: " . $connection->connect_error);
        }
        
        $id = 0;
        $query = "INSERT INTO CUSTOMER (FIRST_NAME, LAST_NAME) VALUES (?, ?)";
        if ($statement = $connection->prepare($query))
        {
            $statement->bind_param('ss', $firstName, $lastName);
            $statement->execute();
            $id = $statement->insert_id;
            $statement->close();
        }
        
        $connection->close();
        
        return $id;
    }
    
    *
    *--------------------------------------------------*/
    
}

