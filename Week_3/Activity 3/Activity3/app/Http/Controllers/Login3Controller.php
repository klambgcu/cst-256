<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Services\Business\SecurityService;
use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;
use App\Services\Business\OrderService;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-17
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 3
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handles Login Functionality
 * 3.
 * ---------------------------------------------------------------
 */

class Login3Controller extends Controller
{
    public function index(Request $request)
    {
        // Validate the Form Data (note will automatically redirect back to Login View if errors)
        $this->validateForm($request);
        
        $username = $request->input('username');
        $password = $request->input('password');

        $userModel = new UserModel($username, $password);
        
        $service = new SecurityService();
        $results = $service->login($userModel);

 /* --------------------------------------------
  * First Pass - Test DAO
  * Second & Subsequent Passes - Test Service

        // Test CustomerDAO
        $testCustomer = new CustomerDAO();
        $id = $testCustomer->addCustomer("Test First 1", "Test Last 1");
        
        echo "Test Customer Results: ID=" . $id . "<br>";
        
        // Test OrderDAO
        $testOrder = new OrderDAO();
        $id = $testOrder->addOrder($id, "Test Product 1");
        
        echo "Test Order Results: ID=" . $id . "<br>";
        
  * -------------------------------------------- */
        
        $orderService = new OrderService();
        $result = $orderService->createOrder("Test First 2", "Test Last 2", "Test Product 2");
        
        echo "Test Service Results: " . $result . "<br>";
        
        
        
        exit;
        
        
        
        
        
        
        if ($results)
        {
            $data = ['userModel' => $userModel];
            return view('loginPassed2')->with($data);
        }
        else 
        {
            return view('loginFailed');
        }
    }
    
    private function validateForm(Request $request)
    {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,10 | Alpha', 'password' => 'Required | Between:4,10'];
        
        // Run Data Validation Rules
        $this->validate($request, $rules);
    }
}
