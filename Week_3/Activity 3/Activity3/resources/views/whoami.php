<?php 

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-08
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Activity 2
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Test view for the WhatsMyNameController
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WhoAmI</title>
	</head>
<body>

    <form action = "whoami" method = "POST">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
        <h2> What's Your Name?</h2>
        <table>
        	<tr>
        		<td>First Name: </td>
        		<td><input type = "text" name = "firstname" /></td>
        	</tr>
        
        	<tr>
        		<td>Last Name:</td>
        		<td><input type = "text" name = "lastname" /></td>
        	</tr>
        	<tr>
        		<td colspan = "2" align = "center">
        			<input type = "submit" value = "Ask Now" />
        		</td>
        </table>
	</form>
</body>
</html>