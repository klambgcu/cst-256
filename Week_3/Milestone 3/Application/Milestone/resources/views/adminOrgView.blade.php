<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Organization View
 * 2. Create / Edit Organization/Location Entries
 * 3.
 * ---------------------------------------------------------------
 */

if (session_status() === PHP_SESSION_NONE)
{
    session_start();
}

$user = $_SESSION['principal'];

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Organization Page')
@section('content')

<div class="container">

	<form action="doAdminOrganization" method="POST">
	@csrf
		<div align="center">
		    <h1>Organization View Form</h1>
		    <p>Please enter organization &amp; location(s) information.</p>
		    <hr>
		</div>
	    <h3>Organization Information:</h3>
		<div class="row">
			<div class="col-1"></div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Name of Organization" name="Name" id="Name" value = "<?php echo $org->getName(); ?>" required>
        		    <label for="Name" class="form-label">Organization Name:</label>
				</div>
			</div>
			<div class="col-3">
				<div class="form-floating mb-3 mt-3">
        		    <input type="url" class="form-control" placeholder="Website" name="Website" id="Website" value = "<?php echo $org->getWebsite(); ?>" required>
        		    <label for="Website" class="form-label">Website:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
        		    <input type="tel" class="form-control" placeholder="(999) 999-9999" name="Phone" id="Phone" value = "<?php echo $org->getPhone(); ?>" pattern="\([0-9]{3}\) [0-9]{3}-[0-9]{4}" required>
        		    <label for="Phone" class="form-label">Phone:</label>
				</div>
			</div>
			<div class="col-2">
				<div class="form-floating mb-3 mt-3">
        		    <input type="email" class="form-control" placeholder="Email" name="Email" id="Email" value = "<?php echo $org->getEmail(); ?>" required>
        		    <label for="Email" class="form-label">Email:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text"  class="form-control" placeholder="Description" name="Description" id="Description" value = "<?php echo $org->getDescription(); ?>" required>
        		    <label for="Description" class="form-label">Description:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text" class="form-control" placeholder="Mission Statement" name="Mission" id="Mission" value = "<?php echo $org->getMission_statement(); ?>" required>
        		    <label for="Mission" class="form-label">Mission Statement:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-10">
				<div class="form-floating mb-3 mt-3">
        		    <input type="text"  class="form-control" placeholder="Ethics Statement" name="Ethics" id="Ethics" value = "<?php echo $org->getEthics_statement(); ?>" required>
        		    <label for="Ethics" class="form-label">Ethics Statement:</label>
				</div>
			</div>
			<div class="col-1"></div>
		</div>
		<hr>

	<div id="Location">
	<h3>Location Information:</h3>
    <button onclick="addLocation()">Add</button>
<script>
function addLocation() {
    document.getElementById("Location").innerHTML += 
      "<div class='row'><div class='col-1'></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Location Name' name='LocName[]' id='LocName' value = '' required><label for='LocName' class='form-label'>Location Name:</label></div></div><div class='col-3'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='Street Address' name='Street[]' id='Street' value = '' required><label for='Street' class='form-label'>Street:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><input type='text' class='form-control' placeholder='City' name='City[]' id='City' value = '' required><label for='City' class='form-label'>City:</label></div></div><div class='col-2'><div class='form-floating mb-3 mt-3'><select class='form-select' aria-label='Default select example' name='State[]' id='State' required><option value='AL'>Alabama</option><option value='AK'>Alaska</option><option value='AZ'>Arizona</option><option value='AR'>Arkansas</option><option value='CA'>California</option><option value='CO'>Colorado</option><option value='CT'>Connecticut</option><option value='DE'>Delaware</option><option value='DC'>District Of Columbia</option><option value='FL'>Florida</option><option value='GA'>Georgia</option><option value='HI'>Hawaii</option><option value='ID'>Idaho</option><option value='IL'>Illinois</option><option value='IN'>Indiana</option><option value='IA'>Iowa</option><option value='KS'>Kansas</option><option value='KY'>Kentucky</option><option value='LA'>Louisiana</option><option value='ME'>Maine</option><option value='MD'>Maryland</option><option value='MA'>Massachusetts</option><option value='MI'>Michigan</option><option value='MN'>Minnesota</option><option value='MS'>Mississippi</option><option value='MO'>Missouri</option><option value='MT'>Montana</option><option value='NE'>Nebraska</option><option value='NV'>Nevada</option><option value='NH'>New Hampshire</option><option value='NJ'>New Jersey</option><option value='NM'>New Mexico</option><option value='NY'>New York</option><option value='NC'>North Carolina</option><option value='ND'>North Dakota</option><option value='OH'>Ohio</option><option value='OK'>Oklahoma</option><option value='OR'>Oregon</option><option value='PA'>Pennsylvania</option><option value='RI'>Rhode Island</option><option value='SC'>South Carolina</option><option value='SD'>South Dakota</option><option value='TN'>Tennessee</option><option value='TX'>Texas</option><option value='UT'>Utah</option><option value='VT'>Vermont</option><option value='VA'>Virginia</option><option value='WA'>Washington</option><option value='WV'>West Virginia</option><option value='WI'>Wisconsin</option><option value='WY'>Wyoming</option></select><label for='State' class='form-label'>State:</label></div></div><div class='col-1'><div class='form-floating mb-3 mt-3'><input type='hidden' name='LocID[]' id='LocID' value = '0'><input type='text' class='form-control' placeholder='Zip' name='ZipCode[]' id='ZipCode' value = '' required><label for='ZipCode' class='form-label'>Zip:</label></div></div><div class='col-1'></div></div>";
}
</script>

<?php
foreach($locList as $l)
{
    echo "<div class='row'>\n";
    echo "	<div class='col-1'></div>\n";
    echo "	<div class='col-2'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<input type='text' class='form-control' placeholder='Location Name' name='LocName[]' id='LocName' value = '" . $l->getLocation_name() . "' required>\n";
    echo "			<label for='LocName' class='form-label'>Location Name:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-3'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<input type='text' class='form-control' placeholder='Street Address' name='Street[]' id='Street' value = '" . $l->getStreet() . "' required>\n";
    echo "			<label for='Street' class='form-label'>Street:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-2'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<input type='text' class='form-control' placeholder='City' name='City[]' id='City' value = '" . $l->getCity() . "' required>\n";
    echo "			<label for='City' class='form-label'>City:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-2'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<select class='form-select' aria-label='Default select example' name='State[]' id='State' required>\n";
    echo "				<option value='AL'" . (($l->getState() == 'AL') ? ' selected' : '') . ">Alabama</option>\n";
    echo "				<option value='AK'" . (($l->getState() == 'AK') ? ' selected' : '') . ">Alaska</option>\n";
    echo "				<option value='AZ'" . (($l->getState() == 'AZ') ? ' selected' : '') . ">Arizona</option>\n";
    echo "				<option value='AR'" . (($l->getState() == 'AR') ? ' selected' : '') . ">Arkansas</option>\n";
    echo "				<option value='CA'" . (($l->getState() == 'CA') ? ' selected' : '') . ">California</option>\n";
    echo "				<option value='CO'" . (($l->getState() == 'CO') ? ' selected' : '') . ">Colorado</option>\n";
    echo "				<option value='CT'" . (($l->getState() == 'CT') ? ' selected' : '') . ">Connecticut</option>\n";
    echo "				<option value='DE'" . (($l->getState() == 'DE') ? ' selected' : '') . ">Delaware</option>\n";
    echo "				<option value='DC'" . (($l->getState() == 'DC') ? ' selected' : '') . ">District Of Columbia</option>\n";
    echo "				<option value='FL'" . (($l->getState() == 'FL') ? ' selected' : '') . ">Florida</option>\n";
    echo "				<option value='GA'" . (($l->getState() == 'GA') ? ' selected' : '') . ">Georgia</option>\n";
    echo "				<option value='HI'" . (($l->getState() == 'HI') ? ' selected' : '') . ">Hawaii</option>\n";
    echo "				<option value='ID'" . (($l->getState() == 'ID') ? ' selected' : '') . ">Idaho</option>\n";
    echo "				<option value='IL'" . (($l->getState() == 'IL') ? ' selected' : '') . ">Illinois</option>\n";
    echo "				<option value='IN'" . (($l->getState() == 'IN') ? ' selected' : '') . ">Indiana</option>\n";
    echo "				<option value='IA'" . (($l->getState() == 'IA') ? ' selected' : '') . ">Iowa</option>\n";
    echo "				<option value='KS'" . (($l->getState() == 'KS') ? ' selected' : '') . ">Kansas</option>\n";
    echo "				<option value='KY'" . (($l->getState() == 'KY') ? ' selected' : '') . ">Kentucky</option>\n";
    echo "				<option value='LA'" . (($l->getState() == 'LA') ? ' selected' : '') . ">Louisiana</option>\n";
    echo "				<option value='ME'" . (($l->getState() == 'ME') ? ' selected' : '') . ">Maine</option>\n";
    echo "				<option value='MD'" . (($l->getState() == 'MD') ? ' selected' : '') . ">Maryland</option>\n";
    echo "				<option value='MA'" . (($l->getState() == 'MA') ? ' selected' : '') . ">Massachusetts</option>\n";
    echo "				<option value='MI'" . (($l->getState() == 'MI') ? ' selected' : '') . ">Michigan</option>\n";
    echo "				<option value='MN'" . (($l->getState() == 'MN') ? ' selected' : '') . ">Minnesota</option>\n";
    echo "				<option value='MS'" . (($l->getState() == 'MS') ? ' selected' : '') . ">Mississippi</option>\n";
    echo "				<option value='MO'" . (($l->getState() == 'MO') ? ' selected' : '') . ">Missouri</option>\n";
    echo "				<option value='MT'" . (($l->getState() == 'MT') ? ' selected' : '') . ">Montana</option>\n";
    echo "				<option value='NE'" . (($l->getState() == 'NE') ? ' selected' : '') . ">Nebraska</option>\n";
    echo "				<option value='NV'" . (($l->getState() == 'NV') ? ' selected' : '') . ">Nevada</option>\n";
    echo "				<option value='NH'" . (($l->getState() == 'NH') ? ' selected' : '') . ">New Hampshire</option>\n";
    echo "				<option value='NJ'" . (($l->getState() == 'NJ') ? ' selected' : '') . ">New Jersey</option>\n";
    echo "				<option value='NM'" . (($l->getState() == 'NM') ? ' selected' : '') . ">New Mexico</option>\n";
    echo "				<option value='NY'" . (($l->getState() == 'NY') ? ' selected' : '') . ">New York</option>\n";
    echo "				<option value='NC'" . (($l->getState() == 'NC') ? ' selected' : '') . ">North Carolina</option>\n";
    echo "				<option value='ND'" . (($l->getState() == 'ND') ? ' selected' : '') . ">North Dakota</option>\n";
    echo "				<option value='OH'" . (($l->getState() == 'OH') ? ' selected' : '') . ">Ohio</option>\n";
    echo "				<option value='OK'" . (($l->getState() == 'OK') ? ' selected' : '') . ">Oklahoma</option>\n";
    echo "				<option value='OR'" . (($l->getState() == 'OR') ? ' selected' : '') . ">Oregon</option>\n";
    echo "				<option value='PA'" . (($l->getState() == 'PA') ? ' selected' : '') . ">Pennsylvania</option>\n";
    echo "				<option value='RI'" . (($l->getState() == 'RI') ? ' selected' : '') . ">Rhode Island</option>\n";
    echo "				<option value='SC'" . (($l->getState() == 'SC') ? ' selected' : '') . ">South Carolina</option>\n";
    echo "				<option value='SD'" . (($l->getState() == 'SD') ? ' selected' : '') . ">South Dakota</option>\n";
    echo "				<option value='TN'" . (($l->getState() == 'TN') ? ' selected' : '') . ">Tennessee</option>\n";
    echo "				<option value='TX'" . (($l->getState() == 'TX') ? ' selected' : '') . ">Texas</option>\n";
    echo "				<option value='UT'" . (($l->getState() == 'UT') ? ' selected' : '') . ">Utah</option>\n";
    echo "				<option value='VT'" . (($l->getState() == 'VT') ? ' selected' : '') . ">Vermont</option>\n";
    echo "				<option value='VA'" . (($l->getState() == 'VA') ? ' selected' : '') . ">Virginia</option>\n";
    echo "				<option value='WA'" . (($l->getState() == 'WA') ? ' selected' : '') . ">Washington</option>\n";
    echo "				<option value='WV'" . (($l->getState() == 'WV') ? ' selected' : '') . ">West Virginia</option>\n";
    echo "				<option value='WI'" . (($l->getState() == 'WI') ? ' selected' : '') . ">Wisconsin</option>\n";
    echo "				<option value='WY'" . (($l->getState() == 'WY') ? ' selected' : '') . ">Wyoming</option>\n";
    echo "			</select>\n";
    echo "			<label for='State' class='form-label'>State:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-1'>\n";
    echo "		<div class='form-floating mb-3 mt-3'>\n";
    echo "			<input type='hidden' name='LocID[]' id='LocID' value = '" . $l->getId() . "'>\n";
    echo "			<input type='text' class='form-control' placeholder='Zip' name='ZipCode[]' id='ZipCode' value = '" . $l->getPostal_code() . "' required>\n";
    echo "			<label for='ZipCode' class='form-label'>Zip:</label>\n";
    echo "		</div>\n";
    echo "	</div>\n";
    echo "	<div class='col-1'></div>\n";
    echo "</div>\n";
    
}

?>

	</div>
		<hr>
		<input type="hidden" name="OrgID" id="OrgID" value="<?php echo $org->getId(); ?>">
		<button type="submit" class="btn btn-primary">Save Changes</button>
	</form>
</div>

@endsection
