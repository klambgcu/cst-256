<?php

use App\Models\UserModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin User Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin User Listing')
@section('content')

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	@csrf
	<div align="center">
	    <h1>Admin User Maintenance Listing</h1>
	    <p></p>
	    <hr>
	</div>

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Password</th>
        <th>Birthdate</th>
        <th>Gender</th>
        <th>Role</th>
        <th>Suspended</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php
    $gender = ["Male", "Female"];
    $suspend = ["No", "Yes"];

    foreach ($usersList as $u)
    {
        echo "  <tr>\n";
        echo "      <td>" . $u->getId() . "</td>\n";
        echo "      <td>" . $u->getFirst_name() . "</td>\n";
        echo "      <td>" . $u->getLast_name() . "</td>\n";
        echo "      <td>" . $u->getEmail() . "</td>\n";
        echo "      <td>" . $u->getMobile() . "</td>\n";
        echo "      <td>" . $u->getPassword() . "</td>\n";
        echo "      <td>" . $u->getBirthdate() . "</td>\n";
        echo "      <td>" . $gender[$u->getGender()] . "</td>\n";
        echo "      <td>" . $u->getRole_id() . "</td>\n";
        echo "      <td>" . $suspend[$u->getSuspended()] . "</td>\n";
        echo "      <td>" . "<a href='adminChangeUser?id=" . $u->getId() . "&mode=0'>Edit<a>" . 
            "&nbsp;|&nbsp;<a href='adminChangeUser?id=" . $u->getId() . "&mode=1' onclick=\"return confirm('Confirmation Required. Delete " . $u->getFirst_name() . " Y/N ?')\">Delete<a></td>\n";
        
        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>

</div>

@endsection

