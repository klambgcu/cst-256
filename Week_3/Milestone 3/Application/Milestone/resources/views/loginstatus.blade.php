<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Welcome Page (welcome.blade.php)
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Login Status Page')
@section('content')

<div class="container">
    <div align='center'>
    <h1>
    <?php echo $_SESSION["message"]; ?>
    </h1>
    </div>
    <hr>
</div>

@endsection

