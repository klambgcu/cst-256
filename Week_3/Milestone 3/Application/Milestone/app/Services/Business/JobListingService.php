<?php


/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service - JobListing 
 * 2. Various CRUD - Jobs, Locations, Organization
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Business;

use App\Services\Data\JobDAO;
use App\Services\Data\LocationDAO;
use App\Services\Data\OrganizationDAO;

class JobListingService
{

    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Job DAO Specific Methods
    // -------------------------------------------------------------------
    
    public function createJob($jobModel)
    {
        $dao = new JobDAO();
        if ($jobModel->getId() == 0)
        {
            // ID <= 0 -- CREATE NEW JOB
            return $dao->create($jobModel);
        }
        else 
        {
            // ID > 0 -- UPDATE EXISTING JOB
            return $dao->update($jobModel);
        }
    }
    
    public function getAllJobs()
    {
        $dao = new JobDAO();
        return $dao->getAll();
    }
    
    public function getJobByID($job_id)
    {
        $dao = new JobDAO();
        return $dao->getByID($job_id);
    }

    public function updateJob($jobModel)
    {
        $dao = new JobDAO();
        return $dao->update($jobModel);
    }
    
    public function deleteJobByID($job_id)
    {
        $dao = new JobDAO();
        return $dao->deleteByID($job_id);
    }
        
    
    // -------------------------------------------------------------------
    // Location DAO Specific Methods
    // -------------------------------------------------------------------
    
    public function createLocation($locationModel)
    {
        $dao = new LocationDAO();
        return $dao->create($locationModel);
    }
    
    public function getAllLocations()
    {
        $dao = new LocationDAO();
        return $dao->getAll();
    }
    
    public function getLocationByID($location_id)
    {
        $dao = new LocationDAO();
        return $dao->getByID($location_id);
    }
    
    public function getLocationsByOrgID($organization_id)
    {
        $dao = new LocationDAO();
        return $dao->getLocationsByOrgID($organization_id);
    }
    
    public function updateLocation($locationModel)
    {
        $dao = new LocationDAO();
        return $dao->update($locationModel);
    }
    
    public function deleteLocationByID($location_id)
    {
        // CASCADE DELETES TO JOBS VIA FOREIGN KEY
        $dao = new LocationDAO();
        return $dao->deleteByID($location_id);
    }
    
    
    // -------------------------------------------------------------------
    // Organization DAO Specific Methods
    // -------------------------------------------------------------------
    
    public function createOrganization($organizationModel)
    {
        $dao = new OrganizationDAO();
        return $dao->create($organizationModel);
    }
    
    public function getAllOrganizations()
    {
        $dao = new OrganizationDAO();
        return $dao->getAll();
    }
    
    public function getOrganizationByID($organization_id)
    {
        $dao = new OrganizationDAO();
        return $dao->getByID($organization_id);
    }
    
    public function getAllOrgLocNames()
    {
        $dao = new OrganizationDAO();
        return $dao->getAllOrgLocNames();
    }
    
    public function updateOrganization($organizationModel)
    {
        $dao = new OrganizationDAO();
        return $dao->update($organizationModel);
    }
    
    public function deleteOrganizationByID($organization_id)
    {
        // CASCADE DELETES TO JOBS VIA FOREIGN KEY
        // CASCADE DELETES TO LOCATIONS VIA FOREIGN KEY
        $dao = new OrganizationDAO();
        return $dao->deleteByID($organization_id);
    }
    
    public function insertUpdateOrgLoc($org, $locArray)
    {

//         echo "<pre>\n";
//         print_r($org);
//         print_r($locArray);
//         echo "</pre>\n";
//         exit;
        
        
        
        $OrgDao = new OrganizationDAO();
        $LocDao = new LocationDAO();
        
        if ($org->getId() > 0)
        {
            $org_id = $org->getId();
            $OrgDao->update($org);
        }
        else
        {
            $org_id = $OrgDao->create($org);
        }

        foreach($locArray as $a)
        {
            $a->setOrganization_id($org_id);
            if ($a->getId() == 0)
            {
                $LocDao->create($a);
            }
            else 
            {
                $LocDao->update($a);
            }
        }
    }
}

