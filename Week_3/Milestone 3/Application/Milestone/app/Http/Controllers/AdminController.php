<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Controller
 * 2. Handles Listing Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

namespace App\Http\Controllers;

use App\Services\Business\UserService;
use Illuminate\Http\Request;
use App\Services\Business\RoleService;
use App\Services\Business\JobListingService;
use App\Models\JobModel;
use App\Models\OrganizationModel;
use App\Models\LocationModel;

class AdminController extends Controller
{
    // ------------------------------------------------------------
    // User Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminUserListing()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        $service = new UserService();
        $usersList = $service->getAllUsers();

        return view('adminUserListing')->with(['usersList' => $usersList]);
    }

    
    // Admin possibly altering user
    public function showMemberProfile(Request $request)
    {        
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        $user_id = $request->input('id');
        $mode    = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen user
            $service = new UserService();
            $userAggregateModel = $service->getUserAggregate($user_id);
            
            $roleService = new RoleService();
            $rolesList = $roleService->getAllRoles();
            $data = ['userAggregateModel' => $userAggregateModel, 'rolesList' => $rolesList];
            return view('memberProfile')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete user
            $service = new UserService();
            $service->deleteUser($user_id);
            return redirect('adminUser');
        }
        else 
        {
            // Something wrong mode (0,1) only - redirect to welcome
            return view('welcome');
        }
    }
    
    // ------------------------------------------------------------
    // Job Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminJobListing()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }
        
        $service = new JobListingService();
        $jobsList = $service->getAllJobs();
        
//         echo "<pre>\n";
//         print_r($jobsList);
//         echo "</pre>\n";
//         exit;
        
        return view('adminJobListing')->with(['jobsList' => $jobsList]);
    }

    // Admin possibly altering user
    public function showAdminJob(Request $request)
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }
        
        $job_id = $request->input('id');
        $mode   = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen job
            $service = new JobListingService();
            $orgsList = $service->getAllOrganizations();
            $locList = $service->getAllOrgLocNames();
            $job = $service->getJobByID($job_id);
            
            $data = ['orgsList' => $orgsList, 'locList' => $locList, 'job' => $job];
            return view('adminJobView')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete job
            $service = new JobListingService();
            $service->deleteJobByID($job_id);
            
            // Return to Job Listing
            return redirect('adminJob');
        }
        else
        {
            // Something wrong mode (0,1) only - redirect to welcome
            return view('welcome');
        }
    }
    
    public function showAdminCreateJob()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }
        
        $service = new JobListingService();
        $orgsList = $service->getAllOrganizations();
        $locList = $service->getAllOrgLocNames();
        
        $job = new JobModel(0, 0, 0, "", "", 0, "", "", "", "", "");
        
        $data = ['orgsList' => $orgsList, 'locList' => $locList, 'job' => $job];
        return view('adminJobView')->with($data);
    }
    
    public function doAdminJob(Request $request)
    {
        $job_id       = $request->input('JobID');
        $positionName = $request->input('PositionName');
        $description  = $request->input('Description');
        $expireDate   = $request->input('ExpireDate');
        $education    = $request->input('Education');
        $skills       = $request->input('Skills');
        $orgID        = $request->input('OrgID');
        $locID        = $request->input('LocID');
        $posType      = $request->input('PosType');
        
        $jobModel = new JobModel($job_id, $orgID, $locID, $positionName, $description, $posType, $expireDate, $skills, $education, "", "");
        $service = new JobListingService();
        $service->createJob($jobModel);
        
        // Return to Job Listing
        return redirect('adminJob');
    }
    
    // ------------------------------------------------------------
    // Organization Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminOrganizationListing()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }
        
        $service = new JobListingService();
        $orgsList = $service->getAllOrganizations();
                
        return view('adminOrgListing')->with(['orgsList' => $orgsList]);
    }

    // Admin Organization Listing Form - Alter org (Edit/Delete)
    public function showAdminOrganization(Request $request)
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        $org_id = $request->input('id');
        $mode   = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen Organization
            $service = new JobListingService();
            $locList = $service->getLocationsByOrgID($org_id);
            $org = $service->getOrganizationByID($org_id);
            
            $data = ['locList' => $locList, 'org' => $org];
            return view('adminOrgView')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete organization - cascades on locations/jobs
            $service = new JobListingService();
            $service->deleteOrganizationByID($org_id);
            
            // Return to Organization Listing
            return redirect('adminOrganization');
        }
        else
        {
            // Something wrong mode (0,1) only - redirect to welcome
            return view('welcome');
        }
    }
    
    // Admin Organization - Create New Organization
    public function showAdminCreateOrganization()
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        // Create Empty Model to allow admin to fill in the blanks
        $loc = new LocationModel(0, 0, "", "", "", "", "", "USA");
        $org = new OrganizationModel(0, "", "", "", "", "", "", "");
        $locList = [$loc];
        
        $data = ['locList' => $locList, 'org' => $org];
        return view('adminOrgView')->with($data);
        
    }
    
    // Admin Organization View Form Action
    public function doAdminOrganization(Request $request)
    {
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (! isset($_SESSION['principal']))
        {
            return view('login');
        }

        // Retrieve Organization Information
        $org_id            = $request->input('OrgID');
        $name              = $request->input('Name');
        $website           = $request->input('Website');
        $phone             = $request->input('Phone');
        $email             = $request->input('Email');
        $description       = $request->input('Description');
        $mission_statement = $request->input('Mission');
        $ethics_statement  = $request->input('Ethics');
        $orgModel = new OrganizationModel($org_id, $name, $description, $mission_statement, $ethics_statement, $phone, $website, $email);
        
        // Retrieve Organization Information
        $emptyArray = array();
        $loc_id      = $request->input('LocID',   $emptyArray);
        $loc_name    = $request->input('LocName', $emptyArray);
        $website     = $request->input('Website', $emptyArray);
        $street      = $request->input('Street',  $emptyArray);
        $city        = $request->input('City',    $emptyArray);
        $state       = $request->input('State',   $emptyArray);
        $postal_code = $request->input('ZipCode', $emptyArray);
        
        $locations = array();
        $index = 0;
        for ($x = 0; $x < count($loc_name); ++$x)
        {
            if (trim($loc_name[$x]) !== "")
            {
                $locationModel = new LocationModel($loc_id[$x], $org_id, $loc_name[$x], $street[$x], $city[$x], $state[$x], $postal_code[$x], "USA");
                $locations[$index] = $locationModel;
                ++$index;
            }
        }
        
        $service = new JobListingService();
        $service->insertUpdateOrgLoc($orgModel, $locations);
        
        // Return to Organization Listing
        return redirect('adminOrganization');
    }
}
