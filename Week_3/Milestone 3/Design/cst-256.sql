-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 19, 2021 at 07:19 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.16

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst-256`
--
CREATE DATABASE IF NOT EXISTS `cst-256` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cst-256`;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORGANIZATION_ID` int(11) NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `POSITION_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(500) NOT NULL,
  `POSITION_TYPE` int(11) NOT NULL,
  `EXPIRE_DATE` date NOT NULL,
  `SKILLS_KEYWORDS` varchar(500) NOT NULL,
  `EDUCATION_KEYWORDS` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORG_ID_IDX` (`ORGANIZATION_ID`),
  KEY `LOC_ID_IDX` (`LOCATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `jobs`
--

TRUNCATE TABLE `jobs`;
--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`ID`, `ORGANIZATION_ID`, `LOCATION_ID`, `POSITION_NAME`, `DESCRIPTION`, `POSITION_TYPE`, `EXPIRE_DATE`, `SKILLS_KEYWORDS`, `EDUCATION_KEYWORDS`) VALUES
(1, 2, 3, 'Instructor PHP Programming', 'Teach and facility PHP Learning Classroom for undergrad students - online', 1, '2022-03-01', 'PHP, MYSQL, LARAVEL', 'MA, PHD');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORGANIZATION_ID` int(11) NOT NULL,
  `LOCATION_NAME` varchar(100) NOT NULL,
  `STREET` varchar(100) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `STATE` varchar(100) NOT NULL,
  `POSTAL_CODE` varchar(100) NOT NULL,
  `COUNTRY` varchar(100) NOT NULL DEFAULT 'USA',
  PRIMARY KEY (`ID`),
  KEY `ORG_ID_IDX` (`ORGANIZATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `locations`
--

TRUNCATE TABLE `locations`;
--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`ID`, `ORGANIZATION_ID`, `LOCATION_NAME`, `STREET`, `CITY`, `STATE`, `POSTAL_CODE`, `COUNTRY`) VALUES
(3, 2, 'HQ', '3300 W. Camelback Rd.', 'Phoenix', 'AZ', '85017', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE IF NOT EXISTS `organizations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(1000) NOT NULL,
  `MISSION_STATEMENT` varchar(1000) NOT NULL,
  `ETHICS_STATEMENT` varchar(1000) NOT NULL,
  `PHONE` varchar(100) NOT NULL,
  `WEBSITE` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `organizations`
--

TRUNCATE TABLE `organizations`;
--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`ID`, `NAME`, `DESCRIPTION`, `MISSION_STATEMENT`, `ETHICS_STATEMENT`, `PHONE`, `WEBSITE`, `EMAIL`) VALUES
(2, 'Grand Canyon University', 'Welcome to Grand Canyon University (GCU), Arizona\'s premier private Christian university. For more than 65 years, our Phoenix campus has been the gateway to success for countless scholars and industry leaders. Our commitment to our Christian worldview is carried out in a loving way that provides greater unity across the university, and our foundational documents help articulate those beliefs and who we are as a community. With acres of scenic grounds and outstanding facilities, we invite the next generation of students and working professionals to enjoy the complete university experience.', 'Grand Canyon University is a premier Christian University, educating people to lead and serve.\r\nGrand Canyon University prepares learners to become global citizens, critical thinkers, effective communicators and responsible leaders by providing an academically challenging, values-based curriculum from the context of our Christian heritage.', 'GCU’s Ethical Positions Statement outlines our beliefs on moral truth, creation, human life, salvation and more. Our goal is to provide clarity, unity and alignment across the university on matters of ethics and morality. Also view our Statement on Civility, Compassion and the Way of Jesus.', '(855) 428-5673', 'https://www.gcu.edu/', 'info@gcu.edu');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLENAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`ID`, `ROLENAME`, `DESCRIPTION`) VALUES
(1, 'Normal User', 'A Customer'),
(2, 'User Maintenance', 'User Maintenance'),
(3, 'Group Maintenance', 'Group Maintenance'),
(4, 'Admin', 'Administrator Users and Groups');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `MOBILE` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `BIRTHDATE` date NOT NULL,
  `GENDER` tinyint(1) NOT NULL,
  `ROLE_ID` int(11) NOT NULL DEFAULT '1',
  `SUSPENDED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  KEY `ROLE_ID_idx` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `MOBILE`, `PASSWORD`, `BIRTHDATE`, `GENDER`, `ROLE_ID`, `SUSPENDED`) VALUES
(1, 'User', 'Maint', 'umaint@getjobs.com', '(111) 111-1111', '11111111', '2000-01-01', 0, 2, 0),
(2, 'Group', 'Maint', 'gmaint@getjobs.com', '(222) 222-2222', '22222222', '2000-01-01', 0, 3, 0),
(3, 'Ad', 'min', 'admin@getjobs.com', '(333) 333-3333', '33333333', '2000-01-01', 0, 4, 0),
(4, 'Kelly', 'Lamb', 'kl@kl.com', '(562) 505-0983', '12345678', '1965-05-01', 0, 1, 0),
(5, 'Test1', 'Test2', 'test@test.com', '(123) 123-1234', '12345678', '2021-12-14', 0, 1, 0),
(6, 'Deena', 'Lamb', 'dl@dl.com', '(123) 456-7890', '11111111', '1970-07-20', 1, 1, 0),
(8, 'Mickey', 'Mouse', 'mm@mm.com', '(111) 111-1111', '11111111', '2021-12-17', 0, 4, 0),
(9, 'Minnie', 'Mouse', 'mm1@mm.com', '(123) 123-1234', '11111111', '2021-12-27', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_education`
--

DROP TABLE IF EXISTS `users_education`;
CREATE TABLE IF NOT EXISTS `users_education` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ORG_NAME` varchar(100) NOT NULL,
  `DEGREE_DESCRIPTION` varchar(100) NOT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `GRADUATED` tinyint(1) NOT NULL,
  `LINK_TO_ORG` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_education`
--

TRUNCATE TABLE `users_education`;
--
-- Dumping data for table `users_education`
--

INSERT INTO `users_education` (`ID`, `USER_ID`, `ORG_NAME`, `DEGREE_DESCRIPTION`, `START_DATE`, `END_DATE`, `GRADUATED`, `LINK_TO_ORG`) VALUES
(99, 8, 'DisneyU', 'MBA', '2021-12-01', '2021-12-02', 1, ''),
(104, 5, 'ad', 'ad', '2021-12-17', '2021-12-17', 0, ''),
(138, 4, 'Norwalk High School', 'Diploma Honors', '1979-06-01', '1983-06-02', 1, ''),
(139, 4, 'Cerritos Community College', 'Certificate: Programmer', '1983-06-16', '2013-06-23', 1, ''),
(140, 4, 'Cerritos Community College', 'AA: Programmer', '2013-06-23', '2020-05-23', 1, ''),
(141, 4, 'Grand Canyon University', 'BA: Programmer', '2019-06-01', '2021-12-16', 0, ''),
(142, 3, 'GCU', 'BA', '2021-12-01', '2021-12-08', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
CREATE TABLE IF NOT EXISTS `users_profile` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `PHOTO` varchar(100) NOT NULL DEFAULT 'default_member_photo.jpg',
  `DESCRIPTION` varchar(500) NOT NULL,
  `STREET` varchar(100) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `STATE` varchar(100) NOT NULL,
  `POSTAL_CODE` varchar(100) NOT NULL,
  `COUNTRY` varchar(100) NOT NULL DEFAULT 'USA',
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_profile`
--

TRUNCATE TABLE `users_profile`;
--
-- Dumping data for table `users_profile`
--

INSERT INTO `users_profile` (`ID`, `USER_ID`, `PHOTO`, `DESCRIPTION`, `STREET`, `CITY`, `STATE`, `POSTAL_CODE`, `COUNTRY`) VALUES
(1, 1, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(2, 2, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(3, 3, '3.jpg', 'Administrator', '12345 Here St', 'Somewhere', 'AZ', '85123', 'USA'),
(4, 4, '4.jpg', 'I love to program computer software applications, play guitar, and enjoy fun time with my family.', '16217 Grand Ave.', 'Bellflower', 'CA', '90706', 'USA'),
(5, 5, 'default_member_photo.jpg', 'dfgdgdgdfdgg', '123', 'dssdfs', 'AL', '55555', 'USA'),
(6, 6, 'default_member_photo.jpg', '', '', '', '', '', 'USA'),
(8, 8, 'default_member_photo.jpg', 'An animated cartoon character.', '1 Disney Way', 'Anaheim', 'CA', '99292', 'USA'),
(9, 9, 'default_member_photo.jpg', 'A cartoon character from Disney animation studios. I play the girl friends and wife to Mickey Mouse.', '2123', 'fsfsdfsd', 'AL', '43211', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `users_skillset`
--

DROP TABLE IF EXISTS `users_skillset`;
CREATE TABLE IF NOT EXISTS `users_skillset` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SKILL_NAME` varchar(100) NOT NULL,
  `DESCRIPTION` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_skillset`
--

TRUNCATE TABLE `users_skillset`;
--
-- Dumping data for table `users_skillset`
--

INSERT INTO `users_skillset` (`ID`, `USER_ID`, `SKILL_NAME`, `DESCRIPTION`) VALUES
(107, 8, 'Dancing', 'Scurries around the dance floor'),
(112, 5, 'ada', 'as'),
(146, 4, 'JAVA', 'J2EE, J2SE, Android Programming'),
(147, 4, 'C, C++, C#', 'All Flavors of C Programming'),
(148, 4, 'LAMP, WAMP', 'PHP Programming'),
(149, 4, 'C++', 'C++ Programming'),
(150, 3, 'Test', 'Testing');

-- --------------------------------------------------------

--
-- Table structure for table `users_work_history`
--

DROP TABLE IF EXISTS `users_work_history`;
CREATE TABLE IF NOT EXISTS `users_work_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ORG_NAME` varchar(100) NOT NULL,
  `WORK_DESCRIPTION` varchar(500) NOT NULL,
  `START_DATE` date NOT NULL,
  `END_DATE` date NOT NULL,
  `CURRENTLY_EMPLOYED` tinyint(1) NOT NULL,
  `LINK_TO_ORG` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_ID_IDX` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `users_work_history`
--

TRUNCATE TABLE `users_work_history`;
--
-- Dumping data for table `users_work_history`
--

INSERT INTO `users_work_history` (`ID`, `USER_ID`, `ORG_NAME`, `WORK_DESCRIPTION`, `START_DATE`, `END_DATE`, `CURRENTLY_EMPLOYED`, `LINK_TO_ORG`) VALUES
(49, 8, 'Disney', 'Main Cartoon Character', '2021-12-01', '2021-12-01', 1, ''),
(54, 5, 'asd', 'ad', '2021-12-17', '2021-12-19', 1, ''),
(72, 4, 'Optum Software', 'Senior Developer: Enhance Standard Product, Apply Changes/Corrections to custom accounts', '1996-05-20', '2006-04-10', 0, ''),
(73, 4, 'Capital Group Co.', 'Programmer Analyst: Enhance Custom Interface code for In-House Clients', '2006-04-10', '2009-08-10', 0, ''),
(74, 3, 'Test', 'Test: Tester', '2021-12-15', '2021-12-28', 1, '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `LOC_ID_FK` FOREIGN KEY (`LOCATION_ID`) REFERENCES `locations` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ORG_ID_FK` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organizations` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `ORG_ID_FK1` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organizations` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `ROLE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `roles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_education`
--
ALTER TABLE `users_education`
  ADD CONSTRAINT `USERS_ID_FK3` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_profile`
--
ALTER TABLE `users_profile`
  ADD CONSTRAINT `USERS_ID_FK1` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_skillset`
--
ALTER TABLE `users_skillset`
  ADD CONSTRAINT `USERS_ID_FK` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_work_history`
--
ALTER TABLE `users_work_history`
  ADD CONSTRAINT `USERS_ID_FK2` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
