<?php
namespace App\Http\Controllers;

use App\Models\GroupModel;
use App\Models\GroupPostModel;
use App\Models\JobModel;
use App\Models\LocationModel;
use App\Models\OrganizationModel;
use App\Services\Business\GroupService;
use App\Services\Business\JobListingService;
use App\Services\Business\RoleService;
use App\Services\Business\UserService;
use App\Services\Utility\ILoggerService;
use Illuminate\Http\Request;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Controller
 * 2. Handles Listing Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */



class AdminController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    // ------------------------------------------------------------
    // User Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminUserListing()
    {
        $this->logger->info("Entering AdminController::showAdminUserListing()");
        if ($this->assertLogin(true)) return view('login');
        
        $service = new UserService();
        $usersList = $service->getAllUsers();

        $this->logger->info("Exiting AdminController::showAdminUserListing()");
        return view('adminUserListing')->with(['usersList' => $usersList]);
    }

    
    // Admin possibly altering user
    public function showMemberProfile(Request $request)
    {        
        $this->logger->info("Entering AdminController::showMemberProfile()");
        if ($this->assertLogin(true)) return view('login');
        
        $user_id = $request->input('id');
        $mode    = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen user
            $service = new UserService();
            $userAggregateModel = $service->getUserAggregate($user_id);
            
            $roleService = new RoleService();
            $rolesList = $roleService->getAllRoles();
            $data = ['userAggregateModel' => $userAggregateModel, 'rolesList' => $rolesList];
            $this->logger->info("Exiting AdminController::showMemberProfile() - memberProfile");
            return view('memberProfile')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete user
            $service = new UserService();
            $service->deleteUser($user_id);
            $this->logger->info("Exiting AdminController::showMemberProfile() - adminUser");
            return redirect('adminUser');
        }
        else 
        {
            // Something wrong mode (0,1) only - redirect to welcome
            $this->logger->info("Exiting AdminController::showMemberProfile() - welcome");
            return view('welcome');
        }
    }
    
    // ------------------------------------------------------------
    // Job Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminJobListing()
    {
        $this->logger->info("Entering AdminController::showAdminJobListing()");
        if ($this->assertLogin(true)) return view('login');
        
        $service = new JobListingService();
        $jobsList = $service->getAllJobs();
        
        $this->logger->info("Exiting AdminController::showAdminJobListing()");
        return view('adminJobListing')->with(['jobsList' => $jobsList]);
    }

    // Admin possibly altering user
    public function showAdminJob(Request $request)
    {
        $this->logger->info("Entering AdminController::showAdminJob()");
        if ($this->assertLogin(true)) return view('login');
        
        $job_id = $request->input('id');
        $mode   = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen job
            $service = new JobListingService();
            $orgsList = $service->getAllOrganizations();
            $locList = $service->getAllOrgLocNames();
            $job = $service->getJobByID($job_id);
            
            $data = ['orgsList' => $orgsList, 'locList' => $locList, 'job' => $job];
            $this->logger->info("Exiting AdminController::showAdminJob() - adminJobView");
            return view('adminJobView')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete job
            $service = new JobListingService();
            $service->deleteJobByID($job_id);
            
            // Return to Job Listing
            $this->logger->info("Entering AdminController::showAdminJob() - adminJob");
            return redirect('adminJob');
        }
        else
        {
            // Something wrong mode (0,1) only - redirect to welcome
            $this->logger->info("Entering AdminController::showAdminJob() - welcome");
            return view('welcome');
        }
    }
    
    public function showAdminCreateJob()
    {
        $this->logger->info("Entering AdminController::showAdminCreateJob()");
        if ($this->assertLogin(true)) return view('login');
        
        $service = new JobListingService();
        $orgsList = $service->getAllOrganizations();
        $locList = $service->getAllOrgLocNames();
        
        $job = new JobModel(0, 0, 0, "", "", 0, "", "", "", "", "");
        
        $data = ['orgsList' => $orgsList, 'locList' => $locList, 'job' => $job];
        $this->logger->info("Exiting AdminController::showAdminCreateJob()");
        return view('adminJobView')->with($data);
    }
    
    public function doAdminJob(Request $request)
    {
        $this->logger->info("Entering AdminController::doAdminJob()");
        if ($this->assertLogin(true)) return view('login');

        $job_id       = $request->input('JobID');
        $positionName = $request->input('PositionName');
        $description  = $request->input('Description');
        $expireDate   = $request->input('ExpireDate');
        $education    = $request->input('Education');
        $skills       = $request->input('Skills');
        $orgID        = $request->input('OrgID');
        $locID        = $request->input('LocID');
        $posType      = $request->input('PosType');
        
        $jobModel = new JobModel($job_id, $orgID, $locID, $positionName, $description, $posType, $expireDate, $skills, $education, "", "");
        $service = new JobListingService();
        $service->createJob($jobModel);
        
        // Return to Job Listing
        $this->logger->info("Exiting AdminController::doAdminJob()");
        return redirect('adminJob');
    }
    
    // ------------------------------------------------------------
    // Organization Section
    // ------------------------------------------------------------
    
    // Display the admin user listing
    public function showAdminOrganizationListing()
    {
        $this->logger->info("Entering AdminController::showAdminOrganizationListing()");
        if ($this->assertLogin(true)) return view('login');

        $service = new JobListingService();
        $orgsList = $service->getAllOrganizations();
                
        $this->logger->info("Exiting AdminController::showAdminOrganizationListing()");
        return view('adminOrgListing')->with(['orgsList' => $orgsList]);
    }

    // Admin Organization Listing Form - Alter org (Edit/Delete)
    public function showAdminOrganization(Request $request)
    {
        $this->logger->info("Entering AdminController::showAdminOrganization()");
        if ($this->assertLogin(true)) return view('login');
        
        $org_id = $request->input('id');
        $mode   = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit Chosen Organization
            $service = new JobListingService();
            $locList = $service->getLocationsByOrgID($org_id);
            $org = $service->getOrganizationByID($org_id);
            
            $data = ['locList' => $locList, 'org' => $org];
            $this->logger->info("Exiting AdminController::showAdminOrganization() - adminOrgView");
            return view('adminOrgView')->with($data);
        }
        else if ($mode == 1)
        {
            // Permanently delete organization - cascades on locations/jobs
            $service = new JobListingService();
            $service->deleteOrganizationByID($org_id);
            
            // Return to Organization Listing
            $this->logger->info("Exiting AdminController::showAdminOrganization() - adminOrganization");
            return redirect('adminOrganization');
        }
        else
        {
            // Something wrong mode (0,1) only - redirect to welcome
            $this->logger->info("Exiting AdminController::showAdminOrganization() - welcome");
            return view('welcome');
        }
    }
    
    // Admin Organization - Create New Organization
    public function showAdminCreateOrganization()
    {
        $this->logger->info("Entering AdminController::showAdminCreateOrganization()");
        if ($this->assertLogin(true)) return view('login');
        
        // Create Empty Model to allow admin to fill in the blanks
        $loc = new LocationModel(0, 0, "", "", "", "", "", "USA");
        $org = new OrganizationModel(0, "", "", "", "", "", "", "");
        $locList = [$loc];
        
        $data = ['locList' => $locList, 'org' => $org];
        $this->logger->info("Exiting AdminController::showAdminCreateOrganization()");
        return view('adminOrgView')->with($data);
        
    }
    
    // Admin Organization View Form Action
    public function doAdminOrganization(Request $request)
    {
        $this->logger->info("Entering AdminController::doAdminOrganization()");
        if ($this->assertLogin(true)) return view('login');
        
        // Retrieve Organization Information
        $org_id            = $request->input('OrgID');
        $name              = $request->input('Name');
        $website           = $request->input('Website');
        $phone             = $request->input('Phone');
        $email             = $request->input('Email');
        $description       = $request->input('Description');
        $mission_statement = $request->input('Mission');
        $ethics_statement  = $request->input('Ethics');
        $orgModel = new OrganizationModel($org_id, $name, $description, $mission_statement, $ethics_statement, $phone, $website, $email);
        
        // Retrieve Organization Information
        $emptyArray = array();
        $loc_id      = $request->input('LocID',   $emptyArray);
        $loc_name    = $request->input('LocName', $emptyArray);
        $website     = $request->input('Website', $emptyArray);
        $street      = $request->input('Street',  $emptyArray);
        $city        = $request->input('City',    $emptyArray);
        $state       = $request->input('State',   $emptyArray);
        $postal_code = $request->input('ZipCode', $emptyArray);
        
        $locations = array();
        $index = 0;
        for ($x = 0; $x < count($loc_name); ++$x)
        {
            if (trim($loc_name[$x]) !== "")
            {
                $locationModel = new LocationModel($loc_id[$x], $org_id, $loc_name[$x], $street[$x], $city[$x], $state[$x], $postal_code[$x], "USA");
                $locations[$index] = $locationModel;
                ++$index;
            }
        }
        
        $service = new JobListingService();
        $service->insertUpdateOrgLoc($orgModel, $locations);
        
        // Return to Organization Listing
        $this->logger->info("Exiting AdminController::doAdminOrganization()");
        return redirect('adminOrganization');
    }
    
    
    // ------------------------------------------------------------
    // Group Section
    // ------------------------------------------------------------

    // Display the admin group listing
    public function showAdminGroupListing()
    {
        $this->logger->info("Entering AdminController::showAdminGroupListing()");
        if ($this->assertLogin(true)) return view('login');
        $user = $_SESSION['principal'];
        
        $service = new GroupService();
        $groupsList = $service->getAllGroupsSetMemberTag($user->getId());
        
        $this->logger->info("Exiting AdminController::showAdminGroupListing()");
        return view('adminGroupListing')->with(['groupsList' => $groupsList]);
    }
    
    // Handle the action requested by administrator: group - delete or show edit (view) form
    public function doAdminGroupAction(Request $request)
    {
        $this->logger->info("Entering AdminController::doAdminGroupAction()");
        if ($this->assertLogin(true)) return view('login');
        
        $group_id = $request->input('id');
        $mode     = $request->input('mode');
        
        if ($mode == 0)
        {
            // Edit this group
            $service = new GroupService();
            $postsList = $service->getAllPostsForGroup($group_id);
            $group = $service->getGroupByID($group_id);
            
            $data = ['group' => $group, 'postsList' => $postsList];
            $this->logger->info("Exiting AdminController::doAdminGroupAction() - adminGroupView");
            return view('adminGroupView')->with($data);
        }
        else if ($mode == 1)
        {
            // Delete this group
            $service = new GroupService();
            $service->deleteGroupByID($group_id);
            $this->logger->info("Exiting AdminController::doAdminGroupAction() - adminGroup 1");
            return redirect('adminGroup');
        }
        
        $this->logger->info("Exiting AdminController::doAdminGroupAction() - adminGroup");
        return redirect('adminGroup');
    }
    
    // Show group create (view) form
    public function showAdminCreateGroup(Request $request)
    {
        $this->logger->info("Entering AdminController::showAdminCreateGroup()");
        if ($this->assertLogin(true)) return view('login');
        $user = $_SESSION['principal'];
        
        $date = new \DateTime();
        $created_date = $date->format("Y-m-d");
        $group = new GroupModel(0, $user->getId(), "", "", $created_date, 0);
        
        $postsList = array();

        $data = ['group' => $group, 'postsList' => $postsList];        
        $this->logger->info("Exiting AdminController::showAdminCreateGroup()");
        return view('adminGroupView')->with($data);
    }

    public function doAdminGroup(Request $request)
    {
        $this->logger->info("Entering AdminController::doAdminGroup()");
        if ($this->assertLogin(true)) return view('login');
        $user = $_SESSION['principal'];
        $date = new \DateTime();
        $created_time1 = $date->format("Y-m-d H:i:s");
        
        // Retrieve form input fields for the group Model
        $group_id          = $request->input('GroupID');
        $group_name        = $request->input('GroupName');
        $group_description = $request->input('Description');
        $created_date      = $request->input('CreatedDate');
        $groupModel = new GroupModel($group_id, $user->getId(), $group_name, $group_description, $created_date, 0);
        
        // Retrieve Announcement (Posts) Information
        $emptyArray = array();
        $post_id      = $request->input('PostID',      $emptyArray);
        $post         = $request->input('Post',        $emptyArray);
        $created_time = $request->input('CreatedTime', $emptyArray);
        
        $groupPostModelArray = array();
        $index = 0;
        for ($x = 0; $x < count($post); ++$x)
        {
            if (trim($post[$x]) !== "")
            {
                if ($post_id[$x] == 0)
                {
                    $created_time[$x] = $created_time1;
                }
                $groupPostModel = new GroupPostModel($post_id[$x], $group_id, $user->getId(), $post[$x], $created_time[$x]);
                $groupPostModelArray[$index] = $groupPostModel;
                ++$index;
            }
        }

        $service = new GroupService();
        $service->createUpdateGroupAndPosts($groupModel, $groupPostModelArray);
        
        $this->logger->info("Exiting AdminController::doAdminGroup()");
        return redirect('adminGroup');
    }
}
