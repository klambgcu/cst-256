<?php
namespace App\Http\Controllers\RestAPI;

use App\Models\DTO;
use App\Services\Business\JobListingService;
use App\Services\Utility\ILoggerService;
use Exception;
use Illuminate\Http\Response;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2023-01-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Job API Controller
 * 2. Handles Rest API Functionality
 * 3. JSON All Job Listings
 * 4. JSON Job By Id
 * ---------------------------------------------------------------
 */

class JobAPIController
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->logger->info("Entering JobAPIController::index()");
        $service = new JobListingService();
        $data = [];
        
        try
        {
            $data = $service->getAllJobs();
            $status = 200;
            $results = new DTO($status, "Job Listing Information. Count = " . count($data), $data);
        } catch (Exception $e)
        {
            $status = 500;
            $results = new DTO($status, "Internal Server Error. Please try again later or contact administrator.", $data);
            $this->logger->error("Internal Server Error: JobAPIController::index() " . $e->getMessage());
        }
        
        $content = json_encode($results);
        
        $this->logger->info("Exiting JobAPIController::index()");
        return (new Response($content, 200))->header('Content-Type', 'application/json; charset=utf-8');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->logger->info("Entering JobAPIController::show()");
        $service = new JobListingService();
        $data = [];
        
        if (! is_numeric($id))
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be numeric. id = " . $id, $data);
        }
        else if ($id <> floor($id))
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be an integer. id = " . $id, $data);
        }
        else if ($id <= 0)
        {
            $status = 400;
            $results = new DTO($status, "Bad Request: Invalid Parameter - id must be a positive integer. id = " . $id, $data);
        }
        else
        {
            try
            {
                if (! $service->JobIDExists($id))
                {
                    $status = 404;
                    $results = new DTO($status, "Not Found: Job not found: id = " . $id, $data);
                }
                else 
                {
                    $data = $service->getJobAggregate($id);
                    $status = 200;
                    $results = new DTO($status, "OK: Job information for id = " . $id, $data);
                    
                }
            } catch (Exception $e)
            {
                $status = 500;
                $results = new DTO($status, "Internal Server Error: Please try again later or contact administrator. id = " . $id, $data);
                $this->logger->error("Internal Server Error: JobAPIController::show() " . $e->getMessage());
            }
        }
        
        $content = json_encode($results);
        
        $this->logger->info("Exiting JobAPIController::show()");
        return (new Response($content, $status))->header('Content-Type', 'application/json; charset=utf-8');
    }
}

