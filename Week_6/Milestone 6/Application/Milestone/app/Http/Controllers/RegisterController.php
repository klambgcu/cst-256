<?php
namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\UserService;
use App\Services\Utility\ILoggerService;
use Illuminate\Support\Facades\DB;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Registration Controller
 * 2. Handles Registration Functionality
 * 3. Stores in database
 * ---------------------------------------------------------------
 */

class RegisterController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    //Registration Method
    public function register()
    {
        $this->logger->info("Entering RegisterController::register()");
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        // store registration parameters
        $firstname = filter_input(INPUT_POST,'FirstName');
        $lastname  = filter_input(INPUT_POST,'LastName');
        $email     = filter_input(INPUT_POST,'Email');
        $password  = filter_input(INPUT_POST,'Password');
        $mobile    = filter_input(INPUT_POST,'Mobile');
        $birthdate = filter_input(INPUT_POST,'Birthdate');
        $gender    = filter_input(INPUT_POST,'Gender');
        $role_id   = 1; // Default to Basic - Administrator can decide to change
        $suspended = 0; // No suspended by default
        
        // Convert birthdate to string for insert "YYYY-MM-DD" format
        $bdate = new \DateTime($birthdate);
        $bdate_str = $bdate->format("Y-m-d");

        $email_ok = DB::select('select 1 as amount from users where email = ?', [$email]);
            
        if (count($email_ok) == 0)
        {
            // Create an user entry
            $userModel = new UserModel(0, $firstname, $lastname, $email, $mobile, $password, $bdate_str, $gender, $role_id, $suspended);
            $service = new UserService();
            $service->createUser($userModel);
            $result = true;
        }
        else
        {
            $result = false;
        }
        
        // Test for success
        if ($result)
        {
            // Registration Successful
            $_SESSION["message"] = "Registration Successful " . $firstname . " " . $lastname;
        }
        else 
        {
            // Registration Failed
            $_SESSION["message"] = "Registration unsuccessful - Email Already Exists. Please try another.";
            
        }
        
        $this->logger->info("Exiting RegisterController::register()");
        return view('registerstatus');
    }
    
}
