<?php
namespace App\Services\Utility;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-22
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Logging Mechanism Abstract
 * 2. 
 * 3.
 * ---------------------------------------------------------------
 */

interface ILoggerService
{
    public function debug($message, array $context = array());
    
    public function info($message, array $context = array());
    
    public function warning($message, array $context = array());
    
    public function error($message, array $context = array()); 
}

