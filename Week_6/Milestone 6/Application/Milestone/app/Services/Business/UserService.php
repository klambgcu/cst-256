<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-14
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service - User
 * 2. Various CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Business;

use App\Services\Data\UserDAO;

class UserService
{
    public function __construct()
    {}
    
    public function createUser($userModel)
    {
        $dao = new UserDAO();
        return $dao->createUser($userModel);
    }

    public function UserIDExists($user_id)
    {
        $dao = new UserDAO();
        return $dao->UserIDExists($user_id);
    }
    
    public function getUserByEmail($email)
    {
        $dao = new UserDAO();
        return $dao->getUserByEmail($email);
    }

    public function getUserByID($user_id)
    {
        $dao = new UserDAO();
        return $dao->getUser($user_id);
    }

    public function getUserAggregate($user_id)
    {
        $dao = new UserDAO();
        return $dao->getUserAggregate($user_id);
    }

    public function getAllUsers()
    {
        $dao = new UserDAO();
        return $dao->getAllUsers();
    }
    
    public function suspendUser($user_id)
    {
        $dao = new UserDAO();
        return $dao->suspendUser($user_id);
    }
    
    public function unsuspendUser($user_id)
    {
        $dao = new UserDAO();
        return $dao->unsuspendUser($user_id);
    }

    public function updateUserByAggregate($userAggregateModel)
    {
        $dao = new UserDAO();
        return $dao->updateUserByAggregate($userAggregateModel);
    }
    
    public function deleteUser($user_id)
    {
        $dao = new UserDAO();
        return $dao->deleteUser($user_id);
    }
}

