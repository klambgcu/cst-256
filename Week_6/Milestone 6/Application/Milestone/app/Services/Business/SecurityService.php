<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-14
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Service - Security
 * 2. Authenicate Login
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Business;

use App\Services\Data\SecurityDAO;

class SecurityService
{

    public function __construct()
    {}
    
    public function AuthenicateLogin($email, $password)
    {
        $dao = new SecurityDAO();
        return $dao->AuthenicateLogin($email, $password);
    }
    
}

