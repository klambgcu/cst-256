<?php

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Organizations (Job Postings)
 * 2. Various Organization CRUD
 * 3.
 * ---------------------------------------------------------------
 */

namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\OrganizationModel;

class OrganizationDAO
{
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($organizationModel)
    {
        $organization_id = 0;
        $name = $organizationModel->getName();
        $description = $organizationModel->getDescription();
        $mission_statement = $organizationModel->getMission_statement();
        $ethics_statement = $organizationModel->getEthics_statement();
        $phone = $organizationModel->getPhone();
        $website = $organizationModel->getWebsite();
        $email = $organizationModel->getEmail();
        
        $sql = 'INSERT INTO organizations (NAME, DESCRIPTION, MISSION_STATEMENT, ETHICS_STATEMENT, PHONE, WEBSITE, EMAIL) VALUES (?, ?, ?, ?, ?, ?, ?)';
        $data = [$name, $description, $mission_statement, $ethics_statement, $phone, $website, $email];
        DB::insert($sql, $data);
        $organization_id = DB::getPdo()->lastInsertId();
        
        return $organization_id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $organizations = array();
        $index = 0;
        
        $rows = DB::select('SELECT * FROM organizations ORDER BY ID ASC');
        
        foreach ($rows as $row)
        {
            $organization = new OrganizationModel($row->ID,
                                                  $row->NAME,
                                                  $row->DESCRIPTION,
                                                  $row->MISSION_STATEMENT,
                                                  $row->ETHICS_STATEMENT,
                                                  $row->PHONE,
                                                  $row->WEBSITE,
                                                  $row->EMAIL);
            $organizations[$index] = $organization;
            ++$index;
        }
        
        return $organizations;
    }
    
    public function getByID($organization_id)
    {
        $row = DB::select('SELECT * FROM organizations WHERE ID = ?', [$organization_id]);
        
        $organization = new OrganizationModel($row[0]->ID,
                                              $row[0]->NAME,
                                              $row[0]->DESCRIPTION,
                                              $row[0]->MISSION_STATEMENT,
                                              $row[0]->ETHICS_STATEMENT,
                                              $row[0]->PHONE,
                                              $row[0]->WEBSITE,
                                              $row[0]->EMAIL);
        
        return $organization;
    }
    
    // return array org id/name, loc id/name, combo org-loc name
    public function getAllOrgLocNames()
    {
        $sql = "SELECT o.ID, o.NAME, l.ID AS LOC_ID, l.LOCATION_NAME, CONCAT(o.NAME, ' - ', l.LOCATION_NAME) AS NAME_COMBO " .
               "  FROM organizations o, locations l " .
               " WHERE o.ID = l.ORGANIZATION_ID " .
               " ORDER BY o.ID, l.ID ASC";
        $rows = DB::select($sql);
        
        return $rows;
    }
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function update($organizationModel)
    {
        $id = $organizationModel->getId();
        $name = $organizationModel->getName();
        $description = $organizationModel->getDescription();
        $mission_statement = $organizationModel->getMission_statement();
        $ethics_statement = $organizationModel->getEthics_statement();
        $phone = $organizationModel->getPhone();
        $website = $organizationModel->getWebsite();
        $email = $organizationModel->getEmail();
        
        $sql = 'UPDATE organizations SET NAME = ?, DESCRIPTION = ?, MISSION_STATEMENT = ?, ETHICS_STATEMENT = ?, PHONE = ?, WEBSITE = ?, EMAIL = ? WHERE ID = ?';
        $data = [$name, $description, $mission_statement, $ethics_statement, $phone, $website, $email, $id];
        $count = DB::update($sql, $data);
        
        return $count;
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($organization_id)
    {
        $count = DB::delete('DELETE FROM organizations WHERE ID = ?', [$organization_id]);
        
        return $count;
    }
    
}

