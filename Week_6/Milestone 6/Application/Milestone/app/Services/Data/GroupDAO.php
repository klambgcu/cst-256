<?php
namespace App\Services\Data;

use Illuminate\Support\Facades\DB;
use App\Models\GroupModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. DAO - Groups (Group Information)
 * 2. Various Group CRUD
 * 3.
 * ---------------------------------------------------------------
 */

class GroupDAO
{
    public function __construct()
    {}
    
    // -------------------------------------------------------------------
    // Create Functionality Methods
    // -------------------------------------------------------------------
    
    public function create($groupModel)
    {
        // $groupModel = new GroupModel($id, $creator_id, $group_name, $group_description, $created_date);
        $creator_id = $groupModel->getCreator_id();
        $group_name = $groupModel->getGroup_name();
        $group_description = $groupModel->getGroup_description();
        $created_date = $groupModel->getCreated_date();
        
        $sql = 'INSERT INTO groups (CREATOR_ID, GROUP_NAME, GROUP_DESCRIPTION, CREATED_DATE) VALUES (?, ?, ?, ?)';
        $data = [$creator_id, $group_name, $group_description, $created_date];
        DB::insert($sql, $data);
        $group_id = DB::getPdo()->lastInsertId();
        
        return $group_id;
    }
    
    
    // -------------------------------------------------------------------
    // Retrieve Functionality Methods
    // -------------------------------------------------------------------
    
    public function getAll()
    {
        $groups = array();
        $index = 0;
        
        $rows = DB::select('SELECT g.* FROM groups g ORDER BY g.GROUP_NAME ASC');
        
        foreach ($rows as $row)
        {
            $group = new GroupModel($row->ID,
                                    $row->CREATOR_ID,
                                    $row->GROUP_NAME,
                                    $row->GROUP_DESCRIPTION,
                                    $row->CREATED_DATE,
                                    0);
            $groups[$index] = $group;
            ++$index;
        }
        
        return $groups;
    }
    
    public function getByID($group_id)
    {
        $row = DB::select('SELECT g.* FROM groups g WHERE g.ID = ? ', [$group_id]);
        
        $group = new GroupModel($row[0]->ID,
                                $row[0]->CREATOR_ID,
                                $row[0]->GROUP_NAME,
                                $row[0]->GROUP_DESCRIPTION,
                                $row[0]->CREATED_DATE,
                                0);

        return $group;
    }
    
    
    // -------------------------------------------------------------------
    // Update Functionality Methods
    // -------------------------------------------------------------------
    
    public function update($groupModel)
    {
        $id = $groupModel->getId();
        $creator_id = $groupModel->getCreator_id();
        $group_name = $groupModel->getGroup_name();
        $group_description = $groupModel->getGroup_description();

        $sql = 'UPDATE groups SET CREATOR_ID = ?, GROUP_NAME = ?, GROUP_DESCRIPTION = ? WHERE ID = ?';
        $data = [$creator_id, $group_name, $group_description, $id];
        $count = DB::update($sql, $data);
        
        return $count;
    }
    
    // -------------------------------------------------------------------
    // Delete Functionality Methods
    // -------------------------------------------------------------------
    
    public function deleteByID($group_id)
    {
        $count = DB::delete('DELETE FROM groups WHERE ID = ?', [$group_id]);
        
        return $count;
    }
    
}
