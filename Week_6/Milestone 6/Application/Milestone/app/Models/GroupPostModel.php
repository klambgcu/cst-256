<?php
namespace App\Models;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-09
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold posts associated to a group
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

class GroupPostModel
{
    private $id;
    private $group_id;
    private $user_id;
    private $post;
    private $created_time;
    
    public function __construct($id, $group_id, $user_id, $post, $created_time)
    {
        $this->id = $id;
        $this->group_id = $group_id;
        $this->user_id = $user_id;
        $this->post = $post;
        $this->created_time = $created_time;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroup_id()
    {
        return $this->group_id;
    }

    /**
     * @return mixed
     */
    public function getUser_id()
    {
        return $this->user_id;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @return mixed
     */
    public function getCreated_time()
    {
        return $this->created_time;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $group_id
     */
    public function setGroup_id($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @param mixed $created_time
     */
    public function setCreated_time($created_time)
    {
        $this->created_time = $created_time;
    }

}

