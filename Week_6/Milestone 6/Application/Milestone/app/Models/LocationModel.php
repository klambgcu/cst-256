<?php
namespace App\Models;

use JsonSerializable;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Model to hold organization location information
 * 2. Job Postings
 * 3.
 * ---------------------------------------------------------------
 */

class LocationModel implements JsonSerializable
{
    private $id;
    private $organization_id;
    private $location_name;
    private $street;
    private $city;
    private $state;
    private $postal_code;
    private $country;
    
    public function __construct($id, $organization_id, $location_name, $street, $city, $state, $postal_code, $country)
    {
        $this->id = $id;
        $this->organization_id = $organization_id;
        $this->location_name = $location_name;
        $this->street = $street;
        $this->city = $city;
        $this->state = $state;
        $this->postal_code = $postal_code;
        $this->country = $country;
    }
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization_id()
    {
        return $this->organization_id;
    }

    /**
     * @return mixed
     */
    public function getLocation_name()
    {
        return $this->location_name;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getPostal_code()
    {
        return $this->postal_code;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $organization_id
     */
    public function setOrganization_id($organization_id)
    {
        $this->organization_id = $organization_id;
    }

    /**
     * @param mixed $location_name
     */
    public function setLocation_name($location_name)
    {
        $this->location_name = $location_name;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostal_code($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}

