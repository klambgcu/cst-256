<?php
use App\Models\GroupModel;

/*
 * ---------------------------------------------------------------
 * Name : Kelly E. Lamb
 * Date : 2022-01-12
 * Class : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Group View
 * 2. Need DAO/Model Group, All Group Posts, All Member(user) info
 * 3.
 * ---------------------------------------------------------------
 */

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Group View')
@section('content')

<script type="text/javascript">
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<script type="text/javascript">
$(document).ready( function () {
	$('#member_entries').DataTable();
} );
</script>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="container">
	<h1>Group: {{ $group->getGroup_name() }}</h1>
	<p><h3>Description:</h3>{{ $group->getGroup_description() }}</p>
	<hr>
</div>

@if ($isMember)

<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-7">
			<h2>Announcements</h2>
			<table id="post_entries">
				<thead>
					<tr>
						<th>Information</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($postsList as $p)
					<tr>
						<td>{{ $p->getPost() }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
		<div class="col-md-5">
			<h2>Members</h2>
			<table id="member_entries">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($membersUserModelList as $m)
					<tr>
						<td>{{ $m->getFirst_name() . ' ' . $m->getLast_name() }}</td>
						<td>{{ $m->getEmail() }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>

	<hr>

</div>
<!-- /container -->

@else

<center>
	<p>You must be a member of the group to see posts and other member
		information.</p>
</center>
@endif
@endsection

