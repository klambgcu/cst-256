<?php

use App\Models\UserModel;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-19
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Admin Job Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

 ?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Admin Job Listing')
@section('content')

<style>
  .button {
    background-color: lightblue;
    border: none;
    color: white;
    padding: 4px 16px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 20px;
    margin: 2px 2px;
    cursor: pointer;
  }
</style>

<script>
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	@csrf
	<div align="center">
	    <h1>Admin Job Maintenance Listing</h1>
	    <p><a class='button' href='adminCreateJob'>Create Job</a>&nbsp;&nbsp;<a class='button' href='adminOrganization'>View Organizations</a></p>
	    <hr>
	</div>

<table id="post_entries">
  <thead>
    <tr>
        <th>ID</th>
        <th>Organization</th>
        <th>Location</th>
        <th>Position</th>
        <th>Description</th>
        <th>Pos. Type</th>
        <th>Expire Date</th>
        <th>Skills</th>
        <th>Education</th>
        <th>Action</th>
    </tr>
  </thead>
  <tbody>

<?php
    $type = ["Full-Time", "Part-Time", "Contract"];
    $suspend = ["No", "Yes"];

    foreach ($jobsList as $u)
    {
        echo "  <tr>\n";
        echo "      <td>" . $u->getId() . "</td>\n";
        echo "      <td>" . $u->getOrg_name() . "</td>\n";
        echo "      <td>" . $u->getOrg_loc_name() . "</td>\n";
        echo "      <td>" . $u->getPosition_name() . "</td>\n";
        echo "      <td>" . $u->getDescription() . "</td>\n";
        echo "      <td>" . $type[$u->getPosition_type()] . "</td>\n";
        echo "      <td>" . $u->getExpire_date() . "</td>\n";
        echo "      <td>" . $u->getSkills_keywords() . "</td>\n";
        echo "      <td>" . $u->getEducation_keywords() . "</td>\n";
        echo "      <td>" . "<a href='adminChangeJob?id=" . $u->getId() . "&mode=0'>Edit<a>" . 
             "&nbsp;|&nbsp;<a href='adminChangeJob?id=" . $u->getId() . "&mode=1' onclick=\"return confirm('Confirmation Required. Delete " . $u->getPosition_name() . " Y/N ?')\">Delete<a></td>\n";
        
        echo "  </tr>\n";
	}
 ?>
</tbody>
</table>

</div>

@endsection

