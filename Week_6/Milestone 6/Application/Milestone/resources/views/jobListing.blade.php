<?php
use App\Models\GroupModel;

/*
 * ---------------------------------------------------------------
 * Name : Kelly E. Lamb
 * Date : 2022-01-10
 * Class : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Job Listing
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

$type = ["Full-Time", "Part-Time", "Contract"];

?>
<!DOCTYPE html>
@extends('layouts.appmaster')
@section('title', 'Job Search')
@section('content')

<script type="text/javascript">
$(document).ready( function () {
	$('#post_entries').DataTable();
} );
</script>

<div class="container">
	<form action="doJobSearch" method="POST">
    	@csrf
    	<div align="center">
    		<h1>Job Search</h1>
    		<hr>
    		<div class="row">
    			<div class="col-1"></div>
    			<div class="col-4">
    				<div class="form-floating mb-3 mt-3">
            		    <input type="text" class="form-control"  placeholder="Query Position" name="Position" id="Position" value = "{{ $position }}">
            		    <label for="Position" class="form-label">Position:</label>
    				</div>
    			</div>
    			<div class="col-3">
    				<div class="form-floating mb-3 mt-3">
            		    <input type="text" class="form-control"  placeholder="Query Description" name="Description" id="Description" value = "{{ $description }}">
            		    <label for="Description" class="form-label">Description:</label>
    				</div>
    			</div>
    			<div class="col-3">
    				<div class="form-floating mb-3 mt-3">
            		    <input type="text" class="form-control"  placeholder="Query Required Skill" name="Skill" id="Skill" value = "{{ $skill }}">
            		    <label for="Skill" class="form-label">Skill:</label>
    				</div>
    			</div>
    			<div class="col-1"></div>
    		</div>
    	<button type="submit" class="btn btn-primary">Search</button>
    	</div>
	</form>
	<hr>

	<table id="post_entries">
		<thead>
			<tr>
				<th>ID</th>
				<th>Organization</th>
				<th>Position</th>
				<th>Description</th>
				<th>Type</th>
				<th>Skills</th>
				<th>Education</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($jobsList as $j)
		<tr>
			<td>{{ $j->getId() }}</td>
			<td>{{ $j->getOrg_name() }}</td>
			<td><a href='doJobView?id={{ $j->getId() }}' data-toggle='tooltip' title='View Details'>{{ $j->getPosition_name() }}</a></td>
			<td>{{ $j->getDescription() }}</td>
			<td><?php echo $type[$j->getPosition_type()]; ?></td>
			<td>{{ $j->getSkills_keywords() }}</td>
			<td>{{ $j->getEducation_keywords() }}</td>
		</tr>
		@endforeach
		</tbody>
	</table>
</div>

@endsection

