<?php
namespace App\Http\Controllers;

use App\Services\Business\SecurityService;
use App\Services\Business\UserService;
use App\Services\Utility\ILoggerService;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2021-12-05
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Login Controller
 * 2. Handle Login Functionality
 * 3. Added Main Menu requirement
 * ---------------------------------------------------------------
 */

class LoginController extends Controller
{
    protected $logger;
    
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
    }
    
    //Login Method
    public function login()
    {
        $this->logger->info("Entering LoginController::login()");
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        // store registration parameters
        $email    = filter_input(INPUT_POST,'Email');
        $password = filter_input(INPUT_POST,'Password');

        // Validate Login
        $service = new SecurityService();
        $valid = $service->AuthenicateLogin($email, $password);
        
        // Determine if login successful
        if ($valid)
        {
            // Valid user
            $user_service = new UserService();
            $user = $user_service->getUserByEmail($email);
            $_SESSION["message"] = "Welcome " . $user->getFirst_name() . " " . $user->getLast_name() . ".";
            $_SESSION['principal'] = $user;
        }
        else
        {
            // Invalid user
            $_SESSION["message"] = "Login was unsuccessful - please try again.";
            unset($_SESSION['principal']);
        }

        $this->logger->info("Exiting LoginController::login()");
        return view('loginstatus');      
    }
    
    public function logout()
    {
        $this->logger->info("Entering LoginController::logout()");
        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        // remove all session variables
        session_unset();
        
        // destroy the session
        session_destroy();
        
        $this->logger->info("Exiting LoginController::logout()");
        return view('welcome');
    }
}
