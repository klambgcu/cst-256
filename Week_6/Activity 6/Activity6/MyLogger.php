<?php
namespace App\Services\Utility;

use Illuminate\Support\Facades\Log;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-22
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Logging Mechanism Concrete
 * 2.
 * 3.
 * ---------------------------------------------------------------
 */

class MyLogger implements ILoggerService
{
    public function debug($message, array $context = array())
    {
        Log::debug($message, $context);
    }

    public function warning($message, array $context = array())
    {
        Log::warning($message, $context);
    }

    public function error($message, array $context = array())
    {
        Log::error($message, $context);
    }

    public function info($message, array $context = array())
    {
        Log::info($message, $context);
    }
}

