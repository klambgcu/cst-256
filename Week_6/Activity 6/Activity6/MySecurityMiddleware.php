<?php
namespace App\Http\Middleware;

use Closure;

/*
 * ---------------------------------------------------------------
 * Name      : Kelly E. Lamb
 * Date      : 2022-01-22
 * Class     : CST-256 Database Application Programming III
 * Professor : Dr. Todd Wolfe
 * Assignment: Milestone
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Middleware - control access - require login
 * 2. Redirect to login if necessary
 * 3.
 * ---------------------------------------------------------------
 */

class MySecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $secureCheck = true;

        if (session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }
        
        if (isset($_SESSION['principal']))
        {
            $secureCheck = false;
            
        }
        else
        if ($request->is('/') ||
            $request->is('home') ||
            $request->is('login') ||
            $request->is('doLogin') ||
            $request->is('register') ||
            $request->is('loginstatus') ||
            $request->is('logout') ||
            $request->is('doRegister') ||
            $request->is('registerstatus') ||
            $request->is('welcome') ||
            $request->is('api') ||
            $request->is('api/*'))
        {
            $secureCheck = false;
        }
                
        if($secureCheck)
        {
            return redirect('login');
        }
        
        return $next($request);
    }
}
